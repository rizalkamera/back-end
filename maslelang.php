<?php
session_start();
require 'db.php';


?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Lelang | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-camera"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                                 <li>
                                    <a href="masterinputlelang.php"> Lelang</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pegembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                        <li>
                            <a href="maslelang.php"><i class="fas fa-hammer "></i> Master Lelang </a>
                        </li>
                        

                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                         <li>
                            <a href="laporan.php"><i class="fa fa-fw fa-edit"></i>Laporan</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Lelang
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-university"></i> Master Lelang
                                 </li>
                        </ul>
                    </div>
                </nav>
                            <!-- /.row -->
                        <div class="container">
                            <div class="row">
                                <div class='col-sm-4'>
                                    <h3> Form Lelang </h3>
                                    <form action="process.php?act=spnLelangBarang" method="post" class="form-center" role="form" enctype="multipart/form-data">
                                     <div class="row">
                                     <fieldset  class="form-group col-xs-8">
                                        <label for="namaB">Nama Barang:</label>
                                        <input type="text" class="form-control" name="namabarang" placeholder="Nama Barang" required>
                                    </fieldset>
                                </div>
                                    <div class="row">
                                         <fieldset  class="form-group col-xs-8">
                                            <label for="isiResep">Waktu Mulai:</label>
                                            <div class='input-group date' >
                                                <input type="datetime-local" class="form-control" id="wktuMulai" name='waktumulai' required oninvalid="this.setCustomValidity('Harap Diisi')"/>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset  class="form-group col-xs-8">
                                            <label for="isiResep">Waktu Selesai:</label>
                                            <div class='input-group date' >
                                                <input type='datetime-local' class="form-control" id="waktuselesai" name='waktuselesai' required />
                                            </div>
                                        </fieldset>
                                    </div>
                                    
                                    <div class="row">
                                         <fieldset  class="form-group col-xs-8">
                                            <label for="isiResep">Harga Awal :</label>
                                            <div class='input-group' >
                                                <input type='number' name="hargaawal" class="form-control" required/>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                    <fieldset class="form-group col-sm-5">
                                        <label for="hargaB">Kategori Barang</label>
                                        <select name="kategori" class="form-control" required>
                                            <option value="">--Kargori Barang--</option>
                                            <?php
                                            $r = mysqli_query($link, "SELECT * from kategori WHERE hapuskah = '0'");
                                            while($r1 = mysqli_fetch_array($r))
                                            {
                                                echo '<option value="' .$r1['id']. '">' .$r1['nama']. '</option>';
                                            }
                                            ?>
                                        </select>
                                    </fieldset>
                                </div>
        
                                    <div class="row">
                                        <fieldset class="form-group ">
                                            <div class="form-group col-xs-4">
                                                <label>Deskripsi :</label>
                                                <textarea class="form-control" name="deskripsi" cols="5" rows="3" required>
                                             </textarea>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                         <fieldset  class="form-group col-xs-8">
                                             <label for="gambarkamera">Gambar Barang:</label> </br>
                                             <label for="file" class="file">
                                                <input type="file" name="gambarkamera" accept="image/jpeg" onchange="loadFile(event)" required/>
                                                <br>
                                                <img id="output" style="width: 50%; height: 50%;" />
                                            </label>
                                        </fieldset>
                                    </div>
                                    <div class="row">
                                        <fieldset  class="form-group col-xs-8">
                                            <input type="submit" class="btn btn-info" name="submit" value="Tambah">
                                        </fieldset>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <br></br>
                        </div>
                            <div class="col-sm-15">
                                <h4> Data Lelang yang Tersedia :</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                        <thead style="text-align: center;">
                                            <tr >
                                                <th >ID</th>
                                                <th >WAKTU MULAI</th>
                                                <th >WAKTU AKHIR</th>
                                                <th >BARANG</th>
                                                <th >HARGA AWAL</th>
                                                <th >HARGA TERTINGGI</th>
                                                <th >STATUS PEMBAYARAN</th>
                                                <th >DATA TRANSFER</th>
                                                <th >PELANGGAN</th>
                                                <th >EDIT | HAPUS | CETAK</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            include 'tanggal_indo.php';
                                            //      $sql = "SELECT *,l.norek as rek_trf FROM lelang l LEFT JOIN user u ON l.pelanggan_id = u.id LEFT JOIN bank b ON l.bankid = b.idbank WHERE l.hapuskah = '0'";
                                            $sql = "SELECT *,l.id, l.norek as rek_trf FROM lelang l LEFT JOIN user p ON l.pelanggan_id = p.id LEFT JOIN bank b ON l.bankid = b.idbank WHERE l.hapuskah = '0'";
                                            $result = mysqli_query($link, $sql);
                                            if (!$result) {
                                                die("SQL Error:" . $sql);
                                            }
                                            while ($row = mysqli_fetch_array($result)) 
                                            {
                                                echo "
                                            <tr class= 'row1'>
                                                <th >" . $row['id'] . "</th>
                                                <td class='row1 col-xs-2'>" . TanggalIndo($row['tanggalawal']) . "</td>
                                                <td class='row1 col-xs-2'>" . TanggalIndo($row['tanggalakhir']) . "</td>

                                                <td class='row1 col-xs-1'>
                                                    <button type='button' style='margin-left: 3%;' class='btn btn-info btn-sm' data-id='" .$row['id']. "' data-nr='".$row['namabarang']."' data-gbr='images/".$row['gambarbarang']."'data-je='" .$row['jenis']. "' data-des='" .$row['deskripsi']. "' data-toggle='modal' data-target='#viewbarang'>Lihat Barang</button>
                                                <br></br>
                                                    <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id']. "'  data-toggle='modal' data-target='#editgbr'>Ubah Gambar</button>
                                                <br></br>
                                                 


                                                </td>

                                                <td class='row1 col-xs-1'>Rp." .number_format($row['hargaawal'], 0, ',', '.') .",-" ."</td>

                                                <td class='row1 col-xs-1'>Rp." .number_format($row['hargatertinggi'], 0, ',', '.') .",-" ."</td>

                                                <td class='row1 col-xs-1'>" .$row['statuspembayaran'] ."</td>";

                                                if($row['statuspembayaran'] == '')
                                                {
                                                    echo "
                                                <td class='row1 col-sm-1'>
                                                    <button type='button' class='btn btn-info btn-sm' data-nb='" .$row['namabank']. "' data-nr='" .$row['rek_trf']. "' data-pr='" .$row['namapemilik']. "' data-nom='" .$row['nominaltransfer']. "' data-toggle='modal' data-target='#viewtrf' disabled>Detail</button>
                                                <br></br> 
                                                    <a href='images/upload_bukti/not_found.jpg'<button type='button' class='btn btn-info btn-sm'>Lihat Gambar</button></a>
                                                <br></br>
                                                    <button type='button' class='btn btn-primary btn-sm' data-vali='" .$row['buktivalid']. "' data-toggle='modal' data-target='#buktitrf' disabled>Validasi Bukti</button>
                                                </td>";
                                                }
                                                else if ($row['statuspembayaran'] == 'menunggu pembayaran') 
                                                {
                                                    echo "
                                                <td class='row1 col-sm-1'>
                                                    <button type='button' class='btn btn-info btn-sm' data-nb='" .$row['namabank']. "' data-nr='" .$row['rek_trf']. "' data-pr='" .$row['namapemilik']. "' data-nom='" .$row['nominaltransfer']. "' data-toggle='modal' data-target='#viewtrf' disabled>Detail</button>
                                                <br></br> 
                                                    <a href='images/upload_bukti/not_found.jpg'><button type='button' class='btn btn-info btn-sm'>Lihat Gambar</button></a>
                                                <br></br>
                                                    <button type='button' class='btn btn-primary btn-sm' data-vali='" .$row['buktivalid']. "' data-toggle='modal' data-target='#buktitrf' disabled>Validasi Bukti</button>
                                                </td>";
                                                }
                                                else if($row['statuspembayaran'] == 'menunggu bukti transfer')
                                                {
                                                    //jika bank id kosong, brarti data transfer belum diinput
                                                    if ($row['bankid'] == '') 
                                                    {
                                                        echo "
                                                <td class='row1 col-sm-1'>
                                                    <button type='button' class='btn btn-info btn-sm' data-nb='" .$row['namabank']. "' data-nr='" .$row['rek_trf']. "' data-pr='" .$row['namapemilik']. "' data-nom='" .$row['nominaltransfer']. "' data-toggle='modal' data-target='#viewtrf' disabled>Detail</button>
                                                <br></br> 
                                                    <a href='images/upload_bukti/not_found.jpg'><button type='button' class='btn btn-info btn-sm'>Lihat Gambar</button></a>
                                                <br></br>
                                                    <button type='button' class='btn btn-primary btn-sm' data-vali='" .$row['buktivalid']. "' data-toggle='modal' data-target='#validasibukti' disabled>Validasi Bukti</button>
                                                </td>";
                                                    }
                                                    else
                                                    {
                                                        echo "
                                                <td class='row1 col-sm-1'>
                                                    <button type='button' class='btn btn-info btn-sm' data-nb='" .$row['namabank']. "' data-nr='" .$row['rek_trf']. "' data-pr='" .$row['namapemilik']. "' data-nom='" .$row['nominaltransfer']. "' data-toggle='modal' data-target='#viewtrf'>Detail</button>
                                                <br></br> 
                                                    <a href='images/upload_bukti/" .$row['buktitransfer']. "'><button type='button' class='btn btn-info btn-sm'>Lihat Gambar</button></a>
                                                <br></br>
                                                    <button type='button' class='btn btn-primary btn-sm' data-idl='" .$row['id']. "' data-vali='" .$row['buktivalid']. "' data-pel='" .$row['id']. "' data-toggle='modal' data-target='#validasibukti'>Validasi Bukti</button>
                                                </td>";
                                                    }
                                                    
                                                }
                                                else if ($row['statuspembayaran'] == 'selesai') 
                                                {
                                                    echo "
                                                <td class='row1 col-sm-1'>
                                                    <button type='button' class='btn btn-info btn-sm' data-nb='" .$row['namabank']. "' data-nr='" .$row['rek_trf']. "' data-pr='" .$row['namapemilik']. "' data-nom='" .$row['nominaltransfer']. "' data-toggle='modal' data-target='#viewtrf' >Detail</button>
                                                <br></br> 
                                                    <a href='images/upload_bukti/" .$row['buktitransfer']. "'><button type='button' class='btn btn-info btn-sm'>Lihat Gambar</button></a>
                                                <br></br>
                                                    <button type='button' class='btn btn-primary btn-sm' data-vali='" .$row['buktivalid']. "' data-toggle='modal' data-target='#validasibukti' disabled>Validasi Bukti</button>
                                                </td>";
                                                }
                                            

                                                echo "<td class='row1 col-xs-1'>" .$row['namapenawar']."</td>";


                                                //convert datetime agar bsa tampil di input type date
                                                $mulai = strftime('%Y-%m-%dT%H:%M:%S', strtotime($row['tanggalawal']));
                                                $selesai = strftime('%Y-%m-%dT%H:%M:%S', strtotime($row['tanggalakhir']));

                                                echo "<td class='row1 col-xs-1'>
                                                    <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id']. "' data-wm='" .$mulai. "' data-ws='" .$selesai."' data-ha='" .$row['hargaawal']. "' data-nb='" .$row['namabarang']. "' data-je='" .$row['jenis']. "' data-de='" .$row['deskripsi']. "' data-st='" .$row['statuspembayaran']. "' data-toggle='modal' data-target='#editlelang'> <span class='glyphicon glyphicon-pencil'></span></button><br></br>

                                                    <a href='process.php?act=deletelelang&idlelang=" .$row['id']."'<button type='button' class='btn btn-danger btn-sm' ";?> onclick="return confirm('Apakah anda yakin untuk menghapus data?');"><span class='glyphicon glyphicon-trash'></span></button></a><br></br>

                                                    <form method='POST' action='cetak_notaLelang.php'>
                                                    <?php echo " <input type='hidden' name='idlelang' value='" .$row['id']. "' />
                                                    <button type='submit' class='btn btn-success btn-sm' name='cetak'><i class='fa fa-print'></i></button>
                                                       <button type='button' class='btn btn-info btn-sm' data-idk='" .$row['id']. "' data-valik='" .$row['statusmenang']. "'data-toggle='modal' data-target='#validasiselesai'> Lelang Selesai</button>
                                                    </form>

                                                      </td>";
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>   
                            
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
          <div class="modal fade" id="validasiselesai" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Bukti Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Status Bukti </label><br>
                                                    <form method="POST" action="maslelang.php">
                                                    <input type="hidden" name="idlelangs" id="idlelangs">
                                                    
                                                    <!-- <input type="text" id="tst"> -->

                                                    <select id="status" name="status" class="form-control">
                                                        <option value="">Belum Diset</option>
                                                        <option value="0">Proses</option>
                                                        <option value="1">Menang</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="updts" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#validasiselesai').on('show.bs.modal', function(e) {
                                var valida = $(e.relatedTarget).data('valik');
                                var idlel = $(e.relatedTarget).data('idk');
                                 
                                // $("#tst").val(valida);  
                                $("#status").val(valida);  
                                $("#idlelangs").val(idlel);                                 
                            });
                            </script>


                            <?php
if(isset($_POST['updts']))
{
    $lelangs = $_POST['idlelangs'];
    $statuss = $_POST['status'];
    //$stokkamera = '';

    if($statuss == '')
    {
        //do nothing, redirect ke masorder.php
        // echo '<script language="javascript"> 
        //   document.location.href="maslelang.php"
        //   </script>';
    }
    elseif ($statuss == '0') {

        echo '<script language="javascript"> 
          alert("status 0")
          </script>';
        //update jadi menunggu bukti transfer
        //(jaga jaga ketika setelah diupdate valid dan ingin dikembalikan menjadi tidak valid)
       
         
        //update status bukti valid jadi 0
        $r = mysqli_query($link, "UPDATE lelang SET statusmenang = '0' WHERE id = '" .$lelangs. "'");

        // echo '<script language="javascript"> 
        //   alert("Data berhasil diubah.")
        //  document.location.href="maslelang.php"
        //   </script>';
    }
    else
    {


     
        $r = mysqli_query($link, "UPDATE lelang SET statusmenang = '1' WHERE id = '" .$lelangs. "'");

        // echo '<script language="javascript"> 
        //   alert("Data berhasil diubah.")
        //   document.location.href="maslelang.php"
        //   </script>';
    }
}

?>





        <script type="text/javascript">
            var loadFile = function(event) {
                var output = document.getElementById('output');
                output.src = URL.createObjectURL(event.target.files[0]);
            };
        </script>

        <script type="text/javascript">
            $('#wktuMulai, #wktuSelesai').on('change textInput input', function () {
                if ( ($("#wktuMulai").val() != "") && ($("#wktuSelesai").val() != "")) {
                   
                    var firstDate = new Date($("#wktuMulai").val());
                    var secondDate = new Date($("#wktuSelesai").val());
                    var mulai = firstDate.getTime();
                    var selesai = secondDate.getTime();
                    if( mulai >= selesai)
                    {
                        alert("Waktu Selesai harus lebih besar dari waktu mulai.");
                        document.getElementById('wktuMulai').value = '';
                        document.getElementById('wktuSelesai').value = '';
                    }
                }
            });
        </script>

      
        
        
        <div class="modal fade" id="viewbarang" role="dialog">
            <div class="modal-dialog" role="document">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Informasi Barang Dilelang</h4>
                    </div>
                    <div class="modal-body">
                        <div style="margin-left: 5%;">
                            <div class="row">
                                <label for="isiResep">Nama Barang </label><br>
                                <span class="namar"></span>
                            </div>
                            <br>
                            <div class="row">
                                <label for="isiResep">Jenis </label><br>
                                <span class="jenisr"></span>
                            </div>
                            <br>
                            <div class="row">
                                <label for="isiResep">Deskripsi Barang </label><br>
                                <textarea class="desk" disabled></textarea>
                            </div>
                            <br>
                            <div class="row">
                                <label for="isiResep">Gambar Tampilan </label><br>
                                <img class="gam" src="">
                            </div>
                            <br>
                           
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> 
        $('#viewbarang').on('show.bs.modal', function(e) {
        
                var lel = $(e.relatedTarget).data('id');
                var nr = $(e.relatedTarget).data('nr');
                var jen = $(e.relatedTarget).data('je');
                var deskr = $(e.relatedTarget).data('des');
                var gb = $(e.relatedTarget).data('gbr');
                
                
                 $(e.currentTarget).find('input[name="idr"]').val(lel);
                $(".namar").html(nr);
                $(".jenisr").html(jen);
                $(".desk").val(deskr);
                $(".gam").attr('src', gb);
                
            });
        </script>

        <div class="modal fade" id="editgbr" role="dialog">
            <div class="modal-dialog" role="document">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Ubah Gambar </h4>
                    </div>
                    <div class="modal-body">
                        <div style="margin-left: 5%;">
                            <br>
                            <div class="row">
                                <label for="isiResep">Gambar Tampilan </label><br>
                                <form method="POST" action="maslelang.php" enctype="multipart/form-data">
                                <input type="hidden" name="idlel" />
                                <input type="file" name="gbr_brg" accept="image/*" onchange="loadFile_edit(event)" />
                            </div>
                            <br>
                            <div class="row">
                                <img id="output_edit" style="width: 50%; height: 50%;" />
                            </div>
                            <br>
                            <div class="row">
                                <button type="submit" name="subb" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"> 

        $('#editgbr').on('show.bs.modal', function(e) {
        
            var idba = $(e.relatedTarget).data('id');
                            
            $(e.currentTarget).find('input[name="idlel"]').val(idba);

        });
        var loadFile_edit = function(event) {
            var output_edit = document.getElementById('output_edit');
            output_edit.src = URL.createObjectURL(event.target.files[0]);
        };
        </script>

        <div class="modal fade" id="editlelang" role="dialog">
            <div class="modal-dialog" role="document">
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Ubah Data Lelang</h4>
                    </div>
                   <div class="modal-body">
                        <div style="margin-left: 5%;">
                            <div class="row">
                                <div class="col-sm-6">
                                    <form method="POST" action="maslelang.php">
                                    <input type="hidden" name="idlela" />
                                    <label for="isiResep">Nama Barang </label><br>
                                    <input type="text" class="form-control" name="namares" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="isiResep">Waktu Mulai </label><br>
                                    
                                    <input type="datetime-local" name="wamulai" class="form-control" id="wamulai"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="isiResep">Waktu Selesai </label><br>
                                    <input type="datetime-local" class="form-control" name="waselesai" id="waselesai"/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label for="isiResep">Harga Awal </label><br>
                                    <input type="text"  class="form-control" id="harg_awl" name="harg_awl" />
                                </div>
                            </div>
                            <br>
                           
                           
                            <br>
                            <div class="row">
                                <div class="col-sm-5">
                                    <label for="isiResep">Deskripsi Barang </label><br>
                                    <textarea class="form-control" rows="3" cols="20" id="deskres" name="des"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <button type="submit" name="sublel" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <script type="text/javascript">

        $('#editlelang').on('show.bs.modal', function(e) {

        var idlel = $(e.relatedTarget).data('id');
        var wamu = $(e.relatedTarget).data('wm');
        var wasel = $(e.relatedTarget).data('ws');
        var har = $(e.relatedTarget).data('ha');
        var namre = $(e.relatedTarget).data('nb');
        var desre = $(e.relatedTarget).data('de');
        

        $(e.currentTarget).find('input[name="idlela"]').val(idlel);
        $(e.currentTarget).find('input[name="wamulai"]').val(wamu);
        $(e.currentTarget).find('input[name="waselesai"]').val(wasel);
        //$(e.currentTarget).find('input[name="harg_awl"]').val(har);
        $("#harg_awl").val(har);
        $(e.currentTarget).find('input[name="namares"]').val(namre);
       
        $("#deskres").val(desre);
        

            $('#wamulai, #waselesai').on('change textInput input', function () {
                if ( ($("#wamulai").val() != "") && ($("#waselesai").val() != "")) {
                   
                    var firstDate = new Date($("#wamulai").val());
                    var secondDate = new Date($("#waselesai").val());
                    var mulai = firstDate.getTime();
                    var selesai = secondDate.getTime();
                    if( mulai >= selesai)
                    {
                        alert("Waktu Selesai harus lebih besar dari waktu mulai.");
                        document.getElementById('wamulai').value = '';
                        document.getElementById('waselesai').value = '';
                    }
                }
            });

        });
        </script>

        <div class="modal fade" id="validasibukti" role="dialog">
            <div class="modal-dialog" role="document">
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Validasi Bukti Transfer</h4>
                    </div>
                    <div class="modal-body">
                        <div style="margin-left: 5%; margin-right: 5%;">
                            <div class="row">
                                <label for="NamaPenerima">Status Bukti </label><br>
                                <form method="POST" action="maslelang.php">
                                <input type="hidden" name="idl" id="lelang">
                                <input type="hidden" name="idp" id="pelangg">
                                
                                <select id="stato" name="statu" class="form-control">
                                    <option value="">Belum Diset</option>
                                    <option value="0">Tidak Valid</option>
                                    <option value="1">Valid</option>
                                </select>
                            </div>
                            <br>
                            <div class="row">
                                <button type="submit" name="updt" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $('#validasibukti').on('show.bs.modal', function(e) {
             
            var lel = $(e.relatedTarget).data('idl');
            var pela = $(e.relatedTarget).data('pel');
           // var emai = $(e.relatedTarget).data('email');
            var valid = $(e.relatedTarget).data('vali');
            
            $("#lelang").val(lel);  
            $("#pelangg").val(pela);
            //$("#email").val(emai);
            $("#stato").val(valid);
        });
        </script>

        <?php
        if(isset($_POST['updt']))
            {
    $lel = $_POST['idl'];
    $lels = $_POST['idp'];
    $status = $_POST['statu'];

    if($status == '')
    {
        //do nothing, redirect ke masorder.php
        echo '<script language="javascript"> 
          document.location.href="maslelang.php"
          </script>';
    }
    elseif ($status == '0') {

        echo '<script language="javascript"> 
          alert("status 0")
          </script>';
        //update jadi menunggu bukti transfer
        //(jaga jaga ketika setelah diupdate valid dan ingin dikembalikan menjadi tidak valid)
        $e = mysqli_query($link, "UPDATE lelang SET statuspembayaran = 'menunggu bukti transfer' WHERE id = '" . $lel . "'");

        //update status bukti valid jadi 0
        $r = mysqli_query($link, "UPDATE lelang SET buktivalid = '0' WHERE id = '" .$lel. "'");



         
        
        // document.location.href="masorder.php"

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
         document.location.href="maslelang.php"
          </script>';
    }
    else
    {
        echo '<script language="javascript"> 
          alert("status 1")
          </script>';
        //update status bukti valid jadi 1
        //update status kiriman jadi 'proses pengiriman'
        $r = mysqli_query($link, "UPDATE lelang SET buktivalid = '1', statuspembayaran = 'selesai' WHERE id = '" .$lel. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="maslelang.php"
          </script>';
    }
}
        ?>
        <div class="modal fade" id="viewtrf" role="dialog">
            <div class="modal-dialog" role="document">
            
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Detail Data Transfer</h4>
                    </div>
                    <div class="modal-body">
                        <div style="margin-left: 5%; margin-right: 5%;">
                            <div class="row">
                                <label for="NamaPenerima">Bank Tujuan Transfer </label><br>
                                <span class="bank"></span>
                            </div>
                            <br>
                            <div class="row">
                                <label for="NoHPPenerima">Nomor Rekening </label><br>
                                <span class="no_rek"></span>
                            </div>
                            <br>
                            <div class="row">
                                <label for="AlamatPenerima">Nama Pemilik Rekening </label><br>
                                <span class="nama_pmilik"></span>
                            </div>
                            <br>
                            <div class="row">
                                <label for="KodePos">Nominal Transfer </label><br>
                                Rp. <span class="nominal"></span>
                            </div>
                            <br>
                            <div class="row">
                                <button class="btn btn-default" data-dismiss="modal">Tutup</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <script type="text/javascript"> 

        function currency(str) {
            var reverse = str.toString().split('').reverse().join(''),
            ribuan  = reverse.match(/\d{1,3}/g);
            ribuan  = ribuan.join('.').split('').reverse().join('');

            return ribuan;
        }

        $('#viewtrf').on('show.bs.modal', function(e) {
        
            var nb1 = $(e.relatedTarget).data('nb');
            var nr1 = $(e.relatedTarget).data('nr');
            var pr1 = $(e.relatedTarget).data('pr');
            var nom1 = $(e.relatedTarget).data('nom');

            $(".bank").html(nb1);
            $(".no_rek").html(nr1);
            $(".nama_pmilik").html(pr1);
            $(".nominal").html(currency(nom1)+',-');
            
            });
        </script>
    </body>
</html>


<?php 
//set ukuran file maksimal
define ("MAX_SIZE","2000");

//method ambil format gambar
function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }


if(isset($_POST['subb']))
{
    $id = $_POST['idlel'];
    $image =$_FILES["gbr_brg"]["name"];
    $uploadedfile = $_FILES['gbr_brg']['tmp_name'];
     
    if ($image) 
    {
        //hapus karakter "/"
        $filename = stripslashes($_FILES['gbr_brg']['name']);

        //ambil format file
        $extension = getExtension($filename);

        //ubah jadi huruf kecil
        $extension = strtolower($extension);
        
        if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png")) 
        {
            //alert format file tidak terbaca
            echo '<script language="javascript"> 
                  alert("format file tidak mendukung. Silahkan unggah file berformat .jpg, .jpeg, atau .png")
                  document.location.href="maslelang.php"
                  </script>';
        }
        else
        {
            $size=filesize($_FILES['gbr_brg']['tmp_name']);
            //jika ukuran melebihi 400kb (max_size diset di awal sebesar 400, dikali 1024 bytes)
            if ($size > MAX_SIZE*1024)
            {
               //alert ukurann meleihi kapasitas
                 echo '<script language="javascript"> 
                      alert("ukuran file terlalu besar. Maksimal ukuran file adalah 2000kB.")
                      document.location.href="maslelang.php"
                      </script>';
            }


            if($extension=="jpg" || $extension=="jpeg" )
            {
                $uploadedfile = $_FILES['gbr_brg']['tmp_name'];

                //fungsi agar file asli dapat diresampled
                $src = imagecreatefromjpeg($uploadedfile);
            }
            else
            {
                $uploadedfile = $_FILES['gbr_brg']['tmp_name'];
                //fungsi agar file asli dapat diresampled
                $src = imagecreatefrompng($uploadedfile);
            }

            // ambil ukuran lebar dan tinggi image asli
            list($width,$height)=getimagesize($uploadedfile);


            //1a. resample image untuk index index
            $newheight=188;
            $newwidth=($width/$height)*$newheight;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp=imagecreatetruecolor($newwidth,$newheight);

            //1b. resample image untuk index prod_det
            $newheight1=188;
            $newwidth1=($width/$height)*$newheight1;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp1=imagecreatetruecolor($newwidth1,$newheight1);

            imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
            imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);


            //sebelum set nama file yang baru, lakukan enkripsi md5
            $sp = explode(".",$_FILES['gbr_brg']['name']);
            $nam = md5($_FILES['gbr_brg']['name'] . time());

            //substring hasil md5 dan concat dgn format file
            $spl0 = substr($nam,0,10);
            $spl = $spl0 . "." .$sp[count($sp) - 1];

            
           $filename1 = "images/". $spl;
           //$filename1 = "../images/lelang/prod_det/". $spl;


            imagejpeg($tmp,$filename,100);
            imagejpeg($tmp1,$filename1,100);


            imagedestroy($src);
            imagedestroy($tmp);
            imagedestroy($tmp1);

            //update nama file pada data yang diubah
            $e = mysqli_query($link, "UPDATE lelang SET gambarbarang = '" .$spl. "' WHERE id= '" .$id. "'");
            
             echo '<script language="javascript"> 
          alert("File Berhasil diunggah.")
          document.location.href="maslelang.php"
          </script>';
        }
    }
    
}

if(isset($_POST['sublel']))
{
    $id = $_POST['idlela'];
    $wm = $_POST['wamulai'];
    $ws = $_POST['waselesai'];
    $har = $_POST['harg_awl'];
    $nam = $_POST['namares'];
    $deskr = $_POST['des'];

    $t = mysqli_query($link, "UPDATE lelang SET tanggalawal = '" .$wm. "', tanggalakhir = '" .$ws. "', namabarang = '" .$nam. "', deskripsi = '" .$deskr. "', hargaawal = '" .$har. "' WHERE id = '" .$id. "'");

    echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="maslelang.php"
          </script>';
    
}
?>
