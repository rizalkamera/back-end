<?php
session_start();
require './db.php';

/*if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}*/
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Karyawan | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
               <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Asesoris</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                        <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Denda</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang </a>
                        </li>
                        <!--  <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li> -->
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Karyawan
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-male"></i> Master Karyawan
                                </li>
                            </ol>
                        </div>
                            <!-- /.row -->
                        <div class="container">
                            <div class="col-lg-6">

                                 <form action="process.php?act=spnKaryawan" method="post" class="form" role="form" enctype="multipart/form-data">
                                <div class="row">
                                    <fieldset  class="form-group col-xs-6">
                                        <label for="namaB">Nama Karyawan:</label>
                                        <input type="text" class="form-control" name="nama" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-7">
                                        <label for="hargaB">Alamat:</label>
                                        <textarea class="form-control" name="alamat" rows="3" cols="15" required></textarea>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="gambarB">Nomor HP:</label> </br>
                                            <input type="number" class="form-control" name="notlp" required>
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="desResep">Jabatan:</label><br>
                                        <select name="jabatan" class="form-control">
                                            <option value="Kepala Teknisi">Kepala Kamera</option>
                                            <option value="Karyawan">Karyawan</option>
                                        </select>
                                    </fieldset>
                                </div>
                                
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" name ="add" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                            </div>
                        </div>
                            <div class="col-sm-8">
                                <h4> Daftar Karyawan :</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                        <thead style="text-align: center;">
                                            <tr >
                                                <th >ID</th>
                                                <th >NAMA</th>
                                                <th >ALAMAT</th>
                                                <th >NO HP</th>
                                                <th >JABATAN</th>
                                                <th >EDIT | HAPUS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                           // $sql = "SELECT id, nama, alamat, notlp, jabatan FROM `user` WHERE hapuskah = '0'";
                                           $sql = "SELECT * FROM user WHERE hapuskah = '0'";
                                            $result = mysqli_query($link, $sql);
                                            if (!$result) {
                                                die("SQL Error:" . $sql);
                                            }
                                            while ($row = mysqli_fetch_array($result)) {
                                        echo '<tr class= "row1">';
                                            echo "<th class='row1 col-xs-1'>" . $row['id'] . "</th>";
                                            echo "<td class='row1 col-xs-2'>" . $row['nama'] . "</td>";
                                            echo "<td class='row1 col-xs-4'>" . $row['alamat'] . "</td>";
                                            echo "<td class='row1 col-xs-1'>" . $row['notlp'] . "</td>";
                                            echo "<td class='row1 col-xs-1'>" . $row['jabatan'] . "</td>";

                                            echo "<td class='row1 col-xs-2'>
                                                <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id']. "' data-nm='" .$row['nama']. "' data-almt='" .$row['alamat']."' data-hp='" .$row['notlp']. "' data-jbt='" .$row['jabatan']. "' data-toggle='modal' data-target='#editkar'> <span class='glyphicon glyphicon-pencil'></span></button>

                                                <a href='maskaryawan.php?idkar=" .$row['id']."'<button type='button' class='btn btn-danger btn-sm'"; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data ?');"><span class='glyphicon glyphicon-trash'></span></button>
                                                  </td>
                                        </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>   
                            
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="editkar" role="dialog">
            <div class="modal-dialog" role="document">
              <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Ubah Data Karyawan</h4>
                    </div>
                   <div class="modal-body">
                        <div style="margin-left: 5%;">
                            <div class="row">
                                <div class="col-sm-8">
                                    <form method="POST" action="maskaryawan.php">
                                    <input type="hidden" name="idkar" />
                                    <label for="isiResep">Nama </label><br>
                                    <input type="text" id="name" name="nama" class="form-control"/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-8">
                                    <label for="isiResep">Alamat </label><br>
                                    <textarea id="alamat" name="alamat" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-5">
                                    <label for="isiResep">Nomor HP </label><br>
                                    <input type="number" name="notlp" class="form-control" />
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-5">
                                    <label for="isiResep">Jabatan </label><br>
                                    <select id="pos" name="jabatan" class="form-control">
                                        <option value="Kepala Kamera">Kepala Kamera</option>
                                        <option value="Karyawan">Karyawan</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <button type="submit" name="updt" class="btn btn-primary">Ubah</button>
                                </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <script type="text/javascript">

        $('#editkar').on('show.bs.modal', function(e) {

            var idk = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('nm');
            var addr = $(e.relatedTarget).data('almt');
            var notlp = $(e.relatedTarget).data('hp');
            var jabat = $(e.relatedTarget).data('jbt');
            
            $(e.currentTarget).find('input[name="idkar"]').val(idk);
            $(e.currentTarget).find('input[name="nama"]').val(name);
            $("#alamat").val(addr);
            $(e.currentTarget).find('input[name="notlp"]').val(notlp);
            $("#pos").val(jabat);
        });
        </script>
    </body>
</html>


<?php 


if(isset($_POST['updt']))
{
    $idkar1 = $_POST['idkar'];
    $name = $_POST['nama'];
    $addr = $_POST['alamat'];
    $phone = $_POST['notlp'];
    $jabatan1 = $_POST['jabatan'];

    $t2 = mysqli_query($link, "UPDATE user SET nama = '" .$name. "', alamat = '" .$addr. "', notlp = '" .$phone. "', jabatan = '" .$jabatan1. "' WHERE id = '" .$idkar1. "'");
    if (!$t2) 
    {
        echo mysqli_error($link);
    }
    else
    {
        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="maskaryawan.php"
          </script>';
    }
}




if(isset($_GET['idkar']))
{
    $idkar1 = $_GET['idkar'];

    $t3 = mysqli_query($link, "UPDATE user SET hapuskah = 1 WHERE id = '" .$idkar1. "'");
    if (!$t3) 
    {
        echo mysqli_error($link);
    }
    else
    {
        echo '<script language="javascript"> 
          alert("Data berhasil dihapus.")
          document.location.href="maskaryawan.php"
          </script>';
    }
}



?>
