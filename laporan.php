<?php
session_start();
require './db.php';

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                            <?php 
                            // echo $pengguna; 
                            ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
             <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Asesoris</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pengembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang </a>
                        </li>
                        
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Order
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order
                                </li>
                            </ol>
                        </div>
                       <div class="container">
                            <div class="row">
                                <div class='col-sm-4'>
                                    <h3> Form Laporan </h3>
                                    
                                    <form action="laporan.php" method="post" class="form-center" role="form" enctype="multipart/form-data">
                                   
                                    <div class="row">
                                        <fieldset  class="form-group col-xs-9">
                                            <label for="isiResep">Dari Tanggal:</label>
                                            <div class='input-group date'>
                                                <input type="date" class="form-control" id="wktuMulai" name='daritanggal' required oninvalid="this.setCustomValidity('Harap Diisi')"/>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="row">
                                        <fieldset  class="form-group col-xs-9">
                                            <label for="isiResep">Sampai Tanggal:</label>
                                            <div class='input-group date'>
                                                <input type="date" class="form-control" id="wktuMulai" name='sampaitanggal' required oninvalid="this.setCustomValidity('Harap Diisi')"/>
                                            </div>
                                        </fieldset>
                                   </div>

                                <div class="row">
                                        <fieldset class="form-group col-xs-9">
                                    <input type="submit" class="btn btn-info" name="laporan" value="Tambah">
                                </div>
                                    </form>
                                </div>
                            </div>
                            <br></br>
                        </div>
                    <?php
                    if(isset($_POST['laporan'])){
                    //tabel notasewa
                    $tgl_awal = date('Y-m-d', strtotime($_POST['daritanggal']));
                    $tgl_akhir = date('Y-m-d', strtotime($_POST['sampaitanggal']));
                    $sql =mysqli_query($link, "SELECT *,
                                                CASE
                                                 WHEN n.user_id = 0 THEN n.namapenyewa
                                                 ELSE u.nama
                                                END AS nama_penyewa,n.id FROM notasewa n left join user u on u.id = n.user_id  left join hub_notasewa_dan_kamera h on h.kamera_id = n.id WHERE tanggalpesan BETWEEN '$tgl_awal' AND '$tgl_akhir'");

                    echo '<h4> Data Laporan yang Tersedia :</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                            <thead style="text-align: center;">
                                        <tr >
                                            <th style="text-align: center;">ID</th>
                                            <th style="text-align: center;" >NAMA PELANGGAN</th>
                                            <th style="text-align: center;">TANGGAL PESAN</th>
                                            <th style="text-align: center;" >DETIL TRANSAKSI</th>
                                            <th style="text-align: center;" >GRAND TOTAL</th>
                                            <th style="text-align: center;">CETAK NOTA</th>
                                        </tr>
                                    </thead>
                                    </div>
                                    </div>
                                    <tbody>';

                            include 'tanggal_indo.php';

                            while ($row = mysqli_fetch_array($sql)) {
                            echo '<tr class= "row1">';
                            echo '<th>' . $row["id"] . "</th>";
                            echo '<td class="row1 col-sm-2">' . $row["nama_penyewa"] . '</td>';
                            echo '<td class="row1 col-sm-6">' . TanggalIndo($row["tanggalpesan"]) . '</td>';
                            echo '<td class="row1 col-sm-2" style="text-align: center;">'.
                                 
                                    "<a href='detil_nota.php?id=" .$row['id']. "'><button type='button' class='btn btn-info btn-sm'>Lihat Detil</button></a>".
                                '</td>
                                <td class="row1 col-sm-2" style="text-align: center;">Rp.' .number_format($row['grandtotal'], 0, ',', '.') .',-' .'</td>';
                                    
                                echo '</td>

                                <td class="row1 col-sm-3" style="text-align: center;">'.
                                    '<form method="POST" action="cetak_notaLaporan.php">
                                    <input type="hidden" name="idlaporan" value="' .$row['id']. '" />
                                    <button type="submit" class="btn btn-success btn-sm" name="cetak"><i class="fa fa-print"></i></button><br></br>
                                  
                            
                            <a href="process.php?act=deletekamera&idkam=' .$row["id"] . '<button type="button" class="btn btn-danger btn-sm" onclick="return confirm("Apakah anda yakin untuk menghapus data ?");"><span class="glyphicon glyphicon-trash"></span></button></a>
                            </td></tr>  </form>
                                </td>
                            </tr>';
                    }
                echo '</tbody>
            </table>';
            
            $p = mysqli_query($link, "SELECT SUM(grandtotal) as total from notasewa WHERE tanggalpesan BETWEEN '$tgl_awal' AND '$tgl_akhir'");
            $res_p = mysqli_fetch_array($p);
            $total = $res_p['total']; 

            ?>

            <b style="font-size: 20px;"><p style="margin-left: 80%"> TOTAL  = Rp. <?php echo number_format($total, 0, ',', '.'); 
            ?>,-</b>
        </div>'
<?php } ?>

</html>

