<?php
session_start();
require './db.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Barang - Biji Kopi | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera</a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa</a>
                                </li>
                                <li>
                                    <a href="masassoris.php"> Asessoris</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Barang - Biji Kopi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-coffee"></i> Master Barang-Biji Kopi
                                </li>
                            </ol>
                        </div>
                        <div class="col-lg-6">
                            <form action="process.php?act=spnBarang" method="post" class="form" role="form" enctype="multipart/form-data">
                                <div class="row">
                                    <fieldset  class="form-group col-xs-6">
                                        <label for="namaB">Nama Barang:</label>
                                        <input type="text" class="form-control" name="namaB" placeholder="Nama Barang" required>
                                    </fieldset>
                                </div>
                                
                                <div class="row">
                                    <fieldset class="form-group col-xs-3">
                                        <label for="hargaB">Harga:</label>
                                        <input type="number" onkeypress="return event.keyCode == 8 || return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="hargaB" id="hbar" placeholder="Harga" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-2">
                                        <label for="stokBB">Stok:</label>

                                        <!-- keyCode 8 -> backspace
                                        charCode >= 48 && <= 57 -> numeric -->
                                        <input type="number" onkeypress="return event.keyCode == 8 || event.charCode >= 48 && event.charCode <= 57" min="1" class="form-control" name="stokB" id="stk" placeholder="Stok" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="gambarB">Gambar Barang:</label> </br>
                                        <label for="file" class="file">
                                            <input type="file" name="gambarB"  accept="image/*" onchange="loadFile(event)"/>
                                            <br>
                                            <img id="output" style="width: 50%; height: 50%;" />
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="desResep">Deskripsi Barang:</label>
                                        <textarea class="form-control" name="desBarang" rows="3" placeholder="Deskripsi Produk" required></textarea>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- tabel barang -->

                    <div class="col-lg-11">
                        <h2>Data Barang yang tersedia</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >NAMA BARANG</th>
                                        <th >GAMBAR</th>
                                        <th >HARGA</th>
                                        <th >STOK</th>
                                        <th>DESKRIPSI</th>
                                        <th >EDIT/HAPUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //brapa data yang ditampilkan di tiap halaman
                                    $baris_per_page = 10;

                                    if(isset($_GET['page']))
                                    {
                                        $page = $_GET['page'];
                                    }
                                    else
                                    {
                                        $page = 1;
                                    }

                                    //set data yang ditampilkan mulai data ke brapa 
                                    $start_from = ($page-1) * $baris_per_page;


                                    $sql = "SELECT b.id_barang,b.nama_barang,b.harga,b.stok,b.deskripsi, b.jpeg_file FROM `kategori`as k,barang as b WHERE b.kategori_id = k.id_kat AND b.kategori_id = '1' AND k.hapuskah = '0' AND b.hapuskah = '0' LIMIT " .$start_from. "," .$baris_per_page;
                                    $result = mysqli_query($link, $sql);
                                    if (!$result) {
                                        die("SQL Error:" . $sql);
                                    }
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<tr class= "row1">';
                                        echo "<th>" . $row['id_barang'] . "</th>";
                                        echo "<td class='row1 col-sm-2'>" . $row['nama_barang'] . "</td>";
                                        echo "<td class='row1 col-sm-1'>
                                        <button type='button' class='btn btn-info btn-sm' data-gbr='../images/barang/index/" .$row['jpeg_file']. "' data-toggle='modal' data-target='#showgbr'><i class='fa fa-eye'></i></button>

                                        <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id_barang']. "' data-toggle='modal' data-target='#editgbr'><span class='glyphicon glyphicon-pencil'></span></button></td>";

                                        echo "<td class='row1 col-sm-1'>Rp." .number_format($row['harga'], 0, ',', '.') .",-" ."</td>";
                                        echo "<td class='row1 col-sm-1'>" . $row['stok'] . "</td>";
                                        echo "<td class='row1 col-sm-6'>" . $row['deskripsi'] . "</td>";
                                        echo "<td class='row1 col-sm-2'>

                                            <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id_barang']. "' data-nb='" .$row['nama_barang']. "' data-hg='" .$row['harga']. "' data-st='" .$row['stok']. "' data-desk='" .$row['deskripsi']. "' data-toggle='modal' data-target='#editbrg'><span class='glyphicon glyphicon-pencil'></span></button>

                                            <a href='process.php?act=deletebarang&idbarang=" .$row['id_barang']."'<button type='button' class='btn btn-danger btn-sm' "; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data ?');"><span class='glyphicon glyphicon-trash'></span></button></a>
                                            
                                            </td>
                                            <?php echo "</tr>";
                                    }?>
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination" style="margin-left: 60%;">
                            <?php

                            //LINK FIRST DAN PREV
                            if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
                            echo '
                            <li class="disabled"><a href="#">First</a></li>
                            <li class="disabled"><a href="#">&laquo;</a></li>
                            ';
                            }else{ // Jika page bukan page ke 1
                              $link_prev = ($page > 1)? $page - 1 : 1;
                            ?>
                            <li><a href="masbarang.php?page=1">First</a></li>
                            <li><a href="masbarang.php?page=<?php echo $link_prev ?>">&laquo;</a></li>
                            <?php
                            }




                            // -----------------LINK NUMBER-----------------
                            //ambil jumlah baris data
                            $y = "SELECT count(b.id_barang) as total FROM `kategori`as k,barang as b WHERE b.kategori_id = k.id_kat AND b.kategori_id = '1' AND k.hapuskah = '0' AND b.hapuskah = '0'";
                            $r = mysqli_query($link, $y);
                            $res = mysqli_fetch_array($r);
                            
                            //hitung jml hlmn yang akan terbentuk
                            $jml_hlm = ceil($res['total'] / $baris_per_page);
                            //tentukan brapa angka yg akan muncul sebelum dan stelah page aktif
                            $number = 2;

                            $start_number = ($page > $number)? $page - $number : 1; // Untuk awal link number
                            $end_number = ($page < ($jml_hlm - $number))? $page + $number : $jml_hlm; // Untuk akhir link number


                            for($i = $start_number; $i <= $end_number; $i++){
                              $link_active = ($page == $i)? ' class="active"' : '';
                            ?>
                            
                            <li<?php echo $link_active; ?>><a href="masbarang.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                            
                            <?php
                            }
                            ?>



                            <!--------- LINK NEXT AND LAST ------>
                            <?php
                            // Jika page sama dengan jumlah page, maka disable link NEXT nya
                            // Artinya page tersebut adalah page terakhir 
                            if($page == $jml_hlm){ // Jika page terakhir
                            ?>
                              <li class="disabled"><a href="#">&raquo;</a></li>
                              <li class="disabled"><a href="#">Last</a></li>
                            <?php
                            }else{ // Jika Bukan page terakhir
                              $link_next = ($page < $jml_hlm)? $page + 1 : $jml_hlm;
                            ?>
                              <li><a href="masbarang.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                              <li><a href="masbarang.php?page=<?php echo $jml_hlm; ?>">Last</a></li>
                            <?php
                            }
                            ?>

                        </ul>
                        <script type="text/javascript">
                            var loadFile = function(event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                        <div class="modal fade" id="editgbr" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Upload Gambar Baru</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                            <div class="row">
                                                <label for="NomorResi">File Gambar Baru </label><br>
                                                <form method="POST" action="masbarang.php" enctype="multipart/form-data">
                                                <input type="hidden" name="idbar" />
                                                <input type="file" name="gbr_brg" accept="image/*" onchange="loadFile_edit(event)"/>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <img id="output_edit" style="width: 50%; height: 50%;" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="subb" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                </form>
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                        $('#editgbr').on('show.bs.modal', function(e) {

                            var idba = $(e.relatedTarget).data('id');
                            
                            $(e.currentTarget).find('input[name="idbar"]').val(idba);

                        });

                        var loadFile_edit = function(event) {
                            var output_edit = document.getElementById('output_edit');
                            output_edit.src = URL.createObjectURL(event.target.files[0]);
                        };
                        </script>

                        <div class="modal fade" id="showgbr" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Gambar Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                            <div class="row">
                                                <img class="gam" src="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#showgbr').on('show.bs.modal', function(e) {

                            var gbr1 = $(e.relatedTarget).data('gbr');
                            
                           $(".gam").attr('src', gbr1);
                        });
                        </script>
                        <div class="modal fade" id="editbrg" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">  
                                            <div class="row">
                                                <form method="POST" action="masbarang.php">
                                                <input type="hidden" name="idbar" />
                                                <label for="isiResep">Nama Barang </label><br>
                                                <input type="text" name="nama" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Harga </label><br>
                                                <input type="text" name="harga" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Stok </label><br>
                                                <input type="number" name="stok" />
                                            </div>
                                            <div class="row">
                                                <label for="isiResep">Deskripsi </label><br>
                                                <textarea class="deskbar" name="deskb"></textarea>
                                            </div>
                                            
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="subbar" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#editbrg').on('show.bs.modal', function(e) {

                            var idb = $(e.relatedTarget).data('id');
                            var nam = $(e.relatedTarget).data('nb');
                            var har = $(e.relatedTarget).data('hg');
                            var sto = $(e.relatedTarget).data('st');
                            var des = $(e.relatedTarget).data('desk');
                            var kate = $(e.relatedTarget).data('kat');
                            
                           $(e.currentTarget).find('input[name="idbar"]').val(idb);
                           $(e.currentTarget).find('input[name="nama"]').val(nam);
                           $(e.currentTarget).find('input[name="harga"]').val(har);
                           $(e.currentTarget).find('input[name="stok"]').val(sto);
                           $(".deskbar").val(des);
                           $("#kateg").val(kate);
                        });
                        </script>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </body>
</html>


<?php 
//set ukuran file maksimal
define ("MAX_SIZE","2000");

//method ambil format gambar
function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }


if(isset($_POST['subb']))
{
    $id = $_POST['idbar'];
    $image =$_FILES["gbr_brg"]["name"];
    $uploadedfile = $_FILES['gbr_brg']['tmp_name'];
     
    if ($image) 
    {
        //hapus karakter "/"
        $filename = stripslashes($_FILES['gbr_brg']['name']);

        //ambil format file
        $extension = getExtension($filename);

        //ubah jadi huruf kecil
        $extension = strtolower($extension);
        
        if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png")) 
        {
            //alert format file tidak terbaca
        }
        else
        {
            $size=filesize($_FILES['gbr_brg']['tmp_name']);
            //jika ukuran melebihi 400kb (max_size diset di awal sebesar 400, dikali 1024 bytes)
            if ($size > MAX_SIZE*1024)
            {
               $change='<div class="msgdiv">You have exceeded the size limit!</div> ';
               $errors=1;
            }


            if($extension=="jpg" || $extension=="jpeg" )
            {
                $uploadedfile = $_FILES['gbr_brg']['tmp_name'];

                //fungsi agar file asli dapat diresampled
                $src = imagecreatefromjpeg($uploadedfile);
            }
            else
            {
                $uploadedfile = $_FILES['gbr_brg']['tmp_name'];
                //fungsi agar file asli dapat diresampled
                $src = imagecreatefrompng($uploadedfile);
            }

            // ambil ukuran lebar dan tinggi image asli
            list($width,$height)=getimagesize($uploadedfile);


            //1a. resample image untuk index index
            $newheight=188;
            $newwidth=($width/$height)*$newheight;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp=imagecreatetruecolor($newwidth,$newheight);

            //1b. resample image untuk index prod_det
            $newheight1=188;
            $newwidth1=($width/$height)*$newheight1;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp1=imagecreatetruecolor($newwidth1,$newheight1);


            //1c. resample image untuk cart
            $newheight2=110;
            $newwidth2=($width/$height)*$newheight2;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp2=imagecreatetruecolor($newwidth2,$newheight2);

            imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
            imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);
            imagecopyresampled($tmp2,$src,0,0,0,0,$newwidth2,$newheight2,$width,$height);


            //sebelum set nama file yang baru, pisahkan nama file dan format file
            $sp = explode(".",$_FILES['gbr_brg']['name']);

            // set nama file baru dengan melakukan enkripsi md5 dari nama file beserta formatnya concate waktu sistem
            $nam = md5($_FILES['gbr_brg']['name'] . time());

            //substring hasil md5 dan concat dgn format file
            $spl0 = substr($nam,0,10);
            $spl = $spl0 . "." .$sp[count($sp) - 1];

            
            $filename = "../images/barang/index/". $spl;
            $filename1 = "../images/barang/prod_det/". $spl;
            $filename2 = "../images/barang/cart/". $spl;

            
            imagejpeg($tmp,$filename,100);
            imagejpeg($tmp1,$filename1,100);
            imagejpeg($tmp2,$filename2,100);


            imagedestroy($src);
            imagedestroy($tmp);
            imagedestroy($tmp1);
            imagedestroy($tmp2);

            //update nama file pada data yang diubah
            $e = mysqli_query($link, "UPDATE barang SET jpeg_file = '" .$spl. "' WHERE id_barang = '" .$id. "'");
            
             echo '<script language="javascript"> 
          alert("File Berhasil diunggah.")
          document.location.href="masbarang.php"
          </script>';
        }
    }
    
    
}
if(isset($_POST['subbar']))
{
    $idb = $_POST['idbar'];
    $nam = $_POST['nama'];
    $har = $_POST['harga'];
    $sto = $_POST['stok'];
    $des = $_POST['deskb'];

    $e = mysqli_query($link, "UPDATE barang SET nama_barang = '" .$nam. "', harga = '" .$har. "', stok = '" .$sto. "', deskripsi = '" .$des. "' WHERE id_barang = '" .$idb. "'");

    if (!$e) {
        echo mysqli_error($link);
    }
    else
    {
         echo '<script language="javascript"> 
          alert("Data Berhasil diubah.")
          document.location.href="masbarang.php"
          </script>';
    }
   
}

?>