<?php
session_start();
require './db.php';

if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}
if(isset($_GET['nota']))
{
    $idservis = $_GET['nota'];
}
else
{
    echo '<script language="javascript">';
    echo 'document.location.href="masorder.php"';
    echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Servis | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $pengguna; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h1 class="page-header">
                                <a href="masservis.php"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i><br>Servis</button></a>
                                Detail Transaksi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Servis
                                </li>
                            </ol>
                        </div>
                        <div class="container">
                            <div class="col-lg-6">
                            <form action="process.php?act=addLayan" method="post" class="form" role="form">
                                <input type="hidden" name="serv" value="<?php echo $idservis ?>">
                                <div class="row">
                                    <fieldset  class="form-group col-xs-6">
                                        <label for="namaB">Layanan</label>
                                        <textarea class="form-control" name="layanan" rows="3" cols="40" required></textarea>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-sm-4">
                                        <label for="hargaB">Biaya</label>
                                        <input type="number" class="form-control" name="harga" />
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h2>Daftar Layanan untuk ID Nota Servis <?php echo $idservis; ?> </h2>

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                    <thead style="text-align: center;">
                                        <tr >
                                            <th >LAYANAN</th>
                                            <th >BIAYA</th>
                                            <th >EDIT | HAPUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT * FROM nota_servis n, hub_layanan_notaservis h, layanan l WHERE n.id_servis = h.servis_id AND h.layanan_id = l.id_layanan AND h.servis_id = '".$idservis."' AND n.hapuskah = '0' AND l.hapuskah='0'";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . mysqli_error($link));
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                        echo "<td class='col-sm-3'>" . $row['nama_layanan'] . "</th>";
                                        echo "<td class='col-sm-3'>Rp. " .number_format($row['harga'], 0, ',', '.') . ",-</td>";
                                        echo "<td class='row1 col-sm-2'>

                                            <button type='button' class='btn btn-primary btn-sm' data-layan='" .$row['id_layanan']. "' data-nama='" .$row['nama_layanan']. "' data-ser='" .$row['id_servis']. "' data-hg='" .$row['harga']. "' data-toggle='modal' data-target='#edit'><span class='glyphicon glyphicon-pencil'></span></button>

                                            <a href='process.php?act=deletelayan&idlayan=" .$row['id_layanan']."&idser=" .$row['id_servis']."'<button type='button' class='btn btn-danger btn-sm' "; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data <?php echo $row['nama_layanan']; ?> ?');"><span class='glyphicon glyphicon-trash'></span></button></a>
                                            
                                            </td>
                                            <?php
                                    echo '</tr>';
                                        }
                                        ?>

                                    </tbody>
                                </table>
                                <?php
                                $p = mysqli_query($link, "SELECT SUM(harga) as total from hub_layanan_notaservis WHERE servis_id = '" .$idservis. "'");
                                $res_p = mysqli_fetch_array($p);
                                $total = $res_p['total']; 
                                ?>
                                <b style="font-size: 20px;">TOTAL BIAYA LAYANAN = Rp. <?php echo number_format($total, 0, ',', '.'); ?>,-</b>
                            </div>
                            <div class="modal fade" id="edit" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Ubah Data Layanan</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%;">
                                                <div class="row">
                                                    <form method="POST" action="process.php?act=edit_lay" enctype="multipart/form-data">
                                                    <input type="hidden" name="idserv" />    
                                                    <input type="hidden" name="idlay" />
                                                    <label for="NomorResi">Nama Layanan </label><br>
                                                    <textarea class="form-control" style="width: 70%;" name="nama_lay" id="nama_lay" cols="30" rows="3"></textarea>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="NomorResi">Harga Layanan</label><br>
                                                    <input type="number" style="width: 20%;" class="form-control" name="harga_lay" id="harga_lay" />
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="edit_lay" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                            $('#edit').on('show.bs.modal', function(e) {
                                
                                var layan1 = $(e.relatedTarget).data('layan');
                                var nama1 = $(e.relatedTarget).data('nama');
                                var ser1 = $(e.relatedTarget).data('ser');
                                var hg1 = $(e.relatedTarget).data('hg');
                                
                                $(e.currentTarget).find('input[name="idserv"]').val(ser1);
                                $(e.currentTarget).find('input[name="idlay"]').val(layan1);
                                $("#nama_lay").html(nama1); 
                                $(e.currentTarget).find('input[name="harga_lay"]').val(hg1); 
                            });
                            </script>
                        </div>   
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </body>
</html>
