<?php
session_start();
require './db.php';

if(isset($_GET['id']))
{
    $notaa = $_GET['id'];
}
else
{

    // echo '<script language="javascript">';
    // echo 'document.location.href="masorder.php"';
    // echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
       <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                            <?php 
                            // echo $pengguna; 
                            ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
               <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pengembalian</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang </a>
                        </li>
                        <!--  <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li> -->
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h1 class="page-header">
                                <a href="maspemesanan.php"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i><br>Order</button></a>
                                Detail Transaksi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order
                                </li>
                            </ol>
                        </div>
                        <div class="col-sm-20">
                            <h2>Detail Transaksi untuk ID Nota <?php echo $notaa; ?> </h2>
                            <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                            <thead>
                                <tr >
                                    <th style="text-align: center;" >ID</th>
                                    <th style="text-align: center;" >NAMA BARANG</th>
                                    <th style="text-align: center;" >STATUS PEMBAYARAN</th>
                                    <th style="text-align: center;" >PEMBAYARAN</th>
                                    <th style="text-align: center;" >JUMLAH SEWA</th>
                                    <th style="text-align: center;" >TANGGAL AMBIL</th>
                                    <th style="text-align: center;" >DURASI SEWA</th>
                                     <th style="text-align: center;" >HARGA SEWA</th>
                                    <th style="text-align: center;" >EDIT | HAPUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include 'tanggal_indo.php';
                                $sql = "SELECT * FROM kamera a, hub_notasewa_dan_kamera h , notasewa n WHERE h.nota_id = '" .$notaa. "' AND h.kamera_id = a.id AND n.id = h.nota_id";
                                $result = mysqli_query($link, $sql);
                                if (!$result) {
                                    die("SQL Error:" . mysqli_error($link));
                                }
                                while ($row = mysqli_fetch_array($result)) {
                                echo '
                                <tr>';
                                echo "<td class='col-sm-0'>" . $row['id'] . "</th>";
                                echo "<td class='col-sm-2'>" . $row['namakamera'] . "</th>";
                                echo "<td class='col-sm-2'>" .$row['pelunasan']. "
                                <button type='button' class='btn btn-info btn-sm'
                                data-idk='" .$row['id']. "' 
                                data-valik='" .$row['pelunasan']. "'
                                data-toggle='modal' data-target='#validasipelunasan'>Validasi Pelunasan</button>";
                                

                                echo "<td class='col-sm-2'>Rp. " .number_format($row['nominaltransfer'], 0, ',', '.') . ",-</td>";
                                echo "<td class='col-sm-1'>" . $row['jmlsewa'] . "</th>";
                                echo "<td class='col-sm-2'>" . TanggalIndo($row['tgl_ambil']) . "</td>";
                                echo "<td class='col-sm-2'>" . $row['durasi'] . " JAM</th>";
                                echo "<td class='col-sm-2'>Rp. " .number_format($row['hargasewa'], 0, ',', '.') . ",-</td>";
                                echo "<td class='row1 col-sm-2'>

                                <button type='button' class='btn btn-primary btn-sm' 
                                data-mes='" .$row['kamera_id']. "' 
                                data-not='" .$row['nota_id']."' 
                                data-hs='" .$row['hargasewa']. "' 
                                data-har='" .$row['durasi']. "' 
                                data-jml='" .$row['jmlsewa']."' 
                                data-toggle='modal' data-target='#editbrg'> <span class='glyphicon glyphicon-pencil'></span></button>"; ?>

                                <a href='process.php?act=delmesinjual&idnota=<?php echo $row['nota_id'] ?>&idbar=<?php echo $row['kamera_id'] ?>'><button type='button' class='btn btn-danger btn-sm' onclick='return confirm("Apakah anda yakin untuk menghapus data?");'><span class='glyphicon glyphicon-trash'></span></button>
                               </td>
                                        <?php echo "</tr>";

                                }?>
                            </tbody>
                        </table>

                        <?php
                        $p = mysqli_query($link, "SELECT SUM(hargasewa*jmlsewa) as total from hub_notasewa_dan_kamera WHERE nota_id = '" .$notaa. "'");
                        $res_p = mysqli_fetch_array($p);
                        $total = $res_p['total']; 
                        ?>
                        <b style="font-size: 20px;">TOTAL BELANJA = Rp. <?php echo number_format($total, 0, ',', '.'); ?>,-</b>
                             </div>
                    <div class="modal fade" id="validasipelunasan" role="dialog">
                       <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Validasi Pembayaran</h4>
                    </div>
                   <div class="modal-body">
                    <div style="margin-left: 5%; margin-right: 5%;">
                    <div class="row">
                        <label for="NamaPenerima">Status Bukti </label><br>
                        <form method="POST" action="detil_order.php">
                        <input type="hidden" name="idnotas" id="idnotas">

                        <select id="status" name="status" class="form-control">
                            <option value="">Belum Diset</option>
                            <option value="belum lunas">Belum Lunas</option>
                            <option value="lunas">Lunas</option>
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <button type="submit" name="updts" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                        </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <script type="text/javascript"> 
    $('#validasipelunasan').on('show.bs.modal', function(e) {
        var idnot = $(e.relatedTarget).data('idk');
        var valida = $(e.relatedTarget).data('valik');
        
         
        $("#idnotas").val(idnot);    
        $("#status").val(valida);  
                                      
    });
    </script>
             <div class="modal fade" id="editbrg" role="dialog">
             <div class="modal-dialog" role="document">
              <!-- Modal content-->
             <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Barang</h4>
                </div>
                <div class="modal-body">
                <div style="margin-left: 5%;">  
                <div class="row">
                <div class="col-sm-8">
                <form method="POST" action="process.php?act=edithar">
                    <input type="hidden" name="bar" />
                    <input type="hidden" name="not" />
                    <label for="NamaPenerima">Harga Sewa </label><br>
                    <input type="number" name="hargas" class="form-control"></span><br><br>
                    <select class="form-control"  name="durasi"  required>
                    <option value="">Pilih Durasi Sewa</option>
                   <option value="6jam" <?php if($row['durasi'] == '6jam'){ echo 'selected'; } ?>>6 jam</option>
                   <option value="12jam" <?php if($row['durasi'] == '12jam'){ echo 'selected'; } ?>>12 jam</option>
                   <option value="24jam" <?php if($row['durasi'] == '24jam'){ echo 'selected'; } ?>>24 jam</option>
                </select><br><br>
                    <label for="NamaPenerima">Jumlah Sewa </label><br>
                    <input type="number" name="jumlas" class="form-control"></span>
                    </div><br>
                <div class="row">
                    <button type="submit" name="updt" class="btn btn-primary">Ubah</button>
                    </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
         <script type="text/javascript"> 
        $('#editbrg').on('show.bs.modal', function(e) {
        var mes = $(e.relatedTarget).data('mes');
        var not = $(e.relatedTarget).data('not');
        var hs = $(e.relatedTarget).data('hs');
        var har = $(e.relatedTarget).data('har');
        var jml = $(e.relatedTarget).data('jml');
                                 
         $(e.currentTarget).find('input[name="bar"]').val(mes);  
         $(e.currentTarget).find('input[name="not"]').val(not);
         $(e.currentTarget).find('input[name="hargas"]').val(hs);
         $(e.currentTarget).find('input[name="durasi"]').val(har);
         $(e.currentTarget).find('input[name="jumlas"]').val(jml);   
        });
        </script>
 </div>
</div>
</div>
            </div>
        </div>
    </body>
    </body>
</html>

 <?php 
 if(isset($_POST['updt']))
        {
            $bara = $_POST['bar'];
            $notaa = $_POST['not'];
            $hrg = $_POST['hargas'];
            $dur = $_POST['durasi'];
            $jml = $_POST['jumlas'];

            //update detil jual mesin
            $t = mysqli_query($link, "UPDATE hub_notasewa_dan_kamera SET hargasewa = '" .$hrg. "', durasi = '" .$dur. "', jmlsewa = '" .$jml. "' WHERE  nota_id = '" .$notaa. "' AND kamera_id = '" .$bara. "'");  
            //$t = mysqli_query($link, "UPDATE hub_nota_barang SET harga_jual = '" .$har. "' WHERE nota_id = '" .$notaa. "' AND barang_id = '" .$bara. "'");


            $p = mysqli_query($link, "SELECT SUM(hargasewa*jmlsewa) as total from hub_notasewa_dan_kamera WHERE nota_id = '" .$notaa. "'");
            $res_p = mysqli_fetch_array($p);
            $total = $res_p['total']; 

            //update total nota jual mesin
            //1. ambil sum dari total penjualan dari hub_nota_barang
            // $p = mysqli_query($link, "SELECT SUM(harga_jual) as total from hub_nota_barang WHERE nota_id = '" .$notaa. "'");
            // $res_p = mysqli_fetch_array($p);
            // $total = $res_p['total'];
            $y = mysqli_query($link,'UPDATE notasewa SET grandtotal = '.$total. ' WHERE id = "' .$notaa. '"');
            

            echo '<script language="javascript"> 
            var id="' .$notaa. '";
            alert("Data berhasil diubah")
          document.location.href="detil_order.php?id="+id
          </script>';
        }

if(isset($_POST['updts']))
{
    $notas = $_POST['idnotas'];
    $statuss = $_POST['status'];


    if ($statuss == 'belum lunas') {

     
        $r = mysqli_query($link, "UPDATE notasewa SET pelunasan = 'belum lunas' WHERE id = '" .$notas. "'");

        echo '<script language="javascript"> 
            var id="' .$notas. '";
            alert("Data berhasil diubah")
          document.location.href="detil_order.php?id="+id
          </script>';
    }
    else
    {
       
        $r = mysqli_query($link, "UPDATE notasewa SET pelunasan = 'lunas' WHERE id = '" .$notas. "'");

       echo '<script language="javascript"> 
            var id="' .$notas. '";
            alert("Data berhasil diubah")
        document.location.href="detil_order.php?id="+id
          </script>';
    }
}


 ?>
 
 