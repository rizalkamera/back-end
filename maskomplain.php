<?php
session_start();
require './db.php';

if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Komplain | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>


    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $pengguna; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Komplain
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-users"></i> Master Komplain
                                </li>
                            </ol>
                        </div>
                        <div class="col-lg-10">
                            <h2>Data Member yang tersedia</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr >
                                            <th >ID</th>
                                            <th >DIAJUKAN OLEH</th>
                                            <th >TANGGAL PENGAJUAN</th>
                                            <th >MESIN </th>
                                            <th >DESKRIPSI</th>
                                            <th >ALAMAT</th>
                                            <th >NOTA SERVIS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT b.nama_barang, k.id_komplain, k.tanggal, k.deskripsi as keluhan, k.alamat, p.nama_pel, s.id_servis, k.notaJual_id, k.barang_id FROM barang b, pelanggan p, komplain k LEFT JOIN nota_servis s ON s.komplain_id = k.id_komplain WHERE b.id_barang = k.barang_id AND k.pelanggan_id = p.id_pel AND b.hapuskah = '0' AND p.hapuskah = '0'";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<tr class= "row1">';
                                            echo "<th >" . $row['id_komplain'] . "</th>";
                                            echo "<td class='row1 col-sm-2'>" . $row['nama_pel'] . "</td>";
                                            echo "<td class='row1 col-sm-2'>" . $row['tanggal'] . "</td>";
                                            echo "<td class='row1 col-sm-2'>" . $row['nama_barang'] . "</td>";

                                            echo "<td class='row1 col-sm-1'><button type='button' class='btn btn-primary btn-sm' data-des='" .$row['keluhan']. "' data-tes='" .$row['nama_barang']. "' data-toggle='modal' data-target='#showdesk'>Detil Keluhan</button></td>";                                            
                                            echo "<td class='row1 col-sm-4'>" . $row['alamat'] . "</td>";

                                            if($row['id_servis'] == "")
                                            {
                                                echo "<td class='row1 col-sm-1'>

                                            <button type='button' class='btn btn-info btn-sm' data-kom='" .$row['id_komplain']. "' data-bar='" .$row['barang_id']. "' data-nota='" .$row['notaJual_id']. "'data-tgl='".$row['tanggal']."' data-toggle='modal' data-target='#insservis'>Buat Nota Servis</button></td>";
                                            }
                                            else
                                            {
                                                echo "<td class='row1 col-sm-1'>
                                            <a href='masservis.php'><button type='button' class='btn btn-info btn-sm'>Master Nota Servis</button></td>";
                                            }
                                            
                                           echo "</tr>";
                                        }   ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="modal fade" id="showdesk" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Keluhan Pelanggan</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <span class="keluh"></span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#showdesk').on('show.bs.modal', function(e) {
                                var des = $(e.relatedTarget).data('des');

                                $(".keluh").html(des);                          
                            });
                            </script>

                            <div class="modal fade" id="insservis" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Input Nota Servis</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="maskomplain.php">
                                                    <input type="hidden" name="tgl_kom" value="">
                                                    <input type="hidden" name="idkom" value="">
                                                    <input type="hidden" name="idbar" value="">
                                                    <input type="hidden" name="idnot" value="">

                                                    <label for="NamaPenerima">Karyawan </label><br>
                                                    <select name="karyawan" class="form-control">
                                                        <option value="">---Pilih Karyawan---</option>
                                                        <?php
                                                        $r = mysqli_query($link,"SELECT id,nama from karyawan where username is null");
                                                        while($t = mysqli_fetch_array($r))
                                                        {
                                                            echo '<option value="' .$t['id']. '">' .$t['nama']. '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="row">
                                                    <button type="submit" name="add" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk menyimpan data?');">Simpan</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('#insservis').on('show.bs.modal', function(e) {

                                var tgl1 = $(e.relatedTarget).data('tgl');
                                var kom1 = $(e.relatedTarget).data('kom');
                                var bar1 = $(e.relatedTarget).data('bar');
                                var nota1 = $(e.relatedTarget).data('nota');
                                
                                $(e.currentTarget).find('input[name="tgl_kom"]').val(tgl1);
                                $(e.currentTarget).find('input[name="idkom"]').val(kom1);
                                $(e.currentTarget).find('input[name="idbar"]').val(bar1);
                                $(e.currentTarget).find('input[name="idnot"]').val(nota1);
                            });
                            </script>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>


    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    </body>
</html>


<?php

if (isset($_POST['add'])) 
{
    
    $tgl_kom1 = $_POST['tgl_kom'];
    $idkom1 = $_POST['idkom'];
    $idbar1 = $_POST['idbar'];
    $idnot1 = $_POST['idnot'];
    $karyawan1 = $_POST['karyawan'];
    $gar = '';

    //ambil tanggal system
    date_default_timezone_set('Asia/Bangkok');
    $tanggalan  = date('Y-m-d H:i:s');


    //cek apakah garansi barang masih berlaku
    //1. ambil tanggal berlaku garansi
    $t = mysqli_query($link, "SELECT * FROM hub_nota_barang WHERE nota_id = '".$idnot1."' AND barang_id = '" .$idbar1. "'");

    if(!$t)
    {
        echo mysqli_error($link);
    }

    $res_t = mysqli_fetch_array($t);
    $tgl_gar = $res_t['tglBerlaku_garansi'];

    //2. bandingkan
    if($tgl_gar >= $tgl_kom1)
    {
        $gar = 'gratis';
    }
    else
    {
        $gar = 'bayar';
    }


    $r = mysqli_query($link, "INSERT INTO nota_servis (tanggal,jenis_servis,status,grand_total,hapuskah,komplain_id,karyawan_id,barang_id,notaJual_id)
        VALUES ('$tanggalan','$gar','Diterima',0,0,'$idkom1','$karyawan1','$idbar1','$idnot1')");

    if(!$r)
    {
        echo mysqli_error($mycon);
    }
    else
    {
        echo '<script language="javascript"> 
          alert("Nota Servis berhasil dibuat.")
          document.location.href="maskomplain.php"
          </script>';
    }
}

?>
