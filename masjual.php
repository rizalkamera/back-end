<?php
session_start();
require './db.php';

// if(isset($_SESSION['admin']))
// {
//     $admin = $_SESSION['admin'];

//     if(!isset($_SESSION['admin_loggedIn']))
//     {
//         echo '<script language="javascript">';
//         echo 'document.location.href="login.php"';
//         echo '</script>';
//     }
//     else
//     {
//         $pengguna = $_SESSION['admin_loggedIn'];
//     }
// }
// else
// {
//     echo '<script language="javascript">';
//     echo 'window.alert("Anda harus login terlebih dahulu!");';
//     echo 'document.location.href="../login.php"';
//     echo '</script>';
// }
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                            <?php 
                            // echo $pengguna; 
                            ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                 <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-camera"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                                 <li>
                                    <a href="masterinputlelang.php"> Lelang</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pegembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                        <li>
                            <a href="maslelang.php"><i class="fas fa-hammer "></i> Master Lelang </a>
                        </li>
                        

                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                         <li>
                            <a href="laporan.php"><i class="fa fa-fw fa-edit"></i>Laporan</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
>
            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Penjualan
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order
                                </li>
                            </ol>
                        </div>
                        <div class="col-sm-6">
                            <h2>Data Order yang Tersedia Saat ini:</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr >
                                            <th >ID NOTA</th>
                                            <th >NAMA PELANGGAN</th>
                                            <th >DETIL JUAL</th>
                                            <th >HAPUS</th>
                                            <th >CETAK NOTA</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';
                                
                                        // $sql = "select *, b.norekening as rek_user from user a, notasewa b, bank c where a.id = b.user_id and b.bankid = c.idbank"; 
                                     $sql = "select b.id, b.namaproduk, b.kondisikamera, b.kategori, b.deskripsi, b.hargajual, b.foto, b.foto2, b.foto3, b.status, u.nama from user as u, postinganjualkamera as b where b.iduser = u.id";
                                        // $sql = "SELECT  p.namaproduk, p.kondisikamera, p.kategori, p.hargajual, p.foto, p.status, h.nama FROM postinganjualkamera p, user h WHERE id";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) 
                                        {
                                            echo '
                                            <tr class= "row1">


                                            <th class="row1 col-sm-1" style="text-align: center;">' . $row['id'] . '</th>
                                                <td class="row1 col-sm-1" style="text-align: center;">'.$row['nama'].'  </td>

                                                 <td class="row1 col-sm-1" style="text-align: center;">'.
                                                //     <form method="POST" action="detil_jual.php">
                                                //     <input type="hidden" name="nota" value="' .$row['id']. '" />'.

                                                    //jika status = menunggu barang sewa, maka button di-disable
                                                    //selain itu, button enable
                                            "<a href='detil_jual.php?id=" .$row['id']. "'><button type='button' class='btn btn-info btn-sm'>Lihat Detil</button>".
                                            '<br>'.'<br>
                                            <a href="images/jual/' .$row['foto']. '"><button type="button" class="btn btn-info btn-sm">Lihat Gambar 1</button></a><br></br>';

                                            if($row['foto2'] != '')
                                            {
                                                echo '<a href="images/jual/' .$row['foto2']. '"><button type="button" class="btn btn-info btn-sm">Lihat Gambar 2</button></a><br></br>';
                                            }
                                            if($row['foto3'] != '')
                                            {
                                                echo '<a href="images/jual/' .$row['foto3']. '"><button type="button" class="btn btn-info btn-sm">Lihat Gambar 3</button></a><br></br>';
                                            }

                                            echo 
                                                    //jika status = menunggu validasi pembayaran, maka button di-enable
                                                    //selain itu, button di-disable
                                                    '<button type="button" class="btn btn-info btn-sm" data-idn="' .$row['id']. '" data-vali="' .$row['status']. '"data-toggle="modal" data-target="#buktitrf">Validasi Posting</button>
                                                </td>';

                                               echo '<td class="row1 col-sm-2">

                                           <a href="process.php?act=deletejual&idjual=' .$row['id'].'"<button type="button" class="btn btn-danger btn-sm"'; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data?');"><span class="glyphicon glyphicon-trash"></span></button></a>
                                        </td>
                                        <?php echo'
                                                

                                                <td class="row1 col-sm-1" style="text-align: center;">'.

                                                    //jika status = selesai, maka button di-enable
                                                    //selain itu, button di-disable
                                                    '<form method="POST" action="cetak_notaJual.php">
                                                    <input type="hidden" name="idnota" value="' .$row['id']. '" />
                                                    <button type="submit" class="btn btn-success btn-sm" name="cetak" disabled><i class="fa fa-print"></i></button>
                                                    </form>
                                                </td>
                                            </tr>';

                                           
                                            
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                           
                            <div class="modal fade" id="buktitrf" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Bukti Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Status Bukti </label><br>
                                                    <form method="POST" action="masjual.php">
                                                    <input type="hidden" name="idnotaa" id="idnota">
                                                   

                                                    <select id="statu" name="statu" class="form-control">
                                                        <option value="">Belum Diset</option>
                                                        <option value="ditolak">Tidak Valid</option>
                                                        <option value="ditampilkan">Valid</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="updt" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#buktitrf').on('show.bs.modal', function(e) {
                                var valida = $(e.relatedTarget).data('vali');
                                var idnot = $(e.relatedTarget).data('idn');
                                var idpel = $(e.relatedTarget).data('pel');
                                
                                // $("#tst").val(valida);  
                                $("#statu").val(valida);  
                                $("#idnota").val(idnot);
                                $("#idpela").val(idpel); 
                                                   
                            });
                            </script>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php

if(isset($_POST['updt']))
{
   $nota = $_POST['idnotaa'];
    $status = $_POST['statu'];

    if($status == '')
    {
        //do nothing, redirect ke masorder.php
        echo '<script language="javascript"> 
          document.location.href="masjual.php"
          </script>';
    }
    elseif ($status == 'ditolak') {

        echo '<script language="javascript"> 
          alert("status 0")
          </script>';

        //update status bukti valid jadi 0
        $r = mysqli_query($link, "UPDATE postinganjualkamera  SET status = 'ditolak' WHERE id = '" .$nota. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
         document.location.href="masjual.php"
          </script>';
    }
    else
    {
        echo '<script language="javascript"> 
          alert("status 1")
          </script>';
        //update status bukti valid jadi 1
        //update status kiriman jadi 'proses pengiriman'
        $r = mysqli_query($link, "UPDATE postinganjualkamera SET status = 'ditampilkan' WHERE id = '" .$nota. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="masjual.php"
          </script>';
    }
}


?>
