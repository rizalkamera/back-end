<?php
session_start();
require './db.php';

if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}
if(isset($_GET['barang']))
{
    $idbar = $_GET['barang'];
}
else
{
    echo '<script language="javascript">';
    echo 'document.location.href="masmesin.php"';
    echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Barang - Mesin Kopi | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $pengguna; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h1 class="page-header">
                                <a href="masmesin.php"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i><br>Mesin Kopi</button></a>
                                Daftar Serial Number Mesin Kopi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order - Mesin Kopi
                                </li>
                            </ol>
                        </div>
                        <div class="container">
                            <div class="col-lg-6">
                            <form action="process.php?act=addSN" method="post" class="form" role="form">
                                <input type="hidden" name="idbar" value="<?php echo $idbar ?>">
                                <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <label for="desResep">Serial Number</label><br>
                                        <input type="text" class="form-control" name="esen" />
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-4">
                                        <label for="gambarB">Status</label> </br>
                                            <select name="stat" class="form-control">
                                                <option value="tersedia">Tersedia</option>
                                                <option value="terjual">Terjual</option>
                                            </select>
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <?php
                            $d = mysqli_query($link, "SELECT * FROM barang WHERE id_barang = '" .$idbar. "'");
                            $res_d = mysqli_fetch_array($d);
                            
                            ?>
                            <h2>Daftar Serial Number untuk Barang <?php echo $res_d['nama_barang']; ?> </h2>
                            <div class="table-responsive col-sm-6">
                                <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                    <thead style="text-align: center;">
                                        <tr style="text-align: center;">
                                            <th >SERIAL NUMBER</th>
                                            <th >STATUS</th>
                                            <th >EDIT | HAPUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT * from serial_number WHERE barang_id = " .$idbar;
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                        echo "<td class='col-sm-4'>" . $row['serial_number'] . "</th>";
                                        echo "<td class='col-sm-2'>" .$row['status']. "</td>";
                                        echo "<td class='row1 col-xs-2'>
                                                <button type='button' class='btn btn-primary btn-sm' data-idbar='" .$row['barang_id']. "' data-sn='" .$row['serial_number']. "' data-st='" .$row['status']. "' data-toggle='modal' data-target='#editsn'> <span class='glyphicon glyphicon-pencil'></span></button>

                                                <a href='process.php?act=deleteSN&idbar=" .$row['barang_id']."&sn=" .$row['serial_number']."'<button type='button' class='btn btn-danger btn-sm' "; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data ?');"><span class='glyphicon glyphicon-trash'></span></button>
                                                  </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>


                            <div class="modal fade" id="editsn" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Serial Number</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%;">  
                                                <div class="form-group row">
                                                    <form method="POST" action="process.php?act=editSN">
                                                    <input type="hidden" name="idbar" />
                                                    <input type="hidden" name="sn_bar">
                                                    <label for="isiResep">Serial Number </label><br>
                                                    <input type="text" name="sn" style="width: 50%;" class="form-control" />
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="isiResep">Status </label><br>
                                                    <select id="sta" name="statuss" class="form-control" style="width: 50%;">
                                                        <option value="tersedia">Tersedia</option>
                                                        <option value="terjual">Terjual</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="sub_sn" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('#editsn').on('show.bs.modal', function(e) {
                                
                                var idb = $(e.relatedTarget).data('idbar');
                                var ser = $(e.relatedTarget).data('sn');
                                var stt = $(e.relatedTarget).data('st');
                                
                                $(e.currentTarget).find('input[name="idbar"]').val(idb);
                                $(e.currentTarget).find('input[name="sn_bar"]').val(ser);
                                $(e.currentTarget).find('input[name="sn"]').val(ser);
                                $("#sta").val(stt);
                            });
                            </script>
                        </div>   
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </body>
</html>

