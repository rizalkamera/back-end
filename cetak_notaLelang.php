<?php 
//batas awal dokumen PDF yang akan tercetak
ob_start(); 

?>
<html>
<head>
    <title>Cetak PDF</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>


<?php
// Load file koneksi.php
include "db.php";
if(isset($_POST['cetak']))
{
    $idle = $_POST['idlelang'];

    $w = mysqli_query($link, "SELECT * FROM lelang_resep l , pelanggan p WHERE l.pelanggan_id = p.id_pel AND l.id_lelang = " .$idle. "");
    $res_w = mysqli_fetch_array($w);
    ?> 
    <div class="container" style="margin-top: 5%;">

        <div class="row">
            <div class="col-sm-8">
                <h1 style="text-align: center;">Bukti Transaksi Lelang</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <h3>Informasi Lelang</h3><br>

                <span>ID Lelang</span><br> 
                <strong> <?php echo $res_w['id_lelang']; ?> </strong><br><br>

                <span>Mulai Lelang</span><br>
                <strong> <?php echo $res_w['waktu_mulai']; ?> </strong><br><br>

                <span>Selesai Lelang</span><br>
                <strong> <?php echo $res_w['waktu_selesai']; ?></strong><br><br>

                <span>Harga Awal</span><br>
                <strong> <?php echo $res_w['harga_awal']; ?></strong><br><br>
            </div>
            <div class="col-sm-4" style="margin-top: 6.5%;">
                <span>Harga Tertinggi yang diajukan </span><br>
                <strong> <?php echo $res_w['harga_tertinggi']; ?></strong><br><br>

                <span>Tanggal Pengajuan</span><br>
                <strong> <?php echo $res_w['tgl_penawaran']; ?></strong><br><br>

                <span>Email Tujuan Pengiriman Resep</span><br>
                <strong> <?php echo $res_w['email_tujuan']; ?></strong><br><br>

                <span>Dimenangkan Oleh</span><br>
                <strong style="font-size: 18px;">
                 <?php echo $res_w['id_pel']; ?> - 
                 <?php echo $res_w['nama_pel']; ?>
                </strong>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h3>Resep yang dimenangkan</h3><br>

                <span>Nama Resep </span><br>
                <strong> <?php echo $res_w['nama_resep']; ?></strong><br><br>

                <span>Jenis Resep</span><br>
                <strong> <?php echo $res_w['jenis']; ?></strong><br><br>

                <span>Deskripsi</span><br>
                <strong> <?php echo $res_w['deskripsi']; ?></strong><br><br>
                
            </div>
        </div>
    </div>
</body>
</html>
        <?php
}
else
{
    echo '<script language="javascript"> 
      alert("tidak ada parameter")
      </script>';
}



//batas akkhir dokumen PDF
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Nota Lelang' .$idle. '.pdf', 'D');
?>
