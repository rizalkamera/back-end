<?php
session_start();
require './db.php';
?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Pelanggan | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
         <script src='https://kit.fontawesome.com/a076d05399.js'></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                  <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-camera"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                                 <li>
                                    <a href="masterinputlelang.php"> Lelang</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pegembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                        <li>
                            <a href="maslelang.php"><i class="fas fa-hammer "></i> Master Lelang </a>
                        </li>
                        

                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                         <li>
                            <a href="laporan.php"><i class="fa fa-fw fa-edit"></i>Laporan</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Pelanggan
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-users"></i> Master Pelanggan
                                </li>
                            </ol>
                        </div>
                        <div class="col-lg-8">
                            <h2>Data Member yang tersedia</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr >
                                            <th >ID</th>
                                            <th >NAMA MEMBER</th>
                                            <th >ALAMAT MEMBER</th>
                                            <th >EMAIL</th>
                                            <th >NO. HP</th>
                                            <th >HAPUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT id, nama, alamat, email, notlp FROM `user` WHERE hapuskah = '0'";
                                        //$sql = "SELECT id, nama,alamat,email,no tlp FROM `user` WHERE hapuskah = '0'";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<tr class= "row1">';
                                            echo "<th >" . $row['id'] . "</th>";
                                            echo "<td class='row1 col-xs-3'>" . $row['nama'] . "</td>";
                                             echo "<td class='row1 col-sm-8'>" . $row['alamat'] . "</td>";
                                            echo "<td class='row1 col-sm-4'>" . $row['email'] . "</td>";
                                            echo "<td class='row1 col-sm-8'>" . $row['notlp'] . "</td>";
                                            echo "<td class='row1 col-sm-8'>
                                            
                                            <a href='process.php?act=deletepel&idpel=" .$row['id']."'<button type='button' class='btn btn-danger btn-sm' "; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data?');"><span class='glyphicon glyphicon-trash'></span></button></a>
                                            
                                            </td>
                                           <?php echo "</tr>";
                                        }   ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>


                <!-- /#wrapper -->

                <!-- jQuery -->
                <script src="js/jquery.js"></script>
                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="js/bootstrap.min.js"></script>

                </body>
                </html>


