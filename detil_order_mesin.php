<?php
session_start();
require './db.php';

if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}
if(isset($_GET['id']))
{
    $idnota = $_GET['id'];
}
// else
// {
//     echo '<script language="javascript">';
//     echo 'document.location.href="masorder_mesin.php"';
//     echo '</script>';
// }
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $pengguna; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h1 class="page-header">
                                <a href="masorder_mesin.php"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i><br>Order</button></a>
                                Detail Transaksi Mesin Kopi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order - Mesin Kopi
                                </li>
                            </ol>
                        </div>
                        <div class="container">
                            <div class="col-lg-6">
                            <form action="process.php?act=tmbhjualmesin" method="post" class="form" role="form">
                                <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <label for="desResep">Mesin</label><br>
                                        <input type="hidden" name="nota" value=" <?php echo $idnota ?> ">
                                        <select name="mesin" id="mesin" class="form-control" required>
                                            <option value="" selected>---Pilih Produk---</option>
                                            <?php 
                                            $r = mysqli_query($link, "SELECT * FROM barang WHERE kategori_id = '2' AND hapuskah = '0'");

                                            while($e = mysqli_fetch_array($r))
                                            {
                                                echo '<option value="' .$e['id_barang']. '">' .$e['nama_barang']. '</option>';
                                            }
                                            ?>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <label for="desResep">Nomor Seri Mesin</label><br>
                                        <select name="sn" id="sn" class="form-control" required>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-4">
                                        <label for="gambarB">Harga Jual</label> </br>
                                            <input type="number" class="form-control" name="harga" required>
                                        </label>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan" onclick="return confirm('Apakah anda yakin untuk menambah data?');">
                                    </fieldset>
                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <h2>Detail Transaksi untuk ID Nota <?php echo $idnota; ?> </h2>
                            <div class="table-responsive col-sm-12">
                                <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                    <thead style="text-align: center;">
                                        <tr style="text-align: center;">
                                            <th >NAMA BARANG</th>
                                            <th >NOMOR SERI</th>
                                            <th >HARGA JUAL</th>
                                            <th >TANGGAL BERLAKU GARANSI</th>
                                            <th >STATUS TERKIRIM</th>
                                            <th >EDIT | HAPUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';
                                        $sql = "SELECT * FROM barang b, hub_nota_barang h WHERE h.nota_id = '" .$idnota. "' AND h.barang_id = b.id_barang AND b.hapuskah='0'";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                    echo '
                                        <tr>';
                                        echo "
                                            <td class='col-sm-3'>" . $row['nama_barang'] . "</th>";
                                        echo "
                                            <td class='col-sm-2'>" . $row['s_n'] . "</th>";
                                        echo "
                                            <td class='col-sm-2'>Rp. " .number_format($row['harga_jual'], 0, ',', '.') . ",-</td>";
                                        echo "
                                            <td class='col-sm-2'>".TanggalIndo($row['tglBerlaku_garansi']). "</td>";

                                        if($row['sisa_kirim'] == $row['jml_jual'])
                                        {
                                        echo "
                                            <td class='col-sm-2'>Belum Dikirim
                                            <button type='button' class='btn btn-primary btn-sm' data-mes='" .$row['barang_id']. "' data-not='" .$row['nota_id']. "' data-sisa='" .$row['sisa_kirim']. "' data-toggle='modal' data-target='#editkirim'> <span class='glyphicon glyphicon-pencil'></span></button>
                                            </td>";
                                        }
                                        else
                                        {
                                         echo "
                                            <td class='col-sm-2'>Sudah Dikirim<br>
                                         <button type='button' class='btn btn-primary btn-sm' data-mes='" .$row['barang_id']. "' data-not='" .$row['nota_id']. "' data-sisa='" .$row['sisa_kirim']. "' data-toggle='modal' data-target='#editkirim'> <span class='glyphicon glyphicon-pencil'></span></button>
                                            </td>";  
                                        }

                                        echo "
                                            <td class='col-sm-1'>
                                                <button type='button' class='btn btn-primary btn-sm' data-mes='" .$row['barang_id']. "' data-not='" .$row['nota_id']. "' data-har='" .$row['harga_jual']. "' data-toggle='modal' data-target='#editharga'> <span class='glyphicon glyphicon-pencil'></span></button>"; ?>

                                                <a href='process.php?act=delmesinjual&idnota=<?php echo $row['nota_id'] ?>&idbar=<?php echo $row['barang_id'] ?>&sn= <?php echo $row['s_n'] ?>'><button type='button' class='btn btn-danger btn-sm' onclick='return confirm("Apakah anda yakin untuk menghapus data?");'><span class='glyphicon glyphicon-trash'></span></button>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                                <?php
                                $p = mysqli_query($link, "SELECT SUM(harga_jual) as total from hub_nota_barang WHERE nota_id = '" .$idnota. "'");
                                $res_p = mysqli_fetch_array($p);
                                $total = $res_p['total']; 
                                ?>
                                <b style="font-size: 20px;">TOTAL BELANJA = Rp. <?php echo number_format($total, 0, ',', '.'); ?>,-</b>
                            </div>
                            <script>
                                $("#mesin").change(function(){
                                
                                    // variabel dari nilai combo box spk
                                    var id_mesin = $("#mesin").val();

                                    // alert(id_mesin);
                                    
                                    // mengirim dan mengambil data
                                    $.ajax({
                                        type: "POST",
                                        url: "cari_sn.php",
                                        data: "mesin="+id_mesin,
                                        success: function(msg){
                                            
                                            // jika tidak ada data
                                            if(msg == ''){
                                                window.alert('tidak ada barang yang tersedia');
                                            }
                                            
                                            // jika dapat mengambil data,, tampilkan di combo box bahan
                                            else{
                                                $("#sn").html(msg);                                                      
                                            }
                                        }
                                    });     
                                });
                            </script>

                            <div class="modal fade" id="editharga" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Detail Jual</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="process.php?act=edithar">
                                                    <input type="hidden" name="bar" />
                                                    <input type="hidden" name="not" />
                                                    <label for="NamaPenerima">Harga Jual </label><br>
                                                    <input type="number" name="harga" class="form-control"></span>
                                                </div>
                                                <div class="form-group row">
                                                    <input type="submit" class="btn btn-info" name="updt" value="Ubah" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#editharga').on('show.bs.modal', function(e) {
                                
                                var mes = $(e.relatedTarget).data('mes');
                                var not = $(e.relatedTarget).data('not');
                                var har = $(e.relatedTarget).data('har');
                                 
                                $(e.currentTarget).find('input[name="bar"]').val(mes);  
                                $(e.currentTarget).find('input[name="not"]').val(not);
                                $(e.currentTarget).find('input[name="harga"]').val(har);                                 
                            });
                            </script>

                            <div class="modal fade" id="editkirim" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Status Kirim</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="process.php?act=statuskirim">
                                                    <input type="hidden" name="bar" />
                                                    <input type="hidden" name="not" />
                                                    <label for="NamaPenerima">Status Barang </label><br>
                                                    <select name="statuss" id="statuss" class="form-control">
                                                        <option value="">--Pilih Status--</option>
                                                        <option value="1">Belum Dikirim</option>
                                                        <option value="0">Sudah Dikirim</option>
                                                    </select>
                                                </div>
                                                <div class="form-group row">
                                                    <input type="submit" class="btn btn-info" name="gnti" value="Ubah" onclick="return confirm('Apakah anda yakin untuk mengubah?');">
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#editkirim').on('show.bs.modal', function(e) {
                                
                                var mes = $(e.relatedTarget).data('mes');
                                var not = $(e.relatedTarget).data('not');
                                var sis = $(e.relatedTarget).data('sisa');
                                 
                                $(e.currentTarget).find('input[name="bar"]').val(mes);  
                                $(e.currentTarget).find('input[name="not"]').val(not);
                                $("#statuss").val(sis);                                 
                            });
                            </script>
                        </div>   
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </body>
</html>