<?php
session_start();
require './db.php';

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                            <?php 
                            // echo $pengguna; 
                            ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
             <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-camera"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Asesoris</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pengembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fas fa-hammer "></i> Master Lelang </a>
                        </li>
                        
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Pemesanan Barang
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Pemesanan Barang
                                </li>
                            </ol>
                        </div>
                        <div class="col-sm-25">

                            <h2>Data Pemesanan yang Tersedia Saat ini:</h2>

                            <a href="ordermanual.php"><button  type="button" class="btn btn-info btn-sm" >Tambah Pesan Manual</button></a><br></br>

                            <div class="table-responsive">
                                <table  class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr >
                                            <th style="text-align: center;">ID</th>
                                            <th style="text-align: center;" >NAMA PELANGGAN</th>
                                            <th style="text-align: center;">TANGGAL PESAN</th>
                                            <th style="text-align: center;" >DETIL TRANSAKSI</th>
                                            <th style="text-align: center;" >GRAND TOTAL</th>
                                            <th style="text-align: center;" >STATUS PESANAN</th>
                                            <th style="text-align: center;" >DATA TRANSFER</th>
                                            <th style="text-align: center;">CETAK NOTA</th>
                                        </tr>
                                    </thead>
                    <tbody>
                        <?php
                        include 'tanggal_indo.php';
                        // $sql = "SELECT
                        // CASE
                        //  WHEN n.user_id = 0 THEN n.namapenyewa
                        //  ELSE u.nama
                        // END AS nama_penyewa,n.id, n.tanggalpesan, n.grandtotal,n.statusnota, n.statusnota, n.pelunasan, n.buktitransfer,n.norekening, n.namapemilik, n.nominaltransfer, n.statuspesanan, n.statuskembali
                        // ,b.namabank, b.norekening as rek_user
                        // FROM notasewa n left join user u on n.user_id = u.id left join bank b on n.bankid = b.idbank join hub_notasewa_dan_kamera h on n.id = h.nota_id join kamera k on h.kamera_id = k.id";

                        $sql = "SELECT *,
                                CASE
                                 WHEN n.user_id = 0 THEN n.namapenyewa
                                 ELSE u.nama
                                END AS nama_penyewa,n.id, n.norekening as rek_user FROM notasewa n left join user u on u.id = n.user_id left join bank b on n.bankid = b.idbank left join hub_notasewa_dan_kamera h on h.kamera_id = n.id";
                        //$sql = "select *, b.norekening as rek_user from user a, notasewa b, bank c, hub_notasewa_dan_kamera h where (a.id = b.user_id and b.bankid = c.idbank)"; 
                        // $sql = "select *, b.norekening as rek_user from user a, notasewa b, bank c, hub_notasewa_dan_kamera h, Kamera k where a.id = b.user_id and b.bankid = c.idbank and  b.id = h.nota_id and k.id = h.kamera_id"; 
                        $result = mysqli_query($link, $sql);
                        if (!$result) {
                            die("SQL Error:" . $sql);
                        }
                        while ($row = mysqli_fetch_array($result)) 
                        {
                                //STATUS NOTA
                                //menunggu barang sewa = pelanggan masih memilih barang sewa untuk dimasukkan ke keranjang sewa
                                //menunggu data sewa = pelanggan belum mengisi form data penyewa 
                                //menunggu data pembayaran = pelanggan belum mengisi data pembayaran
                                //menunggu validasi pembayaran = pelanggan sudah mengisi data pembayaran dan menunggu validasi dari admin
                                //selesai = proses booking sudah selesai dan pelanggan dapat membuat transaksi sewa yang baru lagi

                                echo '<tr class= "row1">
                                <th class="row1 col-sm-1" style="text-align: center;">' . $row['id'] . '</th>
                                <td class="row1 col-sm-2" style="text-align: center;">' . $row['nama_penyewa'] . '</td>
                                <td class="row1 col-sm-2" style="text-align: center;">' . $row['tanggalpesan'] . '</td>

                                <td class="row1 col-sm-2" style="text-align: center;">'.
                                 
                                    "<a href='detil_order.php?id=" .$row['id']. "'><button type='button' class='btn btn-info btn-sm'>Lihat Detil</button></a>".
                                '</td>
                                <td class="row1 col-sm-2" style="text-align: center;">Rp.' .number_format($row['grandtotal'], 0, ',', '.') .',-' .'</td>
                                <td class="row1 col-sm-2" style="text-align: center;">' . $row['statusnota'] . 
                                    '</td>
                                <td class="row1 col-sm-3" style="text-align: center;">';

                                if($row['statusnota'])
                                {
                                    echo
                                    '<button type="button" class="btn btn-info btn-sm" data-bank="'.$row['namabank'].'" data-norek="'.$row['rek_user'].'" data-pmilik="'.$row['namapemilik'].'" data-nom="'.$row['nominaltransfer'].'" data-toggle="modal" data-target="#viewtrf" >Detail</button><br></br>'.

                                    '<a href="images/upload_bukti/' .$row['buktitransfer']. '"><button type="button" class="btn btn-info btn-sm">Lihat Gambar</button></a><br></br>'.
                                    '<button type="button" class="btn btn-info btn-sm" data-idn="' .$row['id']. '" data-vali="' .$row['statuspesanan']. '"data-toggle="modal" data-target="#buktitrf"  >Validasi Bukti</button>';
                                }
                                 else if($row['statusnota'] == null)
                                {
                                    echo 'Lunas';                                                   
                                }
                                    
                                echo '</td>

                                <td class="row1 col-sm-3" style="text-align: center;">'.
                                    '<form method="POST" action="cetak_notaJual.php">
                                    <input type="hidden" name="idnota" value="' .$row['id']. '" />
                                    <button type="submit" class="btn btn-success btn-sm" name="cetak"><i class="fa fa-print"></i></button><br></br>'.
                                    '<button type="button" class="btn btn-info btn-sm" data-idk="' .$row['id']. '" data-valik="' .$row['statuskembali']. '"data-toggle="modal" data-target="#validasikamerakembali"> Kamera Kembali</button>
                                    </form>
                                </td>
                            </tr>';
                        }
                        ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="viewtrf" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detail Data Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Bank Tujuan Transfer </label><br>
                                                    <span class="bank"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="NoHPPenerima">Nomor Rekening </label><br>
                                                    <span class="no_rek"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="AlamatPenerima">Nama Pemilik Rekening </label><br>
                                                    <span class="nama_pmilik"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="KodePos">Nominal Transfer </label><br>
                                                    Rp. <span class="nominal"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 

                            function currency(str) {
                                var reverse = str.toString().split('').reverse().join(''),
                                ribuan  = reverse.match(/\d{1,3}/g);
                                ribuan  = ribuan.join('.').split('').reverse().join('');

                                return ribuan;
                            }

                            $('#viewtrf').on('show.bs.modal', function(e) {
                            
                                var bank1 = $(e.relatedTarget).data('bank');
                                var norek1 = $(e.relatedTarget).data('norek');
                                var pmilik1 = $(e.relatedTarget).data('pmilik');
                                var nom1 = $(e.relatedTarget).data('nom');

                                $(".bank").html(bank1);
                                $(".no_rek").html(norek1);
                                $(".nama_pmilik").html(pmilik1);
                                $(".nominal").html(currency(nom1)+',-');
                                
                                });
                            </script>
                          
                        
                            <script type="text/javascript"> 

                            function currency(str) {
                                var reverse = str.toString().split('').reverse().join(''),
                                ribuan  = reverse.match(/\d{1,3}/g);
                                ribuan  = ribuan.join('.').split('').reverse().join('');

                                return ribuan;
                            }

                            $('#viewkiriman').on('show.bs.modal', function(e) {
                            
                                var tk = $(e.relatedTarget).data('tk');
                                var np = $(e.relatedTarget).data('np');
                                var nohp = $(e.relatedTarget).data('nohp');
                                var almt = $(e.relatedTarget).data('almt');
                                var kodeP = $(e.relatedTarget).data('kodepos');
                                var biaya = $(e.relatedTarget).data('biaya');

                                $(".tipekrm").html(tk);
                                $(".namapen").html(np);
                                $(".no_hp").html(nohp);
                                $(".almat").html(almt);
                                $(".kodepos").html(kodeP);
                                $(".ongkir").html(currency(biaya)+',-');
                                
                                });
                            </script>

                            <div class="modal fade" id="buktitrf" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Bukti Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Status Bukti </label><br>
                                                    <form method="POST" action="maspemesanan.php">
                                                    <input type="hidden" name="idnotaa" id="idnota">
                                                    
                                                    <!-- <input type="text" id="tst"> -->

                                                    <select id="statu" name="statu" class="form-control">
                                                        <option value="">Belum Diset</option>
                                                        <option value="0">Tidak Valid</option>
                                                        <option value="1">Valid</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="updt" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#buktitrf').on('show.bs.modal', function(e) {
                                var valida = $(e.relatedTarget).data('vali');
                                var idnot = $(e.relatedTarget).data('idn');
                                 
                                // $("#tst").val(valida);  
                                $("#statu").val(valida);  
                                $("#idnota").val(idnot);                                 
                            });
                            </script>
                              <div class="modal fade" id="validasikamerakembali" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Bukti Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Status Bukti </label><br>
                                                    <form method="POST" action="maspemesanan.php">
                                                    <input type="hidden" name="idnotas" id="idnotas">
                                                    
                                                    <!-- <input type="text" id="tst"> -->

                                                    <select id="status" name="status" class="form-control">
                                                        <option value="">Belum Diset</option>
                                                        <option value="0">Belom Kembali</option>
                                                        <option value="1">Kembali</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="updts" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#validasikamerakembali').on('show.bs.modal', function(e) {
                                var valida = $(e.relatedTarget).data('valik');
                                var idnot = $(e.relatedTarget).data('idk');
                                 
                                // $("#tst").val(valida);  
                                $("#status").val(valida);  
                                $("#idnotas").val(idnot);                                 
                            });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
if(isset($_POST['updt']))
{
    $nota = $_POST['idnotaa'];
    $status = $_POST['statu'];

    if($status == '')
    {
        //do nothing, redirect ke masorder.php
        echo '<script language="javascript"> 
          document.location.href="masorder.php"
          </script>';
    }
    elseif ($status == '0') {

        echo '<script language="javascript"> 
          alert("status 0")
          </script>';
        //update jadi menunggu bukti transfer
        //(jaga jaga ketika setelah diupdate valid dan ingin dikembalikan menjadi tidak valid)
        $e = mysqli_query($link, "UPDATE notasewa SET statusnota = 'menunggu validasi pembayaran' WHERE id = '" . $nota . "'");

        //update status bukti valid jadi 0
        $r = mysqli_query($link, "UPDATE notasewa SET statuspesanan = '0' WHERE id = '" .$nota. "'");

        // document.location.href="masorder.php"

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
         document.location.href="maspemesanan.php"
          </script>';
    }
    else
    {
        echo '<script language="javascript"> 
          alert("status 1")
          </script>';
        //update status bukti valid jadi 1
        //update status kiriman jadi 'proses pengiriman'
        $r = mysqli_query($link, "UPDATE notasewa SET statuspesanan = '1', statusnota = 'selesai' WHERE id = '" .$nota. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="maspemesanan.php"
          </script>';
    }
}

?>

<?php
if(isset($_POST['updts']))
{
    $notas = $_POST['idnotas'];
    $statuss = $_POST['status'];
    //$stokkamera = '';

    if($statuss == '')
    {
        //do nothing, redirect ke masorder.php
        echo '<script language="javascript"> 
          document.location.href="maspemesanan.php"
          </script>';
    }
    elseif ($statuss == '0') {

        echo '<script language="javascript"> 
          alert("status 0")
          </script>';
        //update jadi menunggu bukti transfer
        //(jaga jaga ketika setelah diupdate valid dan ingin dikembalikan menjadi tidak valid)
       
           $p = mysqli_query($link, "SELECT * FROM kamera ");
            $res_p = mysqli_fetch_array($p);
            $idkamera = $res_p['id'];
         $res_p = mysqli_query($link, "UPDATE kamera SET stoktotal = stoktotal - 1 WHERE id = '".$idkamera."'");
        //update status bukti valid jadi 0
        $r = mysqli_query($link, "UPDATE notasewa SET statuskembali = '0' WHERE id = '" .$notas. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
         document.location.href="maspemesanan.php"
          </script>';
    }
    else
    {
         $p = mysqli_query($link, "SELECT * FROM kamera ");
            $res_p = mysqli_fetch_array($p);
            $idkamera = $res_p['id'];

         $res_p = mysqli_query($link, "UPDATE kamera SET stoktotal = stoktotal + 1 WHERE id = '".$idkamera."'");

        $r = mysqli_query($link, "UPDATE notasewa SET statuskembali = '1' WHERE id = '" .$notas. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="maspemesanan.php"
          </script>';
    }
}

?>
