<?php 
//batas awal dokumen PDF yang akan tercetak
ob_start(); 

?>
<html>
<head>
    <title>Cetak PDF</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>


<?php
// Load file koneksi.php
include "db.php";
if(isset($_POST['cetak']))
{
    $idnot = $_POST['idnota'];
// $q = mysqli_query($mycon, "SELECT k.id, .id FROM user k, notasewa h WHERE  h.user_id = k.id AND k.username = '$pengguna' ");

    $w = mysqli_query($link, "select b.id, a.nama, b.tanggalpesan, b.grandtotal from user a, notasewa b where a.id = b.user_id and b.id= '".$idnot."'");
    $res_w = mysqli_fetch_array($w);
    ?> 
    <div class="container" style="margin-top: 5%;">

        <div class="row">
            <div class="col-sm-8">
                <h1 style="text-align: center;">Bukti Transaksi</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <h3>Detil Transaksi</h3><br>

                <span>ID Nota</span><br> 
                <strong> <?php echo $res_w['id']; ?> </strong><br><br>

                <span>Tanggal Transaksi</span><br>
                <strong> <?php echo $res_w['tanggalpesan']; ?> </strong><br><br>

                <span>Pelanggan</span><br>
                <strong style="font-size: 18px;">
                 <?php echo $res_w['id']; ?> - 
                 <?php echo $res_w['nama']; ?>
                </strong><br><br>
            </div>
            <div class="col-sm-8" style="margin-top: 7%;">
                
                <table class="col-sm-10" >
                    <tr>
                        <th colspan="1" style="text-align: center;">Item</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Jumlah </th>
                        <th>Subtotal</th>
                    </tr>
                    <?php
                    $t = mysqli_query($link, "select b.gambar, b.namakamera, h.hargasewa, h.jmlsewa, (h.hargasewa*h.jmlsewa) as subtotal from hub_notasewa_dan_kamera h, kamera b WHERE h.kamera_id = b.id AND h.nota_id = '".$idnot."'");
                    while ($res_t = mysqli_fetch_array($t)) {
                        echo '
                    <tr>
                        <td><img src="images/' .$res_t['gambar']. '" alt=""></td>
                        <td>' .$res_t['namakamera']. '</td>
                        <td>' .$res_t['hargasewa']. '</td>
                        <td>' .$res_t['jmlsewa']. '</td>
                        <td>' .$res_t['subtotal']. '</td>
                    </tr>
                        ';
                    }
        echo '</table>';
                    ?>

            </div>
        </div>
       
        <div class="row">
            <div class="col-sm-4">

                <?php
                $total = $res_w['grandtotal'];
                ?>

                <span>Total Biaya yang harus dibayarkan sebesar</span><br>
                <h3><strong> <?php echo $total; ?></strong></h3><br><br>
            </div>
        </div>
    </div>
</body>
</html>
        <?php
}
else
{
    echo '<script language="javascript"> 
      alert("tidak ada parameter")
      </script>';
}



//batas akkhir dokumen PDF
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Nota Jual_' .$idnot. '.pdf', 'D');
?>
