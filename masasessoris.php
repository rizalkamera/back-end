<?php
session_start();
require './db.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Barang - Kurnia Kamera | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Asesoris</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                        <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Denda</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang </a>
                        </li>
                        <!--  <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li> -->
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Barang - Asessoris
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-coffee"></i> Master Barang- Asessoris
                                </li>
                            </ol>
                        </div>
                        <div class="col-lg-6">
                            <form action="process.php?act=spnasessoris" method="post" class="form" role="form" enctype="multipart/form-data">
                                <div class="row">
                                    <fieldset  class="form-group col-xs-6">
                                        <label for="namaB">Nama Asessoris:</label>
                                        <input type="text" class="form-control" name="namakamera" placeholder="Nama Barang" required>
                                    </fieldset>
                                </div>
                                 <div class="row">
                                    <fieldset  class="form-group col-xs-6">
                                        <label for="namaB">Nama Tipe:</label>
                                        <input type="text" class="form-control" name="namatipe" placeholder="Nama Tipe" required>
                                    </fieldset>
                                </div>
                                
                                <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <label for="hargaB">Harga Sewa 6 Jam:</label>
                                        <input type="number" onkeypress="return event.keyCode == 8 || return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="harga_6jam" id="hsewa" placeholder="Harga Sewa" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <label for="hargaB">Harga Sewa 12 Jam:</label>
                                        <input type="number" onkeypress="return event.keyCode == 8 || return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="harga_12jam" id="hsewa" placeholder="Harga Sewa" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-5">
                                        <label for="hargaB">Harga Sewa 24 Jam:</label>
                                        <input type="number" onkeypress="return event.keyCode == 8 || return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="harga_24jam" id="hsewa" placeholder="Harga Sewa" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-2">
                                        <label for="stokBB">Stok Kamera:</label>

                                        <!-- keyCode 8 -> backspace
                                        charCode >= 48 && <= 57 -> numeric -->
                                        <input type="number" onkeypress="return event.keyCode == 8 || event.charCode >= 48 && event.charCode <= 57" min="1" class="form-control" name="stokkamera" id="stk" placeholder="Stok Kamera" required>
                                    </fieldset>
                                </div>

                                <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="desResep">Deskripsi Asessoris:</label>
                                        <textarea class="form-control" name="deskripsi" rows="3" placeholder="Deskripsi Kamera" required></textarea>
                                    </fieldset>
                                </div>

                                <div class="row">
                                    <fieldset class="form-group col-xs-9">
                                        <label for="gambarB">Gambar Asessoris</label> </br>
                                        <label for="file" class="file">
                                            <input type="file" name="gambarkamera"  accept="image/*" onchange="loadFile(event)"/>
                                            <br>
                                            <img id="output" style="width: 50%; height: 50%;" />
                                        </label>
                                    </fieldset>
                                </div>
                               
                               <div class="row">
                                    <fieldset class="form-group col-xs-3">
                                        <label for="hargaB">Harga Beli:</label>
                                        <input type="number" onkeypress="return event.keyCode == 8 || return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="hargabeli" id="hbeli" placeholder="Harga Beli" required>
                                    </fieldset>
                                </div>
                               
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- tabel barang -->

                    <div class="col-lg-14">
                        <h2>Data Barang yang tersedia</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >NAMA ASESSORIS</th>
                                        <th >NAMA TIPE</th>
                                        <th >HARGA SEWA 6 JAM</th>
                                        <th >HARGA SEWA 12 JAM</th>
                                        <th >HARGA SEWA 24 JAM</th>
                                        <th >STOK KAMERA</th>
                                        <th >GAMBAR</th>
                                        <th >DESKRIPSI</th>
                                        <th >HARGA BELI</th>
                                        <th >EDIT|HAPUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //brapa data yang ditampilkan di tiap halaman
                                    $baris_per_page = 10;

                                    if(isset($_GET['page']))
                                    {
                                        $page = $_GET['page'];
                                    }
                                    else
                                    {
                                        $page = 1;
                                    }

                                    //set data yang ditampilkan mulai data ke brapa 
                                    $start_from = ($page-1) * $baris_per_page;



                                     $sql = "SELECT b.id,b.namakamera,b.namatipe,b.harga_6jam,b.harga_12jam,b.harga_24jam,b.stoktotal, b.gambar,b.deskripsi,b.hargabeli FROM kategori as k,kamera as b WHERE b.kategori_id = k.id AND b.kategori_id = '3' AND k.hapuskah = '0' AND b.hapuskah = '0' LIMIT " .$start_from. "," .$baris_per_page;
                                    //$sql = "SELECT * FROM `kamera` WHERE kategori = 'kamera'";
                                    //$sql = "SELECT * FROM `kamera`";
                                    $result = mysqli_query($link, $sql);
                                    if (!$result) {
                                        die("SQL Error:" . $sql);
                                    }
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<tr class= "row1">';
                                        echo "<th>" . $row['id'] . "</th>";
                                        echo "<td class='row1 col-sm-2'>" . $row['namakamera'] . "</td>";
                                        echo "<td class='row1 col-sm-2'>" . $row['namatipe'] . "</td>";
                                        echo "<td class='row1 col-sm-1'>Rp." .number_format($row['harga_6jam'], 0, ',', '.') .",-" ."</td>";
                                        echo "<td class='row1 col-sm-1'>Rp." .number_format($row['harga_12jam'], 0, ',', '.') .",-" ."</td>";
                                        echo "<td class='row1 col-sm-1'>Rp." .number_format($row['harga_24jam'], 0, ',', '.') .",-" ."</td>";
                                        echo "<td class='row1 col-sm-1'>" . $row['stoktotal'] . "</td>";
                                        echo "<td class='row1 col-sm-1'>
                                        <button type='button' class='btn btn-info btn-sm' data-gbr='images/" .$row['gambar']. "' data-toggle='modal' data-target='#showgbr'><i class='fa fa-eye'></i></button>

                                        <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id']. "' data-toggle='modal' data-target='#editgbr'><span class='glyphicon glyphicon-pencil'></span></button></td>";
                                        echo "<td class='row1 col-sm-6'>" . $row['deskripsi'] . "</td>";
                                        echo "<td class='row1 col-sm-1'>Rp." .number_format($row['hargabeli'], 0, ',', '.') .",-" ."</td>";
                                        
                                        echo "<td class='row1 col-sm-2'>

                                            <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id']. "' data-nk='" .$row['namakamera']."' data-nt='" .$row['namatipe']. "' data-hs6='" .$row['harga_6jam']."' data-hs12='" .$row['harga_12jam']."' data-hs24='" .$row['harga_24jam']. "' data-st='" .$row['stoktotal']. "' data-desk='" .$row['deskripsi']."' data-hb='" .$row['hargabeli']."' data-toggle='modal' data-target='#editbrg'><span class='glyphicon glyphicon-pencil'></span></button>

                                            <a href='process.php?act=deleteasessoris&idas=" .$row['id']."'<button type='button' class='btn btn-danger btn-sm' "; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data ?');"><span class='glyphicon glyphicon-trash'></span></button></a>
                                            
                                            </td>
                                            <?php echo "</tr>";

                                    }?>


                                </tbody>
                            </table>
                        </div>

                            <script type="text/javascript">
                            var loadFile = function(event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                        <div class="modal fade" id="editgbr" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Upload Gambar Baru</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                            <div class="row">
                                                <label for="NomorResi">File Gambar Baru </label><br>
                                                <form method="POST" action="masasessoris.php" enctype="multipart/form-data">
                                                <input type="hidden" name="idbar" />
                                                <input type="file" name="gbr_brg" accept="image/*" onchange="loadFile_edit(event)"/>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <img id="output_edit" style="width: 50%; height: 50%;" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="subb" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                </form>
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                         <script type="text/javascript">
                            var loadFile = function(event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                        
                        <div class="modal fade" id="showgbr" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Gambar Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                            <div class="row">
                                                <img class="gam" src="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                        $('#editgbr').on('show.bs.modal', function(e) {

                            var idba = $(e.relatedTarget).data('id');
                            
                            $(e.currentTarget).find('input[name="idbar"]').val(idba);

                        });

                        var loadFile_edit = function(event) {
                            var output_edit = document.getElementById('output_edit');
                            output_edit.src = URL.createObjectURL(event.target.files[0]);
                        };
                        </script>

                        <div class="modal fade" id="showgbr" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Gambar Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                            <div class="row">
                                                <img class="gam" src="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <script type="text/javascript">
                        $('#showgbr').on('show.bs.modal', function(e) {

                            var gbr1 = $(e.relatedTarget).data('gbr');
                            
                           $(".gam").attr('src', gbr1);
                        });
                        </script>
                        
                        <div class="modal fade" id="editbrg" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">  
                                            <div class="row">
                                                <form method="POST" action="masasessoris.php">
                                                <input type="hidden" name="id" />
                                                <label for="isiResep">Nama Asessoris </label><br>
                                                <input type="text" name="namakamera" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Nama Tipe </label><br>
                                                <input type="text" name="namatipe" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Harga Sewa 6 Jam </label><br>
                                                <input type="number" name="harga_6jam" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Harga Sewa 12 Jam </label><br>
                                                <input type="number" name="harga_12jam" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Harga Sewa 12 Jam </label><br>
                                                <input type="number" name="harga_24jam" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Stok Asessoris </label><br>
                                                <input type="number" name="stok" />
                                            </div>
                                            <div class="row">
                                                <label for="isiResep">Deskripsi </label><br>
                                                <textarea class="deskripsi" name="deskripsi"></textarea>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Harga Beli </label><br>
                                                <input type="number" name="hargabeli" />
                                            </div>
                                            <br>
                                             
                                            <div class="row">
                                                <button type="submit" name="subbar" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#editbrg').on('show.bs.modal', function(e) {

                            var id = $(e.relatedTarget).data('id');
                            var namakamera = $(e.relatedTarget).data('nk');
                            var namatipe = $(e.relatedTarget).data('nt');
                            var harga_6jam = $(e.relatedTarget).data('hs6');
                            var harga_12jam = $(e.relatedTarget).data('hs12');
                            var harga_24jam = $(e.relatedTarget).data('hs24');
                            var stokkamera = $(e.relatedTarget).data('st');
                            var deskripsi = $(e.relatedTarget).data('desk');
                            var hargabeli = $(e.relatedTarget).data('hb');
                            var kategori = $(e.relatedTarget).data('kt');

                           $(e.currentTarget).find('input[name="id"]').val(id);
                           $(e.currentTarget).find('input[name="namakamera"]').val(namakamera);
                           $(e.currentTarget).find('input[name="namatipe"]').val(namatipe);
                           $(e.currentTarget).find('input[name="harga_6jam"]').val(harga_6jam);
                           $(e.currentTarget).find('input[name="harga_12jam"]').val(harga_12jam);
                           $(e.currentTarget).find('input[name="harga_24jam"]').val(harga_24jam);
                           $(e.currentTarget).find('input[name="stok"]').val(stokkamera);
                           $(".deskripsi").val(deskripsi);
                           $(e.currentTarget).find('input[name="hargabeli"]').val(hargabeli);
                           $(e.currentTarget).find('input[name="kategori"]').val(kategori);
                        });
                        </script>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </body>
</html>


<?php 
//set ukuran file maksimal
define ("MAX_SIZE","2000");

//method ambil format gambar
function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }


if(isset($_POST['subb']))
{
    $id = $_POST['idbar'];
    $image =$_FILES["gbr_brg"]["name"];
    $uploadedfile = $_FILES['gbr_brg']['tmp_name'];
     
    if ($image) 
    {
        //hapus karakter "/"
        $filename = stripslashes($_FILES['gbr_brg']['name']);

        //ambil format file
        $extension = getExtension($filename);

        //ubah jadi huruf kecil
        $extension = strtolower($extension);
        
        if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png")) 
        {
            //alert format file tidak terbaca
        }
        else
        {
            $size=filesize($_FILES['gbr_brg']['tmp_name']);
            //jika ukuran melebihi 400kb (max_size diset di awal sebesar 400, dikali 1024 bytes)
            if ($size > MAX_SIZE*1024)
            {
               $change='<div class="msgdiv">You have exceeded the size limit!</div> ';
               $errors=1;
            }


            if($extension=="jpg" || $extension=="jpeg" )
            {
                $uploadedfile = $_FILES['gbr_brg']['tmp_name'];

                //fungsi agar file asli dapat diresampled
                $src = imagecreatefromjpeg($uploadedfile);
            }
            else
            {
                $uploadedfile = $_FILES['gbr_brg']['tmp_name'];
                //fungsi agar file asli dapat diresampled
                $src = imagecreatefrompng($uploadedfile);
            }

            // ambil ukuran lebar dan tinggi image asli
            list($width,$height)=getimagesize($uploadedfile);


            //1a. resample image untuk index index
            $newheight=188;
            $newwidth=($width/$height)*$newheight;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp=imagecreatetruecolor($newwidth,$newheight);

            //1b. resample image untuk index prod_det
            $newheight1=188;
            $newwidth1=($width/$height)*$newheight1;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp1=imagecreatetruecolor($newwidth1,$newheight1);


            //1c. resample image untuk cart
            $newheight2=110;
            $newwidth2=($width/$height)*$newheight2;
            //fungsi untuk menset template image dengan ukuran yang telah dihitung sebagai destinasi file image hasil resample
            $tmp2=imagecreatetruecolor($newwidth2,$newheight2);

            imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
            //imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);
            //imagecopyresampled($tmp2,$src,0,0,0,0,$newwidth2,$newheight2,$width,$height);


            //sebelum set nama file yang baru, pisahkan nama file dan format file
            $sp = explode(".",$_FILES['gbr_brg']['name']);

            // set nama file baru dengan melakukan enkripsi md5 dari nama file beserta formatnya concate waktu sistem
            $nam = md5($_FILES['gbr_brg']['name'] . time());

            //substring hasil md5 dan concat dgn format file
            $spl0 = substr($nam,0,10);
            $spl = $spl0 . "." .$sp[count($sp) - 1];

            
            $filename = "images/". $spl;
           // $filename1 = "../images/barang/prod_det/". $spl;
            //$filename2 = "../images/barang/cart/". $spl;

            
            imagejpeg($tmp,$filename,100);
          //  imagejpeg($tmp1,$filename1,100);
           // imagejpeg($tmp2,$filename2,100);


            imagedestroy($src);
            imagedestroy($tmp);
         //   imagedestroy($tmp1);
          //  imagedestroy($tmp2);

            //update nama file pada data yang diubah
            $e = mysqli_query($link, "UPDATE kamera SET gambar = '" .$spl. "' WHERE id = '" .$id. "'");
            
             echo '<script language="javascript"> 
          alert("File Berhasil diunggah.")
          document.location.href="masasessoris.php"
          </script>';
        }
    }
    
    
}

if(isset($_POST['subbar']))
{
    $id = $_POST['id'];
    $namakamera = $_POST['namakamera'];
    $namatipe = $_POST['namatipe'];
    $harga_6jam = $_POST['harga_6jam'];
    $harga_12jam = $_POST['harga_12jam'];
    $harga_24jam = $_POST['harga_24jam'];
    $stokkamera = $_POST['stok'];
    $deskripsi = $_POST['deskripsi'];
    $hargabeli = $_POST['hargabeli'];
  
    /*$e = mysqli_query($link, "UPDATE kamera SET nama kamera ='".$namakamera."', nama tipe = '" .$namatipe."',
     harga sewa = " .$hargasewa. ", stok total = " .$stokkamera. ", deskripsi = '" .$deskripsi. "',
    harga beli =" .$hargabeli. " WHERE id = '" .$id. "'");
    */
   $e = mysqli_query($link, "UPDATE `kamera` SET `namakamera`='".$namakamera."',`namatipe`='".$namatipe."',`harga_6jam`= ".$harga_6jam.",`harga_12jam`= ".$harga_12jam.",`harga_24jam`= ".$harga_24jam.",`stoktotal`=".$stokkamera.",`deskripsi`='".$deskripsi."',`hargabeli`=".$hargabeli." WHERE id='".$id."'");
   
    if (!$e) {
        echo mysqli_error($link);
    }
    else
   {
         echo '<script language="javascript"> 
          alert("Data Berhasil diubah.")
          document.location.href="masasessoris.php"
          </script>';
    }
   
}

?>