<?php
session_start();
require './db.php';

/*if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}*/
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Kamera</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Order
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order
                                </li>
                            </ol>
                        </div>
                        <div class="col-sm-11">
                            <h2>Data Order yang Tersedia Saat ini:</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr >
                                            <th > ID JUAL</th>
                                            <th > NAMA PRODUK</th>
                                            <th > KOTA </th>
                                            <th > KATEGORI </th>
                                            <th > DESKRIPSI </th>
                                           
                                            <th > HARGA JUAL</th>
                                            <th > FOTO </th>
    
                                            <th >CETAK NOTA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';
                                        $sql = "SELECT * FROM `postinganjualkamera`";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) 
                                        {
                                           // if($row['status_pesanan'] == 'menunggu selesai belanja')
                                            //{
                                                // disable all
                                                echo '
                                            <tr class= "row1">
                                                
                                                <th class="row1 col-sm-1" style="text-align: center;">' . $row['id'] . '</th>
                                                <td class="row1 col-sm-1" style="text-align: center;">' . $row['namaproduk'] . '</td>
                                                 <td class="row1 col-sm-1" style="text-align: center;">' . $row['kota'] . '</td>
                                                  <td class="row1 col-sm-1" style="text-align: center;">' . $row['kategori'] . '</td>
                                                   <td class="row1 col-sm-1" style="text-align: center;">' . $row['deskripsi'] . '</td>
                                                    <td class="row1 col-sm-1" style="text-align: center;">' . $row['hargajual'] . '</td>
                                                 </tr>';
                                                  echo "<td class='row1 col-sm-1'>
                                                    <button type='button' class='btn btn-info btn-sm' data-gbr='images/" .$row['foto']. "' data-toggle='modal' data-target='#showgbr'><i class='fa fa-eye'></i></button>

                                                    <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id']. "' data-toggle='modal' data-target='#editgbr'><span class='glyphicon glyphicon-pencil'></span></button></td>";
                                            
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="viewtrf" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detail Data Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Bank Tujuan Transfer </label><br>
                                                    <span class="bank"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="NoHPPenerima">Nomor Rekening </label><br>
                                                    <span class="no_rek"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="AlamatPenerima">Nama Pemilik Rekening </label><br>
                                                    <span class="nama_pmilik"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="KodePos">Nominal Transfer </label><br>
                                                    Rp. <span class="nominal"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 

                            function currency(str) {
                                var reverse = str.toString().split('').reverse().join(''),
                                ribuan  = reverse.match(/\d{1,3}/g);
                                ribuan  = ribuan.join('.').split('').reverse().join('');

                                return ribuan;
                            }

                            $('#viewtrf').on('show.bs.modal', function(e) {
                            
                                var nb1 = $(e.relatedTarget).data('nb');
                                var nr1 = $(e.relatedTarget).data('nr');
                                var pr1 = $(e.relatedTarget).data('pr');
                                var nom1 = $(e.relatedTarget).data('nom');

                                $(".bank").html(nb1);
                                $(".no_rek").html(nr1);
                                $(".nama_pmilik").html(pr1);
                                $(".nominal").html(currency(nom1)+',-');
                                
                                });
                            </script>
                            <div class="modal fade" id="viewkiriman" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detail Kiriman</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="TipePengiriman">Tipe Pengiriman </label><br>
                                                    <span class="tipekrm"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="NamaPenerima">Nama Penerima </label><br>
                                                    <span class="namapen"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="NoHPPenerima">No. HP Penerima </label><br>
                                                    <span class="no_hp"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="AlamatPenerima">Alamat Penerima </label><br>
                                                    <span class="almat"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="KodePos">Kode Pos </label><br>
                                                    <span class="kodepos"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="BiayaKirim">Biaya Kirim </label><br>
                                                    Rp. <span class="ongkir"></span>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 

                            function currency(str) {
                                var reverse = str.toString().split('').reverse().join(''),
                                ribuan  = reverse.match(/\d{1,3}/g);
                                ribuan  = ribuan.join('.').split('').reverse().join('');

                                return ribuan;
                            }

                            $('#viewkiriman').on('show.bs.modal', function(e) {
                            
                                var tk = $(e.relatedTarget).data('tk');
                                var np = $(e.relatedTarget).data('np');
                                var nohp = $(e.relatedTarget).data('nohp');
                                var almt = $(e.relatedTarget).data('almt');
                                var kodeP = $(e.relatedTarget).data('kodepos');
                                var biaya = $(e.relatedTarget).data('biaya');

                                $(".tipekrm").html(tk);
                                $(".namapen").html(np);
                                $(".no_hp").html(nohp);
                                $(".almat").html(almt);
                                $(".kodepos").html(kodeP);
                                $(".ongkir").html(currency(biaya)+',-');
                                
                                });
                            </script>

                            <div class="modal fade" id="buktitrf" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Bukti Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Status Bukti </label><br>
                                                    <form method="POST" action="masorder.php">
                                                    <input type="hidden" name="idnotaa" id="idnota">
                                                    <input type="hidden" name="idp" id="idpela">
                                                    <input type="hidden" name="mail" id="mailer">
                                                    
                                                    <!-- <input type="text" id="tst"> -->

                                                    <select id="statu" name="statu" class="form-control">
                                                        <option value="">Belum Diset</option>
                                                        <option value="0">Tidak Valid</option>
                                                        <option value="1">Valid</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="updt" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#buktitrf').on('show.bs.modal', function(e) {
                                var valida = $(e.relatedTarget).data('vali');
                                var idnot = $(e.relatedTarget).data('idn');
                                var idpel = $(e.relatedTarget).data('pel');
                                var mail = $(e.relatedTarget).data('email');
                                 
                                // $("#tst").val(valida);  
                                $("#statu").val(valida);  
                                $("#idnota").val(idnot);
                                $("#idpela").val(idpel); 
                                $("#mailer").val(mail);                                  
                            });
                            </script>
                            <div class="modal fade" id="isiresi" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Bukti Transfer</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NomorResi">Nomor Resi Pengiriman </label><br>
                                                    <form method="POST" action="masorder.php">
                                                    <input type="hidden" name="idkirim" />
                                                    <input type="hidden" name="idnota" />
                                                    <input type="hidden" name="emaill" />
                                                    <input type="text" name="noresi" class="form-control" size="5" />
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="subb" class="btn btn-primary">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#isiresi').on('show.bs.modal', function(e) {
                                var idk = $(e.relatedTarget).data('idkir');
                                var idnotaa = $(e.relatedTarget).data('idn');
                                var nomo = $(e.relatedTarget).data('nom');
                                var ema = $(e.relatedTarget).data('email');

                                $(e.currentTarget).find('input[name="idkirim"]').val(idk);
                                $(e.currentTarget).find('input[name="idnota"]').val(idnotaa);
                                $(e.currentTarget).find('input[name="noresi"]').val(nomo);
                                $(e.currentTarget).find('input[name="emaill"]').val(ema);                               
                            });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
if(isset($_POST['subb']))
{
    //jika field nomor resi dikosongkan
    if ($_POST['noresi'] == '') {
        $idkrm = $_POST['idkirim'];
        $idno = $_POST['idnota'];
        $nores = '';

         //set no resi kiriman menjadi kosong
        $p = mysqli_query($link, "UPDATE pengiriman SET no_resi = '" .$nores. "' WHERE id_pengiriman = '" .$idkrm. "'");

        //update status kiriman jadi proses pengiriman
        $r = mysqli_query($link, "UPDATE nota_jual SET status_pesanan = 'proses pengiriman' WHERE id_notaJual = '" .$idno. "'");

        echo '<script language="javascript"> 
              alert("Nomor Resi Berhasil dihapus.")
             document.location.href="masorder.php"
              </script>';
    }
    else
    {
        $nores = $_POST['noresi'];
        $idkrm = $_POST['idkirim'];
        $idno = $_POST['idnota'];
        $email = $_POST['emaill'];

        //set no resi kiriman
        $p = mysqli_query($link, "UPDATE pengiriman SET no_resi = '" .$nores. "' WHERE id_pengiriman = '" .$idkrm. "'");

        //update status kiriman jadi selesai
        $r = mysqli_query($link, "UPDATE nota_jual SET status_pesanan = 'selesai' WHERE id_notaJual = '" .$idno. "'");




        //kirim email konfirmasi transaksi selesai


        //1. ambil data pengiriman
        $e = mysqli_query($link, "SELECT * FROM pengiriman WHERE id_pengiriman = '" .$idkrm. "'");
        $res_e = mysqli_fetch_array($e);

        //2.simpan informasi yg dibutuhkan
        $nama = $res_e['nama_penerima'];
        $alamat = $res_e['alamat_penerima'];
        $nohp = $res_e['noHP_penerima'];
        $tipe = $res_e['tipe_pengiriman'];
        $resi = $res_e['no_resi'];


        // ini_set('display_errors', 1 );

        // error_reporting( E_ALL );
         
        // $from = "commisF&B@000webhost.com";
         
        // $to = $email;
         
        // $subject = "Transaksi Selesai";
         
        // $message = 
        //     '<p>Transaksi telah selesai. Berikut adalah data pengiriman anda.</p><br>
        //     <p>Nama Penerima</p>
        //     <p>' .$nama. '</p>
        //     <p>Alamat Penerima</p>
        //     <p>' .$alamat. '</p>
        //     <p>Nomor HP</p>
        //     <p>' .$nohp. '</p>
        //     <p>Tipe Pengiriman</p>
        //     <p>' .$tipe. '</p>
        //     <p>No Resi Pengiriman</p>
        //     <p>' .$resi. '</p><br></br>

        //     <table>
        //         <tr>
        //             <th>Nama Barang</th>
        //             <th>Harga</th>
        //             <th>Jumlah Beli</th>
        //             <th>Subtotal</th>
        //         </tr>';
        // $t = mysqli_query($link, "SELECT b.nama_barang, h.harga_jual, h.jml_jual, SUM(h.harga_jual*h.jml_jual) as subtotal FROM nota_jual n, hub_nota_barang h, barang b WHERE n.id_notaJual = h.nota_id AND h.barang_id = b.id_barang AND h.nota_id = '" .$idno. "' AND b.hapuskah = '0' AND n.hapuskah = '0'");

        // if(!$t)
        // {
        //     echo mysqli_error($link);
        // }

        // while($res = mysqli_fetch_array($t))
        // {
        //     $message .= '
        //         <tr>
        //             <td>' .$res['nama_barang']. '</td>
        //             <td>' .$res['harga_jual']. '</td>
        //             <td>' .$res['jml_jual']. '</td>
        //             <td>' .$res['subtotal']. '</td>
        //         </tr>';
        // }
        // $message .= '
        //     </table><br></br>
        //     <p>Terima kasih telah bertransaksi dengan kami.</p>';


        // $headers = "From:" . $from . "\r\n";
        // $headers .= "MIME-Version: 1.0\r\n";
        // $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
         
        // //error karena method mail bisa jalan saat web di onlinekan
        // mail($to,$subject,$message, $headers);




        echo '<script language="javascript"> 
              alert("Data berhasil terinput.")
              document.location.href="masorder.php"
              </script>';
    }
    
}

if(isset($_POST['updt']))
{
    $nota = $_POST['idnotaa'];
    $pelanggan = $_POST['idp'];
    $status = $_POST['statu'];
    $email = $_POST['mail'];
    

    if($status == '')
    {
        //do nothing, redirect ke masorder.php
        echo '<script language="javascript"> 
          document.location.href="masorder.php"
          </script>';
    }
    elseif ($status == '0') {
        //update jadi menunggu bukti transfer
        //(jaga jaga ketika setelah diupdate valid dan ingin dikembalikan menjadi tidak valid)
        $e = mysqli_query($link, "UPDATE nota_jual SET status_pesanan = 'menunggu bukti transfer' WHERE id_notaJual = '" . $nota . "'");


        //update status bukti valid jadi 0
        $r = mysqli_query($link, "UPDATE nota_jual SET bukti_valid = '0' WHERE id_notaJual = '" .$nota. "'");



         //kirim email pemberitahuan utk proses upload ulang
        // $email = $res_e['email'];

       
        // ini_set('display_errors', 1 );
 
        // error_reporting( E_ALL );
         
        // $from = "CommisF&B@000webhost.com";
         
        // $to = $email;
         
        // $subject = "Upload Ulang Bukti Kirim";
         
        // $message = 
        //     '<p>Mohon maaf, Foto bukt transfer yang anda upload tidak valid. Pastikan hasil foto dapat terbaca dengan baik dan jelas, sehingga mempermudah kami dalam melakukan validasi. Silahkan melakukan proses upload foto bukti transfer yang anda miliki.</p><br>
        //     <a class="btn btn-default btn_bayar" href="checkout_done.php">Upload Ulang Bukti</a>';


        // $headers = "From:" . $from . "\r\n";
        // $headers .= "MIME-Version: 1.0\r\n";
        // $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
         
        // //error karena method mail bisa jalan saat web di onlinekan
        // mail($to,$subject,$message, $headers);
        
        // document.location.href="masorder.php"

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
         document.location.href="masorder.php"
          </script>';
    }
    else
    {
        //update status bukti valid jadi 1
        //update status kiriman jadi 'proses pengiriman'
        $r = mysqli_query($link, "UPDATE nota_jual SET bukti_valid = '1', status_pesanan = 'proses pengiriman' WHERE id_notaJual = '" .$nota. "'");

        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="masorder.php"
          </script>';
    }
}

?>
