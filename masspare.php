<?php
session_start();
require './db.php';

if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Barang - Sparepart Mesin | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $pengguna; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Barang - Sparepart Mesin
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-coffee"></i> Master Barang - Sparepart Mesin
                                </li>
                            </ol>
                        </div>
                        <!-- /.row -->

                        <div class="col-lg-6">
                            <form action="masspare.php" method="post" class="form" role="form" enctype="multipart/form-data">
                                <div class="row">
                                    <fieldset  class="form-group col-xs-6">
                                        <label for="namaB">Nama Barang:</label>
                                        <input type="text" class="form-control" name="namaB" placeholder="Nama Barang" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-3">
                                        <label for="hargaB">Harga:</label>
                                        <input type="number" onkeypress="return event.keyCode == 8 || return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="hargaB" id="hbar" placeholder="Harga" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-2">
                                        <label for="stokBB">Stok:</label>

                                        <!-- keyCode 8 -> backspace
                                        charCode >= 48 && <= 57 -> numeric -->
                                        <input type="number" onkeypress="return event.keyCode == 8 || event.charCode >= 48 && event.charCode <= 57" min="1" class="form-control" name="stokB" id="stk" placeholder="Stok" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="add" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- tabel barang -->

                    <div class="col-lg-6">
                        <h2>Data Sparepart yang tersedia</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr >
                                        <th >ID</th>
                                        <th >NAMA BARANG</th>
                                        <th >HARGA</th>
                                        <th >STOK</th>
                                        <th >EDIT | HAPUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT b.id_barang,b.nama_barang,b.harga,b.stok FROM `kategori`as k,barang as b WHERE b.kategori_id = k.id_kat AND b.kategori_id = '5' AND k.hapuskah = '0' AND b.hapuskah = '0'";
                                    $result = mysqli_query($link, $sql);
                                    if (!$result) {
                                        die("SQL Error:" . $sql);
                                    }
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<tr class= "row1">';
                                        echo "<th>" . $row['id_barang'] . "</th>";
                                        echo "<td class='row1 col-sm-5'>" . $row['nama_barang'] . "</td>";

                                        echo "<td class='row1 col-sm-3'>Rp." .number_format($row['harga'], 0, ',', '.') .",-" ."</td>";
                                        echo "<td class='row1 col-sm-1'>" . $row['stok'] . "</td>";
                                        echo "<td class='row1 col-sm-4'>

                                            <button type='button' class='btn btn-primary btn-sm' data-id='" .$row['id_barang']. "' data-nb='" .$row['nama_barang']. "' data-hg='" .$row['harga']. "' data-st='" .$row['stok']. "' data-toggle='modal' data-target='#edit'><span class='glyphicon glyphicon-pencil'></span></button>

                                            <a href='masspare.php?idbarang=" .$row['id_barang']."'<button type='button' class='btn btn-danger btn-sm' "; ?> onclick="return confirm('Apakah anda yakin untuk menghapus data ?');"><span class='glyphicon glyphicon-trash'></span></button></a>
                                            
                                            </td>
                                            <?php echo "</tr>";
                                    }?>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="modal fade" id="edit" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Ubah Data Sparepart</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                            <div class="row">
                                                <form method="POST" action="masspare.php">
                                                <input type="hidden" name="idbar" />
                                                <label for="isiResep">Nama Sparepart </label><br>
                                                <input type="text" style="width: 50%;" class="form-control" name="nama" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Harga </label><br>
                                                <input type="number" style="width: 20%;" class="form-control" name="harga" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label for="isiResep">Stok </label><br>
                                                <input type="number" style="width: 15%;" class="form-control" name="stok" />
                                            </div>
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="subbar" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#edit').on('show.bs.modal', function(e) {

                            var idb = $(e.relatedTarget).data('id');
                            var nam = $(e.relatedTarget).data('nb');
                            var har = $(e.relatedTarget).data('hg');
                            var sto = $(e.relatedTarget).data('st');
                            
                            
                           $(e.currentTarget).find('input[name="idbar"]').val(idb);
                           $(e.currentTarget).find('input[name="nama"]').val(nam);
                           $(e.currentTarget).find('input[name="harga"]').val(har);
                           $(e.currentTarget).find('input[name="stok"]').val(sto);
                        });
                        </script>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>
    </body>
</html>


<?php 

if(isset($_POST['add']))
{
    $namaB1 = $_POST['namaB'];
    $hargaB1 = $_POST['hargaB'];
    $stokB1 = $_POST['stokB'];
    
    //insert data 
    $e = mysqli_query($link, "INSERT INTO barang (nama_barang, harga, stok, hapuskah, kategori_id) VALUES ('$namaB1','$hargaB1','$stokB1',0,5)");
    
    echo '<script language="javascript"> 
      alert("Data Berhasil diinput.")
      document.location.href="masspare.php"
      </script>';
    
}
if(isset($_POST['subbar']))
{
    $idb = $_POST['idbar'];
    $nam = $_POST['nama'];
    $har = $_POST['harga'];
    $sto = $_POST['stok'];

    $e = mysqli_query($link, "UPDATE barang SET nama_barang = '" .$nam. "', harga = '" .$har. "', stok = '" .$sto. "' WHERE id_barang = '" .$idb. "'");

    echo '<script language="javascript"> 
          alert("File Berhasil diubah.")
          document.location.href="masspare.php"
          </script>';
}
if(isset($_GET['idbarang']))
{
    $idb = $_GET['idbarang'];

    $e = mysqli_query($link, "UPDATE barang SET hapuskah = 1 WHERE id_barang = '" .$idb. "'");

    echo '<script language="javascript"> 
          alert("File Berhasil dihapus.")
          document.location.href="masspare.php"
          </script>';
}

?>