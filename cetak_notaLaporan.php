<?php 
//batas awal dokumen PDF yang akan tercetak
ob_start(); 

?>
<html>
<head>
    <title>Cetak PDF</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>


<?php
// Load file koneksi.php
include "db.php";
if(isset($_POST['cetak']))
{
    $idlaporan = $_POST['idlaporan'];
// $q = mysqli_query($mycon, "SELECT k.id, .id FROM user k, notasewa h WHERE  h.user_id = k.id AND k.username = '$pengguna' ");
    /// $w = mysqli_query($link, "select b.id, a.nama, b.tanggalpesan, b.grandtotal from user a, notasewa b where a.id = b.user_id and b.id= '".$idnot."'");
    $w = mysqli_query($link, "select * from user u, notasewa n, kamera k , hub_notasewa_dan_kamera h where k.id = h.kamera_id and n.id = h.nota_id and  u.id = n.user_id and n.id= '".$idlaporan."'");
    $res_w = mysqli_fetch_array($w);
    ?> 
    <div class="container" style="margin-top: %;">

        <div class="row">
            <div class="col-sm-8">
                <h1 style="text-align: center;">Laporan Kurnia Kamera</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
               
            </div>
            <div class="col-sm-8" style="margin-top: 7%;">
                
               <!--  <table class="col-sm-10" > -->
                <table class="table table-bordered">
                    <tr>
                        <th style="text-align: center;">Id</th>
                        <th style="text-align: center;">Nama</th>
                        <th style="text-align: center;">Nama Kamera</th>
                        <th style="text-align: center;">Tanggal Ambil</th>
                        <th style="text-align: center;">Tanggal Kembali</th>
                        <th style="text-align: center;">Durasi</th>
                        <th style="text-align: center;">Jumlah</th>
                        <th style="text-align: center;">Harga Sewa</th>
                       <!--  <th>Subtotal</th> -->
                    </tr>
                    <?php
                      $t = mysqli_query($link, "SELECT *,
                                                CASE
                                                 WHEN n.user_id = 0 THEN n.namapenyewa
                                                 ELSE u.nama
                                                END AS nama_penyewa from user u, notasewa n, kamera k , hub_notasewa_dan_kamera h where k.id = h.kamera_id and n.id = h.nota_id and  u.id = n.user_id and n.id= '".$idlaporan."'");
                    //$t = mysqli_query($link, "select * from user u, notasewa n, kamera k, hub_notasewa_dan_kamera h where a.id = b.user_id and b.id= '".$idlaporan."'");
                    while ($res_t = mysqli_fetch_array($t)) {
                        echo '
                    <tr>
                        <td style="text-align: center;">' .$res_t['id']. '</td>
                        <td style="text-align: center;">' .$res_t['nama_penyewa']. '</td>
                        <td style="text-align: center;">' .$res_t['namakamera']. '</td>
                        <td style="text-align: center;">' .$res_t['tgl_ambil']. '</td>
                        <td style="text-align: center;">' .$res_t['tgl_kembali']. '</td>
                        <td style="text-align: center;">' .$res_t['durasi']. '</td>
                        <td style="text-align: center;">' .$res_t['jmlsewa']. '</td>
                        <td style="text-align: center;">' .$res_t['hargasewa']. '</td>
                        
                    </tr>
                        ';
                    }
        echo '</table>';
                    ?>

            </div>
        </div>
       
        <div class="row">
            <div class="col-sm-4">


                <?php
                $p = mysqli_query($link, "SELECT SUM(hargasewa) as total from hub_notasewa_dan_kamera WHERE nota_id = '".$idlaporan."'");
                $res_p = mysqli_fetch_array($p);
                $total = $res_p['total'];
                ?>
             <p style="text-align-right : 80%;">
             <span>Total pendapatan Rp. <?php echo number_format($total, 0, ',', '.'); 
            ?>,-</span></p><br>

            </div>
        </div>
    </div>
</body>
</html>
        <?php
}
else
{
    echo '<script language="javascript"> 
      alert("tidak ada parameter")
      </script>';
}



//batas akkhir dokumen PDF
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Nota Laporan_' .$idlaporan. '.pdf', 'D');
?>
