<?php
session_start();
require 'db.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Pemesanan Manual| ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>
         <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
               <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-camera"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                                 <li>
                                    <a href="masterinputlelang.php"> Lelang</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pegembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                        <li>
                            <a href="maslelang.php"><i class="fas fa-hammer "></i> Master Lelang </a>
                        </li>
                        

                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                         <li>
                            <a href="laporan.php"><i class="fa fa-fw fa-edit"></i>Laporan</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Order Manual
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-university"></i> Master Lelang
                                 </li>
                        </ul>
                    </div>
                </nav>
                            <!-- /.row -->
                        <div class="container">
                            <div class="row">
                                <div class='col-sm-4'>
                                    <h3> Form Input  Pemesanan </h3>
                                    
                                    <form action="ordermanual.php" method="post" class="form-center" role="form" enctype="multipart/form-data">
                                   <div class="row">
                                       <fieldset  class="form-group col-xs-8">
                                        <label for="namaB">Nama Penyewa:</label>
                                        <input type="text" class="form-control" name="namapenyewa" placeholder="Nama Penyewa" required>
                                    </fieldset>
                                </div>
                                      <div class="row">
                                      <fieldset  class="form-group col-xs-9">
                                        <label for="hargaB">Nama Kamera</label>
                                        <select id="cbokamera" name="namakamera" class="form-control" required>
                                        <option value="" selected disabled>Pilih Kamera</option>
                                            <?php
                                            $sq = mysqli_query($link, "SELECT * FROM kamera WHERE stoktotal > '0' ORDER BY namakamera");
                                            while ($row = mysqli_fetch_array($sq)) {
                                            ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['namakamera'] ?></option>
                                        <?php } ?>
             
                                 </select>
                                    </fieldset>
                                </div>
                                    <div class="row">
                                        <fieldset  class="form-group col-xs-9">
                                            <label for="isiResep">Tanggal Pemesanan:</label>
                                            <div class='input-group date' >
                                                <input type="datetime-local" class="form-control" id="wktuMulai" name='waktumulai' required oninvalid="this.setCustomValidity('Harap Diisi')"/>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="row">
                                        <fieldset  class="form-group col-xs-9">
                                            <label for="isiResep">Tanggal Kembali:</label>
                                            <div class='input-group date' >
                                                <input type="datetime-local" class="form-control" id="wktuMulai" name='waktuselesai' required oninvalid="this.setCustomValidity('Harap Diisi')"/>
                                            </div>
                                        </fieldset>
                                    </div>
                                        <div class="row">
                                      <fieldset  class="form-group col-xs-7">
                                        <label for="hargaB">Persyaratan</label>
                                        <select id="cboalamat" name="persyaratan" class="form-control"  required>
                                            <?php
                                            $sq = mysqli_query($link, "SELECT persyaratan from notasewa");
                                            while ($row = mysqli_fetch_array($sq)) {
                                            }
                                            ?>
                                        <option value="KTP dan Kartu Keluarga" <?php if($row['persyaratan'] == 'KTP dan Kartu Keluarga'){ echo 'selected'; } ?>>KTP dan Kartu Keluarga</option>
                                        <option value="SIM dan STNK" <?php if($row['persyaratan'] == 'SIM dan STNK'){ echo 'selected'; } ?>>SIM dan STNK</option>
             
                                 </select>
                                    </fieldset>
                               
                            </div>

                               <div class="row">
                                        <fieldset  class="form-group col-xs-4">
                                                <label for="isiResep">Jumlah Sewa </label><br>
                                                <input type="text" class="form-control" name="jumlahsewa"/>
                                            </div>
                            

                                <div class="row">
                                <fieldset  class="form-group col-xs-6"> 
                                    <label for="isiResep">Durasi </label>
                                   <select class="form-control" id="cbodurasi" name="durasi" required>
                                     <?php
                                            $sq = mysqli_query($link, "SELECT * from kamera");
                                            while ($row = mysqli_fetch_array($sq)) {
                                            }
                                            ?>
                                           <option value="" disabled selected>Pilih Durasi Sewa</option>
                                           <option value="6" id="durasi6" price="">6 jam </option>
                                           <option value="12" id="durasi12" price="">12 jam </option>
                                           <option value="24" id="durasi24" price="">24 jam </option>
                                   </select>
                                    </div>
                                  
                                <div class="row">
                                <fieldset  class="form-group col-xs-4">    
                                <input type="hidden" name="hargasewa" id="hargasewa" value="0"><br>
                                    <center>Harga per Item :<h4 id="hargasewa1">Rp.xxx.xxx,-</h4></center><br>
                                   
                                 <div class="row">
                                   <fieldset  class="form-group col-xs-6">
                                    <input type="submit" class="btn btn-info" name="submit" value="Tambah">
                                </div>
                                    </form>
                                </div>
                            </div>
                            <br></br>
                        </div>
                            <div class="col-sm-15">
                                <h4> Data Lelang yang Tersedia :</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                        <thead style="text-align: center;">
                                            <tr >
                                                <th >ID</th>
                                                <th >NAMA PELANGGAN</th>
                                                <th >NAMA KAMERA</th>
                                                <th >TANGGAL PESAN</th>
                                                <th >TANGGAL SELESAI</th>
                                                <th >DURASI</th>
                                                <th >JUMLAH SEWA</th>
                                                <th >PERSYARATAN</th>
                                                <th >HARGA SEWA</th>
                                                <th >ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php
                                    include 'tanggal_indo.php';
                                    $sql = "SELECT *,
                                            CASE
                                             WHEN n.user_id = 0 THEN n.namapenyewa
                                             ELSE u.nama
                                            END AS nama_penyewa, n.id FROM notasewa n left join user u on u.id = n.user_id left join hub_notasewa_dan_kamera h on h.nota_id = n.id left join kamera k on k.id = h.kamera_id";
                                    $result = mysqli_query($link, $sql);
                                    if (!$result) {
                                        die("SQL Error:" . $sql);
                                    }
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<tr class= "row1">';
                                        echo "<th>" . $row['id'] . "</th>";
                                        echo "<td class='row1 col-sm-2'>" . $row['nama_penyewa'] . "</td>";
                                        echo "<td class='row1 col-sm-2'>" . $row['namakamera'] . "</td>";
                                        echo "<td class='row1 col-sm-6'>" . TanggalIndo($row['tgl_ambil']) . "</td>";
                                        echo "<td class='row1 col-sm-6'>" . TanggalIndo($row['tgl_kembali']) . "</td>";
                                        echo "<td class='row1 col-sm-2'>" . $row['durasi'] .  "Jam</td>";
                                        echo "<td class='row1 col-sm-2'>" . $row['jmlsewa'] . "</td>";
                                        echo "<td class='row1 col-sm-2'>" . $row['persyaratan'] . "</td>";
                                        echo "<td class='row1 col-xs-2'>Rp. " . number_format($row['grandtotal'], 0, ',', '.') . ",-</td>";
                                        echo "<td class='row1 col-sm-2'>

                                       <button type='button' class='btn btn-primary btn-sm' data-idn='" .$row['nota_id']. "'data-ki='" .$row['kamera_id']. "' data-ta='" .$row['tgl_ambil']. "' data-tk='" .$row['tgl_kembali']. "' data-dr='" .$row['durasi']. "' data-js='" .$row['jmlsewa']. "' data-ps='" .$row['persyaratan']. "'   data-toggle='modal' data-target='#editmanual'> <span class='glyphicon glyphicon-pencil'></span></button><br></br>

                                        <a href='process.php?act=deletemanual&idkam=" .$row['id']."'<button type='button' class='btn btn-danger btn-sm' ";?> onclick="return confirm('Apakah anda yakin untuk menghapus data ?');"><span class='glyphicon glyphicon-trash'></span></button></a>
                                            
                                            </td>
                                            <?php echo "</tr>";
                                    }?>

                                </tbody>
                            </table>
                        </div>

                           <div class="modal fade" id="editmanual" role="dialog">
                            <div class="modal-dialog" role="document">
                            
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                             <div class="row">
                                                <div class="col-sm-6">
                                                <form method="post" action="">
                                                <input type="hidden" name="idn" />
                                                <input type="hidden" name="ki" />
                                            </div>
                                        </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="isiResep">Tanggal Ambil </label><br>
                                                    <input type="datetime-local" class="form-control" name="tanggalambils"  id="tanggalambils" />
                                                </div>
                                            </div>
                                             <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="isiResep">Tanggal Kembali </label><br>
                                                    <input type="datetime-local" class="form-control" name="tanggalkembalis"  id="tanggalkembalis"/>
                                                </div>
                                            </div>
                                           
                                             <div class="row">
                                                <div class="col-sm-6">
                                                     <label for="isiResep">Durasi</label><br>
                                                 <input type="text" class="form-control" name="durasis"/>
                                                  </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-sm-6">
                                                     <label for="isiResep">Jumlah Sewa</label><br>
                                                 <input type="text" class="form-control" name="jumlahsewas"/>
                                                </div>
                                            </div>
                                           
                                           
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="subb" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                           


                        <script type="text/javascript"> 

                        $('#editmanual').on('show.bs.modal', function(e) {

                            var nota_id = $(e.relatedTarget).data('idn');
                            var kamera_id = $(e.relatedTarget).data('ki');
                            var tanggalambil = $(e.relatedTarget).data('ta');
                            var tanggalkembali = $(e.relatedTarget).data('tk');
                            var durasi = $(e.relatedTarget).data('dr');
                            var jumlahsewa = $(e.relatedTarget).data('js');
                           

                            $(e.currentTarget).find('input[name="idn"]').val(nota_id);
                            $(e.currentTarget).find('input[name="ki"]').val(kamera_id);
                            $(e.currentTarget).find('input[name="tanggalambils"]').val(tanggalambil);
                            $(e.currentTarget).find('input[name="tanggalkembalis"]').val(tanggalkembali);
                            $(e.currentTarget).find('input[name="durasis"]').val(durasi);
                            $(e.currentTarget).find('input[name="jumlahsewas"]').val(jumlahsewa);
                            

                        });
                        </script>
                        
                    </div>
                </div>
            </div>
                                           
<?php
if(isset($_POST['submit']))
    {
        //tabel notasewa
        $namapenyewa = $_POST['namapenyewa'];
        $persyaratan = $_POST['persyaratan'];
        $hargasewa= $_POST['hargasewa'];

        //tabel hub_notasewa_kamera 
        $namakamera = $_POST['namakamera'];
        $tanggalpesan = $_POST['waktumulai'];
        $tanggalakhir = $_POST['waktuselesai'];
        $durasi = $_POST['durasi'];       
        $jumlahsewa = $_POST['jumlahsewa'];

        //tanggal hari ini
        $tanggal = date('Y-m-d H:i:s');

        mysqli_query($link, "UPDATE kamera SET stoktotal = stoktotal - 1 WHERE id = $namakamera");

        $q1 = mysqli_query($link, "INSERT INTO `notasewa`(`namapenyewa`, `persyaratan`, `tanggalpesan`, `grandtotal`)
                                                  VALUES('$namapenyewa', '$persyaratan', '$tanggal', '$hargasewa')");
        
        $nota_id = mysqli_insert_id($link);

        $q2 = mysqli_query($link, "INSERT INTO `hub_notasewa_dan_kamera`(`nota_id`, `kamera_id`, `hargasewa`, `tgl_ambil`, `tgl_kembali`, `durasi`, `jmlsewa`)
                                                  VALUES('$nota_id', '$namakamera', '$hargasewa', '$tanggalpesan' , '$tanggalakhir', '$durasi', '$jumlahsewa')");

                
              if(!$q1 || $q2) {
                echo mysqli_error($link);
              }
               echo '<script language="javascript">
                    window.alert("Data berhasil diinput")
                    document.location.href="ordermanual.php"
                </script>';
    }

?>
<script>
  $(document).ready(function () {
    $('#cbokamera').on('change', function () {
        var id = $(this).find('option:selected').val();
        
        $.post("cek_kamera.php", {
            id_kamera: id
        }, function (data) {
            let obj = JSON.parse(data);
            $('#durasi6').attr('price', obj[0].harga_6jam + '-1');
            $('#durasi12').attr('price', obj[0].harga_12jam + '-2');
            $('#durasi24').attr('price', obj[0].harga_24jam + '-3');
        });

        $('#cbodurasi').val('');
    });

    $('#cbodurasi').on('change', function () {
        temp = $('#cbodurasi :selected').attr('price');
        temp2 = temp.substring(0, (temp.length-2))
        $('#hargasewa1').text(formatRupiah(temp2, 'Rp.'));
        $('#hargasewa').val(temp2);
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  });
</script>


<?php
if(isset($_POST['subb']))
    {
        //tabel notasewa
        $idn = $_POST['idn'];
        $ki = $_POST['ki'];
        $tanggalpesan = $_POST['tanggalambils'];
        $tanggalakhir = $_POST['tanggalkembalis'];
        $durasi = $_POST['durasis'];       
        $jumlahsewa = $_POST['jumlahsewas'];

       
        $w2 = mysqli_query($link,"UPDATE hub_notasewa_dan_kamera SET  tgl_ambil = '".$tanggalpesan."', tgl_kembali = '".$tanggalakhir."' , durasi ='".$durasi."', jmlsewa = '".$jumlahsewa."' WHERE nota_id = '".$idn."' AND kamera_id = '".$ki."'");

             if(!$w2) {
                echo mysqli_error($link);
            }
                echo '<script language="javascript">
                    window.alert("Data berhasil diinput")
                    document.location.href="ordermanual.php"
                </script>';
        
    }
           
?>
