<?php
session_start();
require './db.php';

// if(isset($_SESSION['admin']))
// {
//     $admin = $_SESSION['admin'];

//     if(!isset($_SESSION['admin_loggedIn']))
//     {
//         echo '<script language="javascript">';
//         echo 'document.location.href="login.php"';
//         echo '</script>';
//     }
//     else
//     {
//         $pengguna = $_SESSION['admin_loggedIn'];
//     }
// }
// else
// {
//     echo '<script language="javascript">';
//     echo 'window.alert("Anda harus login terlebih dahulu!");';
//     echo 'document.location.href="../login.php"';
//     echo '</script>';
// }
if(isset($_GET['id']))
{
    $idnota = $_GET['id'];
}
else
{
    // echo '<script language="javascript">';
    // echo 'document.location.href="ma.php"';
    // echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                            <?php 
                            // echo $pengguna; 
                            ?> 
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang </a>
                        </li>
                        <!--  <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li> -->
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h1 class="page-header">
                                <a href="masjual.php"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i><br>Order</button></a>
                                Detail Transaksi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order
                                </li>
                            </ol>
                        </div>
                        <div class="col-sm-11">
                            <h2>Detail Jual untuk ID Nota <?php echo $idnota; ?> </h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                    <thead>
                                        <tr >
                                            
                                            <th style="text-align: center;" >NAMA BARANG</th>
                                            <th style="text-align: center;" >KONDISI KAMERA</th>
                                            <th style="text-align: center;" >KATEGORI</th>
                                            <th style="text-align: center;" > HARGA JUAL</th>
                                            <th style="text-align: center; width:10%" >EDIT|HAPUS</th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';
                                        $sql = "SELECT * FROM postinganjualkamera WHERE id = '".$idnota."'";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . mysqli_error($link));
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                    echo '<tr>';
                                    //echo "<td class='col-sm-4'>" . $row['id'] . "</th>";
                                        echo "<td class='col-sm-4'>" . $row['namaproduk'] . "</th>";
                                        echo "<td class='col-sm-4'>" . $row['kondisikamera'] . "</th>";
                                        // echo "<td class='col-sm-4'>" . $row['deskripsi'] . "</th>";
                                        echo "<td class='col-sm-4'>" . $row['kategori'] . "</th>";
                                        echo "<td class='col-sm-8'>Rp.".number_format($row['hargajual'], 0, ',', '.') . ",-</td>";
                                        //echo "<td class='col-sm-4'>" . $row['foto'] . "</th>";
                                        // echo "<td class='col-sm-2'>" . TanggalIndo($row['tgl_ambil']) . "</td>";
                                        // echo "<td class='col-sm-4'>" . $row['durasi'] . " JAM</th>";

                                         echo "<td class='row1 col-sm-2'>

                                        <button type='button' class='btn btn-primary btn-sm' 
                                        data-id='" .$row['id']."' 
                                        data-nm='" .$row['namaproduk']."' 
                                        data-kn='" .$row['kondisikamera']. "' 
                                        data-kat='" .$row['kategori']. "' 
                                        data-har='" .$row['hargajual']. "' 
                                        data-toggle='modal' data-target='#editbrg'> <span class='glyphicon glyphicon-pencil'></span></button>"; ?>

                                        <a href='detil_jual.php?deleteid=<?php echo $row['id'] ?>'><button type='button' class='btn btn-danger btn-sm' onclick='return confirm("Apakah anda yakin untuk menghapus data?");'><span class='glyphicon glyphicon-trash'></span></button>
        
                                       </td>
                                                <?php echo '</tr>';

                                        }?>

                                  

                                    </tbody>
                                </table>
                              
                            </div>

                             <div class="modal fade" id="editbrg" role="dialog">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Barang</h4>
                                    </div>
                                    <div class="modal-body">
                                     <div style="margin-left: 5%;">  
                                      <div class="row">
                                        <div class="col-sm-8">
                                <form method="POST" action="">
                                   
                                    <input type="hidden" name="ids" />
                                    <label for="NamaPenerima">nama barang </label><br>
                                     <input type="text" name="namas" class="form-control"></span><br><br>
                                    <label for="NamaPenerima">kondisi kamera </label><br>
                                     <input type="text" name="kondisis" class="form-control"></span><br><br>
                                     <label for="NamaPenerima">kategori </label><br>
                                     <input type="text" name="kategoris" class="form-control"></span><br><br>
                                    <label for="NamaPenerima">harga jual </label><br>
                                     <input type="text" name="hargajuals" class="form-control"></span><br><br>
                               


                            <div class="row">
                                <button type="submit" name="updt" class="btn btn-primary">Ubah</button>
                                </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>  
           <script type="text/javascript">

        $('#editbrg').on('show.bs.modal', function(e) {

        var id = $(e.relatedTarget).data('id');
        var nm = $(e.relatedTarget).data('nm');
        var kn = $(e.relatedTarget).data('kn');
        var kat = $(e.relatedTarget).data('kat');
        var har = $(e.relatedTarget).data('har');
        
                                 
         $(e.currentTarget).find('input[name="ids"]').val(id);  
         $(e.currentTarget).find('input[name="namas"]').val(nm);
         $(e.currentTarget).find('input[name="kondisis"]').val(kn);
         $(e.currentTarget).find('input[name="kategoris"]').val(kat);
         $(e.currentTarget).find('input[name="hargajuals"]').val(har);
           
        });
        </script>
    </body>
</html>

 <?php 

if(isset($_POST['updt']))
    {
        $idnota = $_POST['ids'];
        $nm = $_POST['namas'];
        $kn = $_POST['kondisis'];
        $kat = $_POST['kategoris'];
        $har = $_POST['hargajuals'];
        
        //update detil jual mesin
        $t = mysqli_query($link, "UPDATE postinganjualkamera SET
        namaproduk = '" .$nm. "',
        kondisikamera = '" .$kn. "',
        kategori = '" .$kat. "',
        hargajual = '" .$har. "' WHERE id = '".$idnota."'"); 
        if (!$t) 
    {
        echo mysqli_error($link);
    }
    else
    {
         echo '<script language="javascript"> 
            var id="' .$idnota. '";
            alert("Data berhasil diubah")
          document.location.href="detil_jual.php?id="+id
          </script>';
    }
}
?>
<?php
if(isset($_GET['deleteid']))
{
    $idnota = $_GET['deleteid'];

    $a = mysqli_query($link, "DELETE FROM `postinganjualkamera` WHERE id ='".$idnota."'");
    if(!$a)
    {
        echo mysqli_error($link);

    }
    else
{
echo '<script language="javascript"> 
            var id="' .$idnota. '";
            alert("Data berhasil diubah")
          document.location.href="detil_jual.php?id="+id
          </script>';
}
}
        
 
 ?>
 