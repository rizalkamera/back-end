<?php
session_start();
require './db.php';

// if(isset($_SESSION['admin']))
// {
//     $admin = $_SESSION['admin'];

//     if(!isset($_SESSION['admin_loggedIn']))
//     {
//         echo '<script language="javascript">';
//         echo 'document.location.href="login.php"';
//         echo '</script>';
//     }
//     else
//     {
//         $pengguna = $_SESSION['admin_loggedIn'];
//     }
// }
// else
// {
//     echo '<script language="javascript">';
//     echo 'window.alert("Anda harus login terlebih dahulu!");';
//     echo 'document.location.href="../login.php"';
//     echo '</script>';
// }
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order - Mesin Kopi | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>


            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Order - Mesin Kopi
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Order - Mesin Kopi
                                </li>
                            </ol>
                            <h3>Buat Nota Penjualan Mesin Kopi</h3><br>
                        </div>

                        <div class="container">
                            <div class="col-lg-6">
                            <form action="masorder_mesin.php" method="post" class="form" role="form">
                                <div class="row">
                                    <fieldset class="form-group col-sm-6">
                                        <label for="namaB">Tanggal Transaksi</label>
                                        <input type="datetime-local" class="form-control" name="tgl" required>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-sm-5">
                                        <label for="hargaB">Pelanggan</label>
                                        <select name="pelanggan" class="form-control" required>
                                            <option value="">--Pilih Pelanggan--</option>
                                            <?php
                                            // $r = mysqli_query($link, "SELECT * from pelanggan WHERE hapuskah = '0'");
                                            // while($r1 = mysqli_fetch_array($r))
                                            // {
                                            //     echo '<option value="' .$r1['id_pel']. '">' .$r1['nama_pel']. '</option>';
                                            // }
                                            ?>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="row">
                                    <fieldset class="form-group col-xs-8">
                                        <input type="submit" class="btn btn-info" name="addnota" value="Tambahkan">
                                    </fieldset>
                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <h2>Data Order yang Tersedia Saat ini:</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped text-center">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">ID JUAL</th>
                                            <th style="text-align: center;">NAMA PRODUK</th>
                                            <th style="text-align: center;">KONDISI BARANG</th>
                                            <th style="text-align: center;">KOTA </th>
                                            <th style="text-align: center;">KATEGORI</th>
                                            <th style="text-align: center;">HARGA JUAL</th>
                                            <th style="text-align: center;">FOTO</th>
                                            <th style="text-align: center;">STOK</th>
                                            <th style="text-align: center;">EDIT | HAPUS | CETAK NOTA</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';
                                        //tampilkan data transaksi mesin saja (transaksi mesin adalah transaksi yg status_pesanan null)
                                        $sql = "SELECT *, n.no_rek as rek_pel FROM pelanggan p, nota_jual n LEFT JOIN pengiriman k ON n.id_notaJual = k.notaJual_id LEFT JOIN bank b on n.bank_id = b.id WHERE n.pelanggan_id = p.id_pel AND n.jenis_transaksi = 'mesin kopi' AND n.hapuskah = 0";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . $sql);
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '
                                        <tr class= "row1">';
                                            echo "
                                            <td class='row1 col-sm-1'>" . $row['id_notaJual'] . "</th>";
                                            echo "
                                            <td class='row1 col-sm-1'>" . $row['nama_pel'] . "</td>";
                                            echo "
                                            <td class='row1 col-sm-1'>" . TanggalIndo($row['tanggal_notaJual']) . "</td>";
                                            
                                            echo "
                                            <td class='row1 col-sm-1'>
                                                <a href='detil_order_mesin.php?id=" .$row['id_notaJual']. "'><button type='button' class='btn btn-info btn-sm'>Lihat Detil</button></a>
                                            </td>";

                                            if($row['biaya_pasang'] != 0)
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>Rp." .number_format($row['biaya_pasang'], 0, ',', '.') .",-" ."<br></br>
                                                <button type='button' class='btn btn-primary btn-sm' data-nota='" .$row['id_notaJual']. "' data-harga='" .$row['biaya_pasang']. "' data-toggle='modal' data-target='#edit_pasang'>Edit</button>
                                            </td>";
                                            }
                                            else
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>
                                                (belum diset)<br></br>
                                                <button type='button' class='btn btn-primary btn-sm' data-nota='" .$row['id_notaJual']. "' data-toggle='modal' data-target='#ins_pasang'>Input Biaya <br> Pasang</button>
                                            </td>";
                                            }

                                            if($row['grand_total'] != 0)
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>Rp." .number_format($row['grand_total'], 0, ',', '.') .",-" ."
                                            </td>";
                                            }
                                            else
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>(belum diset)</td>";
                                            }
                                            

                                            //jika jenis bayar tidak ada isinya, maka tampilkan button input data
                                            if($row['jenis_bayar'] == '')
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>
                                                Belum ada data<br></br>

                                                <button type='button' class='btn btn-primary btn-sm' data-not='".$row['id_notaJual']."' data-toggle='modal' data-target='#insbayar'>Input Detil <br> Pembayaran</button>
                                            </td>";
                                            }
                                            //jika ada isinya
                                            else
                                            {
                                                //cek jenis bayarnya apa
                                                //jika tunai, maka field nama bank, no rek, dan nama pemilik berisi (tidak ada data)
                                                if($row['jenis_bayar'] == 'tunai')
                                                {
                                                    echo "
                                            <td class='row1 col-sm-1'>
                                                Tunai <br></br>

                                                <button type='button' class='btn btn-primary btn-sm' data-not='".$row['id_notaJual']."' data-jen='".$row['jenis_bayar']."' data-toggle='modal' data-target='#editbayar'>Ubah</span></button>
                                            </td>";
                                                }
                                                //jika transfer, maka data dikirim seperti biasanya
                                                else if($row['jenis_bayar'] == 'transfer')
                                                {
                                                    echo "
                                            <td class='row1 col-sm-1'>
                                                Transfer<br></br>

                                                <button type='button' class='btn btn-info btn-sm' data-jen='".$row['jenis_bayar']."' data-bank='".$row['nama_bank']."' data-rek='".$row['rek_pel']."' data-milik='".$row['nama_pemilik']."' data-trans='".$row['nom_trf']."' data-toggle='modal' data-target='#showbayar'>Detail</button>

                                                <button type='button' class='btn btn-primary btn-sm' data-not='".$row['id_notaJual']."' data-jen='".$row['jenis_bayar']."' data-bank='".$row['id']."' data-rek='".$row['rek_pel']."' data-milik='".$row['nama_pemilik']."' data-trans='".$row['nom_trf']."' data-toggle='modal' data-target='#editbayar'>Ubah</button>
                                            </td>";
                                                }
                                            }


                                            //jika id pengiriman tidak ada isinya, maka tampilkan button input data
                                            if($row['id_pengiriman'] == '')
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>
                                                Belum ada data<br></br>
                                                <button type='button' class='btn btn-primary btn-sm' data-not='".$row['id_notaJual']."' data-toggle='modal' data-target='#inskrm'>Input Data <br> Pengiriman</button>
                                            </td>";
                                            }
                                            //jika ada isinya
                                            else
                                            {
                                                echo "
                                            <td class='row1 col-sm-1'>
                                                <button type='button' class='btn btn-info btn-sm' data-nama='".$row['nama_penerima']."' data-nohp = '".$row['noHP_penerima']."' data-alamat='".$row['alamat_penerima']."' data-kodep='".$row['kodePos']."' data-biaya='".$row['biaya_kirim']."' data-toggle='modal' data-target='#showkrm'>Detail</button>

                                                <button type='button' class='btn btn-primary btn-sm' data-not='".$row['id_notaJual']."' data-id='".$row['id_pengiriman']."' data-nama='".$row['nama_penerima']."' data-nohp = '".$row['noHP_penerima']."' data-alamat='".$row['alamat_penerima']."' data-kodep='".$row['kodePos']."' data-biaya='".$row['biaya_kirim']."' data-toggle='modal' data-target='#editkrm'>Ubah</button><br></br>
                                            ";
                                            ?>
                                                <a href='masorder_mesin.php?delkrm=<?php echo $row['id_pengiriman']?>&not=<?php echo $row['id_notaJual']?>'><button type='button' class='btn btn-danger btn-sm' onclick='return confirm("Apakah anda yakin untuk menghapus data?");'>Hapus</button></a>
                                            </td>
                                            <?php
                                            }
                                            
                                            //convert datetime agar bsa tampil di input type date
                                            $tangal = strftime('%Y-%m-%dT%H:%M:%S', strtotime($row['tanggal_notaJual']));
                                                

                                            echo "
                                            <td class='row1 col-sm-1'>
                                                <form method='POST' action='cetak_notaJual.php'>
                                                <input type='hidden' name='idnota' value='" .$row['id_notaJual']. "' />

                                                <button type='button' class='btn btn-primary btn-sm' data-idnota='" .$row['id_notaJual']. "' data-tanggal='" .$tangal. "' data-pel='" .$row['id_pel']."' data-toggle='modal' data-target='#editnota'> <span class='glyphicon glyphicon-pencil'></span></button><br></br>
                                            ";
                                            ?>

                                                <a href='process.php?act=deleteordermes&not=<?php echo $row['id_notaJual']?>'><button type='button' class='btn btn-danger btn-sm' onclick='return confirm("Apakah anda yakin untuk menghapus data?");'><span class='glyphicon glyphicon-trash'></span></button></a><br></br>

                                                <button type='submit' class='btn btn-success btn-sm' name='cetak'><i class='fa fa-print'></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="modal fade" id="ins_pasang" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Detail Jual</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="not" />
                                                    <label for="NamaPenerima">Biaya Pasang </label><br>
                                                    <input type="number" name="pasang" class="form-control"></span>
                                                </div>
                                                <div class="form-group row">
                                                    <input type="submit" class="btn btn-info" name="ins_psg" value="Simpan" onclick="return confirm('Apakah anda yakin untuk menyimpan data?');">
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#ins_pasang').on('show.bs.modal', function(e) {
                                
                                var not = $(e.relatedTarget).data('nota');
                                 
                                $(e.currentTarget).find('input[name="not"]').val(not);                          
                            });
                            </script>

                            <div class="modal fade" id="edit_pasang" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Detail Jual</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="notaa" />
                                                    <label for="NamaPenerima">Biaya Pasang </label><br>
                                                    <input type="number" name="harga" class="form-control"></span>
                                                </div>
                                                <div class="form-group row">
                                                    <input type="submit" class="btn btn-info" name="edit_psg" value="Ubah" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#edit_pasang').on('show.bs.modal', function(e) {
                                
                                var not = $(e.relatedTarget).data('nota');
                                var har = $(e.relatedTarget).data('harga');
                                 
                                $(e.currentTarget).find('input[name="notaa"]').val(not);
                                $(e.currentTarget).find('input[name="harga"]').val(har);                                 
                            });
                            </script>

                            <div class="modal fade" id="insbayar" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Input Detil Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="idnotaa" id="idnota">

                                                    <label for="NamaPenerima">Jenis Pembayaran </label><br>
                                                    <select name="jenis" id="jenis" class="form-control" style="width: 50%;">
                                                        <option value="" selected>--Pilih Jenis Pembayaran--</option>
                                                        <option value="tunai">Tunai</option>
                                                        <option value="transfer">Transfer</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Bank </label><br>
                                                    <select name="bank" id="bank" class="form-control" style="width: 50%;" disabled>
                                                        <option value="">--Pilih Bank--</option>
                                                        <?php
                                                        $y = mysqli_query($link, "SELECT * FROM bank WHERE hapuskah = '0'");
                                                        while($res_y = mysqli_fetch_array($y))
                                                        {
                                                            echo '
                                                        <option value="'.$res_y['id'].'">'.$res_y['nama_bank'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                    <!-- <input type="text" name="bank" id="bank" class="form-control" style="width: 50%;" /> -->
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nomor Rekening </label><br>
                                                    <input type="number" name="norek" id="norek" style="width: 50%;" class="form-control" disabled/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Pemilik </label><br>
                                                    <input type="text" name="milik" id="milik" class="form-control" style="width: 50%;" disabled/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nominal Transfer </label><br>
                                                    <input type="number" name="nom_trf" id="nom_trf" style="width: 50%;" class="form-control" disabled/>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="ins_bayar" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk menyimpan data?');">Simpan</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#insbayar').on('show.bs.modal', function(e) {

                                var notaa = $(e.relatedTarget).data('not');
                                $(e.currentTarget).find('input[name="idnotaa"]').val(notaa);  

                                //*********set jenis pembayaran************
                                var bank = document.getElementById('bank');
                                var jenis = document.getElementById('jenis');
                                var norek = document.getElementById('norek');
                                var milik = document.getElementById('milik');
                                var nom_trf = document.getElementById('nom_trf');

                                jenis.onchange = function(){
                                    //jika user memilih combobox transfer (transfer valuenya transfer),
                                    //maka textarea di disabled false (diaktifkan)
                                     if (jenis.value === "transfer") {
                                        bank.value = "";
                                        norek.value = "";
                                        milik.value = "";
                                        nom_trf.value = "";
                                        document.getElementById('bank').disabled='';
                                        document.getElementById('norek').disabled='';
                                        document.getElementById('milik').disabled='';
                                        document.getElementById('nom_trf').disabled='';
                                    } else {
                                        bank.value = "";
                                        norek.value = "";
                                        milik.value = "";
                                        nom_trf.value = "";
                                        document.getElementById('bank').disabled='true';
                                        document.getElementById('norek').disabled='true';
                                        document.getElementById('milik').disabled='true';
                                        document.getElementById('nom_trf').disabled='true';
                                    }
                                }                             
                            });
                            </script>


                            <div class="modal fade" id="showbayar" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detail Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Jenis Pembayaran </label><br>
                                                    <span class="jenis1"></span>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Bank </label><br>
                                                    <span class="bank1"></span>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nomor Rekening </label><br>
                                                    <span class="norek1"></span>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Pemilik </label><br>
                                                    <span class="milik1"></span>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nominal Transfer </label><br>
                                                    Rp. <span class="nominal1"></span>,-
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#showbayar').on('show.bs.modal', function(e) {

                                //buat method format rupiah
                                function currency(str) {
                                    var reverse = str.toString().split('').reverse().join(''),
                                    ribuan  = reverse.match(/\d{1,3}/g);
                                    ribuan  = ribuan.join('.').split('').reverse().join('');

                                    return ribuan;
                                }

                                var jen1 = $(e.relatedTarget).data('jen');
                                var bank1 = $(e.relatedTarget).data('bank');
                                var rek1 = $(e.relatedTarget).data('rek');
                                var milik1 = $(e.relatedTarget).data('milik');
                                var trans1 = $(e.relatedTarget).data('trans');

                                var transf = currency(trans1);
                                 
                                $(".jenis1").html(jen1);  
                                $(".bank1").html(bank1);
                                $(".norek1").html(rek1);
                                $(".milik1").html(milik1); 
                                $(".nominal1").html(transf);                                  
                            });
                            </script>

                            <div class="modal fade" id="editbayar" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Ubah Detil Pembayaran</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="idnotaa_edit" id="idnota_edit">
                                                    

                                                    <label for="NamaPenerima">Jenis Pembayaran </label><br>
                                                    <select name="jenis_edit" id="jenis_edit" class="form-control" style="width: 50%;">
                                                        <option value="">--Pilih Jenis Pembayaran--</option>
                                                        <option value="tunai">Tunai</option>
                                                        <option value="transfer">Transfer</option>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Bank </label><br>
                                                    <select name="bank_edit" id="bank_edit" class="form-control" style="width: 50%;" disabled>
                                                        <option value="">--Pilih Bank--</option>
                                                        <?php
                                                        $y = mysqli_query($link, "SELECT * FROM bank WHERE hapuskah = '0'");
                                                        while($res_y = mysqli_fetch_array($y))
                                                        {
                                                            echo '
                                                        <option value="'.$res_y['id'].'">'.$res_y['nama_bank'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <br>
                                                
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nomor Rekening </label><br>
                                                    <input type="text" name="norek_edit" id="norek_edit" style="width: 50%;" class="form-control" disabled/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Pemilik </label><br>
                                                    <input type="text" name="milik_edit" id="milik_edit" class="form-control" style="width: 50%;" disabled/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nominal Transfer </label><br>
                                                    <input type="text" name="nom_trf_edit" id="nom_trf_edit" class="form-control" style="width: 50%;" disabled/>
                                                </div>
                                                <div class="row">
                                                    <button type="submit" name="edit_bayar" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#editbayar').on('show.bs.modal', function(e) {
                                //set id nota jual ke input type hidden
                                var notaa = $(e.relatedTarget).data('not');
                                $(e.currentTarget).find('input[name="idnotaa_edit"]').val(notaa);

                                //tampung data jenis dari passingan button
                                var data_jenis = $(e.relatedTarget).data('jen');

                                 //*********set jenis pembayaran************
                                var bank = document.getElementById('bank_edit');
                                var jenis = document.getElementById('jenis_edit');
                                var norek = document.getElementById('norek_edit');
                                var milik = document.getElementById('milik_edit');
                                var nom_trf = document.getElementById('nom_trf_edit');

                                if (data_jenis =="tunai") 
                                {
                                    bank.value = "";
                                    norek.value = "";
                                    milik.value = "";
                                    nom_trf.value = "";
                                    document.getElementById('jenis_edit').value = 'tunai';
                                    document.getElementById('bank').disabled='true';
                                    document.getElementById('norek').disabled='true';
                                    document.getElementById('milik').disabled='true';
                                    document.getElementById('nom_trf').disabled='true';
                                }

                                // ---------------------last edit------------------------------------------------
                                else if(data_jenis == "transfer")
                                {
                                    //set disable false (diaktifkan)
                                    document.getElementById('bank_edit').disabled='';
                                    document.getElementById('norek_edit').disabled='';
                                    document.getElementById('milik_edit').disabled='';
                                    document.getElementById('nom_trf_edit').disabled='';

                                    //tampung value passingan dari buton
                                    var bank1 = $(e.relatedTarget).data('bank');
                                    var rek1 = $(e.relatedTarget).data('rek');
                                    var milik1 = $(e.relatedTarget).data('milik');
                                    var trans1 = $(e.relatedTarget).data('trans');

                                    //set value hasil tampungan ke dalam input text
                                    document.getElementById('jenis_edit').value = 'transfer';
                                    document.getElementById('bank_edit').value = bank1;
                                    $(e.currentTarget).find('input[name="norek_edit"]').val(rek1);
                                    $(e.currentTarget).find('input[name="milik_edit"]').val(milik1);
                                    $(e.currentTarget).find('input[name="nom_trf_edit"]').val(trans1);
                                }

                                jenis.onchange = function(){
                                    //jika user memilih combobox transfer (transfer valuenya transfer),
                                    //maka textarea di disabled false (diaktifkan)
                                     if (jenis.value === "transfer") {
                                        bank.value = "";
                                        norek.value = "";
                                        milik.value = "";
                                        nom_trf.value = "";
                                        document.getElementById('bank_edit').disabled='';
                                        document.getElementById('norek_edit').disabled='';
                                        document.getElementById('milik_edit').disabled='';
                                        document.getElementById('nom_trf_edit').disabled='';
                                    } else {
                                        bank.value = "";
                                        norek.value = "";
                                        milik.value = "";
                                        nom_trf.value = "";
                                        document.getElementById('bank_edit').disabled='true';
                                        document.getElementById('norek_edit').disabled='true';
                                        document.getElementById('milik_edit').disabled='true';
                                        document.getElementById('nom_trf_edit').disabled='true';
                                    }
                                }                                             
                            });
                            </script>

                            <div class="modal fade" id="inskrm" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Input Data Pengiriman</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="idnotaa" id="idnota">

                                                    <label for="NamaPenerima">Nama Penerima </label><br>
                                                    <input type="text" name="nama" style="width: 40%;" class="form-control" required/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Alamat Penerima </label><br>
                                                    <textarea name="alamat" id="bank" class="form-control" rows="3" cols="30" required style="width: 50%;"></textarea>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Kode Pos </label><br>
                                                    <input type="number" name="kodep" style="width: 20%;" class="form-control" required/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nomor Telepon Penerima </label><br>
                                                    <input type="number" name="notelp" id="norek" style="width: 30%;" class="form-control" required/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Biaya Kirim </label><br>
                                                    <input type="number" name="ongkirp" style="width: 20%;" class="form-control" required/>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="ins_krm" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk menyimpan data pegiriman ini?');">Simpan</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('#inskrm').on('show.bs.modal', function(e) {

                                var notaa = $(e.relatedTarget).data('not');
                                $(e.currentTarget).find('input[name="idnotaa"]').val(notaa); 

                            });

                                 
                            </script>

                            <div class="modal fade" id="showkrm" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Data Pengiriman</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nama Penerima </label><br>
                                                    <span class="nam"></span>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Alamat Penerima </label><br>
                                                    <span class="ala"></span>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Kode Pos </label><br>
                                                    <span class="kod"></span>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nomor Telepon Penerima </label><br>
                                                    <span class="not"></span>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Biaya Kirim </label><br>
                                                    Rp. <span class="ongk"></span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#showkrm').on('show.bs.modal', function(e) {

                                 //buat method format rupiah
                                function currency(str) {
                                    var reverse = str.toString().split('').reverse().join(''),
                                    ribuan  = reverse.match(/\d{1,3}/g);
                                    ribuan  = ribuan.join('.').split('').reverse().join('');

                                    return ribuan;
                                }
                                
                                var nama1 = $(e.relatedTarget).data('nama');
                                var alamat1 = $(e.relatedTarget).data('alamat');
                                var kodep1 = $(e.relatedTarget).data('kodep');
                                var nohp1 = $(e.relatedTarget).data('nohp');
                                var biaya1 = $(e.relatedTarget).data('biaya');

                                var biaya2 = currency(biaya1);

                                $(".nam").html(nama1);  
                                $(".ala").html(alamat1); 
                                $(".kod").html(kodep1);
                                $(".not").html(nohp1);
                                $(".ongk").html(biaya2);                                  
                            });
                            </script>

                            <div class="modal fade" id="editkrm" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Data Pengiriman</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="nota_id" >
                                                    <input type="hidden" name="idkrm" >

                                                    <label for="NamaPenerima">Nama Penerima </label><br>
                                                    <input type="text" name="nama1" id="norek" style="width: 50%;" class="form-control"/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Alamat Penerima </label><br>
                                                    <input type="text" name="alamat1" id="bank" class="form-control" style="width: 50%;"/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Kode Pos </label><br>
                                                    <input type="text" name="kodepos1" id="norek" style="width: 50%;" class="form-control"/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Nomor Telepon Penerima </label><br>
                                                    <input type="text" name="notelp1" id="norek" style="width: 50%;" class="form-control"/>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Biaya Kirim </label><br>
                                                    <input type="text" name="ong" style="width: 50%;" class="form-control"/>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="edit_krm" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#editkrm').on('show.bs.modal', function(e) {
                                
                                var not0 = $(e.relatedTarget).data('not');
                                var id0 = $(e.relatedTarget).data('id');
                                var nama0 = $(e.relatedTarget).data('nama');
                                var nohp0 = $(e.relatedTarget).data('nohp');
                                var alamat0 = $(e.relatedTarget).data('alamat');
                                var kodep0 = $(e.relatedTarget).data('kodep');
                                var biaya0 = $(e.relatedTarget).data('biaya');

                                $(e.currentTarget).find('input[name="nota_id"]').val(not0);
                                $(e.currentTarget).find('input[name="idkrm"]').val(id0);
                                $(e.currentTarget).find('input[name="nama1"]').val(nama0);  
                                $(e.currentTarget).find('input[name="alamat1"]').val(alamat0);  
                                $(e.currentTarget).find('input[name="kodepos1"]').val(kodep0);  
                                $(e.currentTarget).find('input[name="notelp1"]').val(nohp0);                             
                                $(e.currentTarget).find('input[name="ong"]').val(biaya0);  
                                                              
                            });
                            </script>

                            <div class="modal fade" id="editnota" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Ubah Data Nota Jual Mesin Kopi</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="row">
                                                    <label for="NamaPenerima">Tanggal</label><br>
                                                    <form method="POST" action="masorder_mesin.php">
                                                    <input type="hidden" name="idnot" id="idnot">
                                                    <input type="datetime-local" style="width: 50%;" name="tanggal_edit" id="tanggal_edit" class="form-control">
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <label for="NamaPenerima">Pelanggan </label><br>
                                                    <select name="pelang" id="pelang" style="width: 50%;" class="form-control">
                                                        <?php
                                                        $g = mysqli_query($link, "SELECT * FROM pelanggan WHERE hapuskah = '0'");
                                                        while ($res_g = mysqli_fetch_array($g)) 
                                                        {
                                                            echo '
                                                        <option value="'.$res_g['id_pel'].'">'.$res_g['nama_pel'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <button type="submit" name="edit_not" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#editnota').on('show.bs.modal', function(e) {
                                var id1 = $(e.relatedTarget).data('idnota');
                                var tgl = $(e.relatedTarget).data('tanggal');
                                var pel1 = $(e.relatedTarget).data('pel');
                                 
                                // $(e.currentTarget).find('input[name="idnot"]').val(id1); 
                                $("#idnot").val(id1);
                                $(e.currentTarget).find('input[name="tanggal_edit"]').val(tgl);
                                // $(".waselesai").html(nju);
                                $("#pelang").val(pel1);                     
                            });
                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
if(isset($_POST['addnota']))
{
    $tanggal = $_POST['tgl'];
    $pel = $_POST['pelanggan'];
    
    $p = mysqli_query($link, "INSERT INTO nota_jual(tanggal_notaJual,jenis_transaksi,hapuskah,pelanggan_id) 
                                            VALUES('$tanggal','mesin kopi','0','$pel')");
    
    echo '<script language="javascript"> 
          alert("Data berhasil terinput.")
          document.location.href="masorder_mesin.php"
          </script>';

}

if(isset($_POST['editnota']))
{
    $not = $_POST['idnotaa'];
    $pelanggan = $_POST['idp'];
    $tgl = $_POST['statu'];
    $email = $_POST['mail'];
    
    $r = mysqli_query($link, "UPDATE nota_jual SET bukti_valid = '1', status_pesanan = 'proses pengiriman' WHERE id_notaJual = '" .$nota. "'");

    echo '<script language="javascript"> 
      alert("Data berhasil diubah.")
      document.location.href="masorder_mesin.php"
      </script>';

}

if(isset($_POST['deletenota']))
{

}

if(isset($_POST['ins_psg'])) 
{
    $nota = $_POST['not'];
    $biaya = $_POST['pasang'];

    //simpan biaya pasang
    $g = mysqli_query($link, "UPDATE nota_jual SET biaya_pasang = '" .$biaya. "' WHERE id_notaJual = '" .$nota. "'");

    //update grand total
    //1. ambil total belanja, biaya kirim
    $y1 = mysqli_query($link, "SELECT * FROM nota_jual n LEFT JOIN pengiriman k on n.id_notaJual = k.notaJual_id WHERE n.id_notaJual = '".$nota."'");
    $res_y1 = mysqli_fetch_array($y1);
    $total = $res_y1['total_belanja'];
    $krm = $res_y1['biaya_kirim'];

    //2. jumlahkan keseluruhan antara total belanja, biaya pasang, dan biaya kirim (jika ada)
    $grand = $total + $biaya + $krm;

    //3. update grand total nya
    $t = mysqli_query($link, "UPDATE nota_jual SET grand_total = '".$grand. "' WHERE id_notaJual = '" .$nota. "'");
    
    echo '<script language="javascript"> 
      alert("Data berhasil disimpan.")
      document.location.href="masorder_mesin.php"
      </script>';
}

if(isset($_POST['edit_psg'])) 
{
    $nota = $_POST['notaa'];
    $biaya = $_POST['harga'];
    $grand = 0;

    //simpan biaya pasang
    $g = mysqli_query($link, "UPDATE nota_jual SET biaya_pasang = '" .$biaya. "' WHERE id_notaJual = '" .$nota. "'");

    //update grand total
    //1. ambil total belanja, biaya kirim
    $y1 = mysqli_query($link, "SELECT * FROM nota_jual n LEFT JOIN pengiriman k on n.id_notaJual = k.notaJual_id WHERE n.id_notaJual = '".$nota."'");
    $res_y1 = mysqli_fetch_array($y1);
    $total = $res_y1['total_belanja'];
    $krm = $res_y1['biaya_kirim'];

    //2. jumlahkan keseluruhan antara total belanja, biaya pasang, dan biaya kirim (jika ada)
    $grand = $total + $biaya + $krm;

    //3. update grand total nya
    $t = mysqli_query($link, "UPDATE nota_jual SET grand_total = '".$grand. "' WHERE id_notaJual = '" .$nota. "'");
    
    echo '<script language="javascript"> 
      alert("Data berhasil disimpan.")
      document.location.href="masorder_mesin.php"
      </script>';
}
if (isset($_POST['ins_bayar'])) 
{
    $nota = $_POST['idnotaa'];
    $jenis1 = $_POST['jenis'];
    if ($jenis1 == "tunai") 
    {
        //tidak perlu tampung bank, norek, dan nama pemilik

        //langsung update jenis pembayaran menjadi tunai
        $k = mysqli_query($link, "UPDATE nota_jual SET jenis_bayar = 'tunai' WHERE id_notaJual = '" .$nota. "'");
        if(!$k)
        {
            echo mysqli_error($link);
        }
        else
        {
             echo '<script language="javascript"> 
              alert("Data berhasil disimpan.")
              document.location.href="masorder_mesin.php"
              </script>';
        }
    }
    else if ($jenis1 == "transfer") 
    {
        //tampung bank, norek, dan nama pemilik
        $bank1 = $_POST['bank'];
        $norek1 = $_POST['norek'];
        $milik1 = $_POST['milik'];
        $nom_trf1 = $_POST['nom_trf'];
        

        //update data pembayaran
        $k1 = mysqli_query($link, "UPDATE nota_jual SET jenis_bayar = 'transfer', bank_id = '".$bank1."', no_rek = '".$norek1."', nama_pemilik = '".$milik1."', nom_trf = '".$nom_trf1."' WHERE id_notaJual = '" .$nota. "'");

        if(!$k1)
        {
            echo mysqli_error($link);
        }
        else
        {
             echo '<script language="javascript"> 
              alert("Data berhasil disimpan.")
              document.location.href="masorder_mesin.php"
              </script>';
        }
    }
}
if (isset($_POST['edit_bayar'])) 
{
    $id = $_POST['idnotaa_edit'];
    $jenis = $_POST['jenis_edit'];

    if($jenis == 'tunai')
    { 
        //set jenis bayar jadi tunai, set null kolom bankID, noRek, NamaPemilik, nominal_transfer
        $kp = mysqli_query($link, "UPDATE nota_jual SET jenis_bayar = 'tunai', bank_id = null, no_rek = null, nama_pemilik = null, nom_trf = 0 WHERE id_notaJual = '" .$id. "'");
        if(!$kp)
        {
            echo mysqli_error($link);
        }
        else
        {
             echo '<script language="javascript"> 
              alert("Data berhasil diubah.")
              document.location.href="masorder_mesin.php"
              </script>';
        }
    }
    else if ($jenis == 'transfer') 
    {
        //tampung inputan yang lainnya
        $bank = $_POST['bank_edit'];
        $norek = $_POST['norek_edit'];
        $milik = $_POST['milik_edit'];
        $nom_trf = $_POST['nom_trf_edit'];
        $id = $_POST['idnotaa_edit'];
        
        //set jenis bayar jadi transfer, update kolom bankID, noRek, NamaPemilik, nominal_transfer sesuai dengan inputan
       $k1 = mysqli_query($link, "UPDATE nota_jual SET jenis_bayar = 'transfer', bank_id = '".$bank."', no_rek = '".$norek."', nama_pemilik = '".$milik."', nom_trf = '".$nom_trf."' WHERE id_notaJual = '" .$id. "'");
       
       if(!$k1)
        {
            echo mysqli_error($link);
        }
        else
        {
             echo '<script language="javascript"> 
              alert("Data berhasil diubah.")
              document.location.href="masorder_mesin.php"
              </script>';
        }

    }
    
}
if(isset($_POST['ins_krm']))
{
    $idnotaa1 = $_POST['idnotaa'];
    $nama1 = $_POST['nama'];
    $alamat1 = $_POST['alamat'];
    $kodep1 = $_POST['kodep'];
    $notelp1 = $_POST['notelp'];
    $ongkirp1 = $_POST['ongkirp'];

    //insert data pengiriman
    $pk = mysqli_query($link, "INSERT INTO pengiriman(nama_penerima, noHP_penerima, alamat_penerima, kodePos, biaya_kirim, hapuskah, notaJual_id) VALUES('$nama1','$notelp1','$alamat1','$kodep1','$ongkirp1',0,'$idnotaa1')");

    //update grand total
    //1. ambil total belanja dan biaya pasang
    $f = mysqli_query($link, "SELECT * FROM nota_jual WHERE id_notaJual = '".$idnotaa1."'");
    $res_f = mysqli_fetch_array($f);
    $total = $res_f['total_belanja'];
    $pasang = $res_f['biaya_pasang'];


    //2. tambahkan biaya pasang, total belanja, dan biaya kirim
    $grand = $total + $pasang + $ongkirp1;


    //3. update grand total
    $g = mysqli_query($link, "UPDATE nota_jual SET grand_total = '".$grand."' WHERE id_notaJual = '".$idnotaa1."'");
    
    if(!$pk)
    {
        echo mysqli_error($link);
    }
    else
    {
        echo '<script language="javascript"> 
          alert("Data berhasil terinput.")
          document.location.href="masorder_mesin.php"
          </script>';
    }
}

if(isset($_POST['edit_krm']))
{
    $nota0 = $_POST['nota_id'];
    $idkrm0 = $_POST['idkrm'];
    $nama0 = $_POST['nama1'];
    $alamat0 = $_POST['alamat1'];
    $kodepos0 = $_POST['kodepos1'];
    $notelp0 = $_POST['notelp1'];
    $ong0 = $_POST['ong'];

    //update data pengiriman
    $pp = mysqli_query($link, "UPDATE pengiriman SET nama_penerima = '".$nama0."', noHP_penerima = '".$notelp0."', alamat_penerima = '".$alamat0."', kodePos = '".$kodepos0."', biaya_kirim = '".$ong0."' 
        WHERE id_pengiriman = '".$idkrm0."'");

    //update grand total
    //1. ambil total belanja dan biaya pasang
    $f = mysqli_query($link, "SELECT * FROM nota_jual WHERE id_notaJual = '".$nota0."'");
    $res_f = mysqli_fetch_array($f);
    $total = $res_f['total_belanja'];
    $pasang = $res_f['biaya_pasang'];


    //2. tambahkan biaya pasang, total belanja, dan biaya kirim
    $grand = $total + $pasang + $ong0;


    //3. update grand total
    $g = mysqli_query($link, "UPDATE nota_jual SET grand_total = '".$grand."' WHERE id_notaJual = '".$nota0."'");
    
    
    if(!$pp)
    {
        echo mysqli_error($link);
    }
    else
    {
        echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="masorder_mesin.php"
          </script>';
    }
}
if (isset($_GET['delkrm'])) 
{

    $id_krm = $_GET['delkrm'];
    $id_not = $_GET['not'];

    //delete data pengiriman
    $v = mysqli_query($link, "DELETE FROM pengiriman WHERE id_pengiriman = '".$id_krm."'");


    //update grand total
    //1. ambil total belanja dan biaya pasang
    $f = mysqli_query($link, "SELECT * FROM nota_jual WHERE id_notaJual = '".$id_not."'");
    $res_f = mysqli_fetch_array($f);
    $total = $res_f['total_belanja'];
    $pasang = $res_f['biaya_pasang'];


    //2. tambahkan biaya pasang, total belanja, dan biaya kirim
    $grand = $total + $pasang;


    //3. update grand total
    $g = mysqli_query($link, "UPDATE nota_jual SET grand_total = '".$grand."' WHERE id_notaJual = '".$id_not."'");

     echo '<script language="javascript"> 
          alert("Data berhasil dihapus.")
          document.location.href="masorder_mesin.php"
          </script>';
}
if (isset($_POST['edit_not'])) 
{
    $idnot1 = $_POST['idnot'];
    $tanggal1 = $_POST['tanggal_edit'];
    $pelang = $_POST['pelang'];

    $g = mysqli_query($link, "UPDATE nota_jual SET tanggal_notaJual = '".$tanggal1."', pelanggan_id = '".$pelang."' WHERE id_notaJual = '".$idnot1."'");

    echo '<script language="javascript"> 
          alert("Data berhasil diubah.")
          document.location.href="masorder_mesin.php"
          </script>';
}


















?>
