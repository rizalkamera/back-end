<?php
session_start();
require './db.php';

if(isset($_GET['id']))
{
    $nota_id = $_GET['id'];
}
else
{

    // echo '<script language="javascript">';
    // echo 'document.location.href="masorder.php"';
    // echo '</script>';
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Order | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--javascript calendar-->

        <!-- jquery js -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>

        <script src="js/bootstrap.min.js"></script>
            <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    
    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                 <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-camera"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="maskamera.php"> Kamera </a>
                                </li>
                                <li>
                                    <a href="maslensa.php"> Lensa </a>
                                </li>
                                <li>
                                    <a href="masasessoris.php"> Aksesoris</a>
                                </li>
                                 <li>
                                    <a href="masterinputlelang.php"> Lelang</a>
                                </li>
                            </ul>
                        </li>
                         <li>
                            <a href="maspemesanan.php"><i class="fa fa-fw fa-edit"></i> Master Pemesanan Barang</a>
                        </li>
                         <li>
                            <a href="masdenda.php"><i class="fa fa-fw fa-edit"></i> Master Pegembalian</a>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                        <li>
                            <a href="maslelang.php"><i class="fas fa-hammer "></i> Master Lelang </a>
                        </li>
                        

                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li>
                            <a href="masjual.php"><i class="fa fa-wrench"></i>  Master Jual </a>
                        </li>
                         <li>
                            <a href="laporan.php"><i class="fa fa-fw fa-edit"></i>Laporan</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            
                            <h1 class="page-header">
                                <a href="masdenda.php"><button class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i><br>Denda</button></a>
                                Detail Transaksi Denda
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-book"></i> Master Denda
                                </li>
                            </ol>
                        </div>
            
                    <!--tabel kategori-->
                        <div class="col-sm-15">
                            <h2>Detail Transaksi untuk ID Nota <?php echo $nota_id; ?> </h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" style="text-align: center;">
                                    <thead>
                                        <tr >
                                        <th style="text-align: center;" >ID</th>
                                        <th style="text-align: center;">NAMA KAMERA</th>
                                        <th style="text-align: center;">DURASI</th>
                                        <th style="text-align: center;">KONDISI</th>
                                        <th style="text-align: center;">TANGGAL AMBIL</th>
                                        <th style="text-align: center;">TANGGAL KEMBALI</th>
                                        <th style="text-align: center;">DENDA TERLAMBAT</th>
                                        <th style="text-align: center;">DENDA KERUSAKAN</th>
                                        <th style="text-align: center;">UBAH DENDA</th>
                                         <th style="text-align: center;">HAPUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';
        
                                           $sql = "SELECT *, SUM(h.hargasewa+h.denda+h.rusak)as grandtotal
                                                    FROM user p, notasewa n, hub_notasewa_dan_kamera h, kamera b
                                                    WHERE p.id = n.user_id AND h.nota_id= '".$nota_id."' AND h.kamera_id = b.id 
                                                    AND n.hapuskah ='0' AND p.hapuskah ='0' group by h.kamera_id";
                                          //  //CADANGAN         
                                          // $sql = "SELECT  n.grandtotal,n.kondisikamera, b.namakamera, p.nama, h.hargasewa, h.durasi, h.rusak,h.nota_id,h.kamera_id, h.denda, h.tgl_ambil, h.tgl_kembali, h.kondisikamera, SUM(h.hargasewa+h.denda+h.rusak)as grandtotal
                                          //           FROM user p, notasewa n, hub_notasewa_dan_kamera h, kamera b
                                          //           WHERE p.id = n.user_id AND h.nota_id= '".$nota_id."' AND h.kamera_id = b.id 
                                          //           AND n.hapuskah ='0' AND p.hapuskah ='0' group by h.kamera_id";            
                   
                                    $result = mysqli_query($link, $sql);
                                    if (!$result) {
                                        die("SQL Error:" . $sql);
                                    }
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<tr class= "row1">';
                                        echo "<td class='row1 col-xs-1'>" . $row['kamera_id'] . "</td>";
                                        echo "<td class='row1 col-xs-1'>" . $row['namakamera'] . "</td>";
                                        echo "<td class='row1 col-xs-1'>" . $row['durasi'] . " JAM</td>";
                                        echo "<td class='row1 col-xs-1'>" . $row['kondisikamera'] . "</td>";      
                                        echo "<td class='row1 col-xs-1'>" . TanggalIndo($row['tgl_ambil']) . "</td>";
                                        echo "<td class='row1 col-xs-1'>" . TanggalIndo($row['tgl_kembali']) . "</td>";
                                        echo "<td class='row1 col-xs-2'>Rp. " . number_format($row['denda'], 0, ',', '.') . ",-</td>";
                                        echo "<td class='row1 col-xs-1'>Rp. " . number_format($row['rusak'], 0, ',', '.') . ",-</td>";
                                        // echo "<td class='row1 col-xs-1'>Rp. " . number_format($row['grandtotal'], 0, ',', '.') . ",-</td>";

                                      
                                          $tanggalambils = strftime('%Y-%m-%dT%H:%M:%S', strtotime($row['tgl_ambil']));
                                          $tanggalkembalis = strftime('%Y-%m-%dT%H:%M:%S', strtotime($row['tgl_kembali']));

                                        echo '<td class="row1 col-sm-2">
                                         <button type="button" class="btn btn-primary btn-sm" data-idnota="'.$row['nota_id'].'" data-idkamera="'.$row['kamera_id'].'" data-hs="'.$row['hargasewa'].'"data-dr="'.$row['durasi'].'"data-ta="'.$tanggalambils.'"data-tk="'.$tanggalkembalis.'" data-dd="'.$row['denda'].'" data-toggle="modal" data-target="#editdenda">Denda Terlambat</button><br><br>
                                        
                                         <button type="button" class="btn btn-primary btn-sm" data-idnota="'.$row['nota_id'].'" data-idkamera="'.$row['kamera_id'].'" data-hs="'.$row['hargasewa'].'" data-ks="'.$row['rusak'].'" data-kk="'.$row['kondisikamera'].'" data-toggle="modal" data-target="#editkerusakan">Denda Kerusakan</button>'
                                         ; 
                                          echo "<td class='row1 col-sm-2'>
                                            <a href='process.php?act=deletekat&idnota=<?php echo ".$row['nota_id']." ?>&idbar=<?php echo". $row['kamera_id']." ?>'><button type='button' class='btn btn-danger btn-sm' onclick='return confirm('Apakah anda yakin untuk menghapus data?');'><span class='glyphicon glyphicon-trash'></span></button>
                                           
                                         </td>
                                            <?php echo '</tr>";

                                    }?>

                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="editdenda" role="dialog">
                            <div class="modal-dialog" role="document">
                            
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Denda</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                             <div class="row">
                                                <div class="col-sm-6">
                                                <form method="post" action="">
                                                <input type="hidden" name="idnota" />
                                                <input type="hidden" name="idkamera" />
                                                  </div>
                                            </div>
                                           
                                             <div class="row">
                                                <div class="col-sm-6">
                                                <label for="isiResep">Harga Sewa </label><br>
                                                <input type="text" class="form-control" name="hargasewas"/>
                                            </div>
                                            </div>
                                           
                                            <div class="row">
                                                <div class="col-sm-6">
                                                <label for="isiResep">Durasi </label><br>
                                                <input type="text" class="form-control" name="durasis"/>
                                            </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="isiResep">Tanggal Ambil </label><br>

                                                    <input type="datetime-local" class="form-control" name="tanggalambils"  id="tanggalambils" />
                                                </div>
                                            </div>
                                             <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="isiResep">Tanggal Kembali </label><br>
                                                    <input type="datetime-local" class="form-control" name="tanggalkembalis"  id="tanggalkembalis"/>
                                                </div>
                                            </div>
                                           
                                             <div class="row">
                                                <div class="col-sm-6">
                                                     <label for="isiResep">Denda</label><br>
                                                <select style="width: 100%" id="cbodenda" class="form-control" name="dendas" required>
                                                     <option value="0" <?php if($row['denda'] == '0'){ echo 'selected'; } ?>>--pilih denda--</option>
                                                    <option value="10000" <?php if($row['denda'] == '10000'){ echo 'selected'; } ?>>1 jam</option>
                                                    <option value="20000" <?php if($row['denda'] == '20000'){ echo 'selected'; } ?>>2 jam</option>
                                                    <option value="30000" <?php if($row['denda'] == '20000'){ echo 'selected'; } ?>>3 jam</option>
                                                    <option value="40000" <?php if($row['denda'] == '40000'){ echo 'selected'; } ?>>4 jam</option>
                                                    <option value="50000" <?php if($row['denda'] == '50000'){ echo 'selected'; } ?>>5 jam</option>
                                                    <option value="60000" <?php if($row['denda'] == '60000'){ echo 'selected'; } ?>>6 jam</option>
                                                    <option value="70000" <?php if($row['denda'] == '70000'){ echo 'selected'; } ?>>7 jam</option>
                                             </select>
                                                    *Denda 10.000/ Jam
                                            
                                            
                                        </div>
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="subb" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                           
                       
                        <script type="text/javascript"> 

                        $('#editdenda').on('show.bs.modal', function(e) {

                            var idnota = $(e.relatedTarget).data('idnota');
                            var idkamera = $(e.relatedTarget).data('idkamera');
                            var hargasewa = $(e.relatedTarget).data('hs');
                            var durasi = $(e.relatedTarget).data('dr');
                            var tanggalambil = $(e.relatedTarget).data('ta');
                            var tanggalkembali = $(e.relatedTarget).data('tk');
                            var denda = $(e.relatedTarget).data('dd');

                            $(e.currentTarget).find('input[name="idnota"]').val(idnota);
                            $(e.currentTarget).find('input[name="idkamera"]').val(idkamera);
                            $(e.currentTarget).find('input[name="hargasewas"]').val(hargasewa);
                            $(e.currentTarget).find('input[name="durasis"]').val(durasi);
                            $(e.currentTarget).find('input[name="tanggalambils"]').val(tanggalambil);
                            $(e.currentTarget).find('input[name="tanggalkembalis"]').val(tanggalkembali);
                            $(e.currentTarget).find('input[name="dendas"]').val(denda);

                        });
                        </script>
                        
                    </div>
                </div>
            </div>
                     <div class="modal fade" id="editkerusakan" role="dialog">
                            <div class="modal-dialog" role="document">
                            
                              <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Denda Kerusakan</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div style="margin-left: 5%;">
                                             <div class="row">
                                                <div class="col-sm-6">
                                                <form method="post" action="">
                                                <input type="hidden" name="idnota" />
                                                <input type="hidden" name="idkamera" />
                                                  </div>
                                            </div>
                                             <div class="row">
                                                <div class="col-sm-6">
                                                <label for="isiResep">Harga Sewa </label><br>
                                                <input type="text" class="form-control" name="hargasewa"/>
                                            </div>
                                            </div>
                                           
                                             <div class="row">
                                                <div class="col-sm-6">
                                                <label for="isiResep">Biaya Kerusakan </label><br>
                                                <input type="text" class="form-control" name="biayakerusakan"  placeholder="Biaya Kerusakan"/>
                                            </div>
                                            </div>
                                           
                                         
                                           
                                             <div class="row">
                                                <div class="col-sm-6">
                                                     <label for="isiResep">Kondisi Kerusakan</label><br>
                                                <select style="width: 100%" id="cbokondisi" class="form-control" name="rusaks" required>
                                                

                                                    <option value="normal">Normal</option>
                                                    <option value="lecet" >Lecet</option>
                                                    <option value="mati total" >Mati Total</option>
                                                    <option value="kamera error">Kamera Erorr</option>
                                                 
                                             </select>

                                            </div>
                                            </div>

                                            
                                        </div>
                                            <br>
                                            <div class="row">
                                                <button type="submit" name="kerusakan" class="btn btn-primary" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">Ubah</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <script type="text/javascript"> 

                        $('#editkerusakan').on('show.bs.modal', function(e) {

                            var idnota = $(e.relatedTarget).data('idnota');
                            var idkamera = $(e.relatedTarget).data('idkamera');
                            var hargasewa = $(e.relatedTarget).data('hs');
                            var kerusakan = $(e.relatedTarget).data('ks');
                            var kondisikamera = $(e.relatedTarget).data('kk');
                            

                            $(e.currentTarget).find('input[name="idnota"]').val(idnota);
                            $(e.currentTarget).find('input[name="idkamera"]').val(idkamera);
                            $(e.currentTarget).find('input[name="hargasewa"]').val(hargasewa);
                            $(e.currentTarget).find('input[name="biayakerusakan"]').val(kerusakan);
                            // $(e.currentTarget).find('input[name="rusaks"]').val(kondisikamera);
                            // $("#cbodenda").val(kondisikamera);
                            document.getElementById('cbokondisi').value = kondisikamera;


                        
                        });
                        </script>
                        
                    </div>
                </div>
            </div>  

            <!-- /#wrapper -->

            
    </body>
</html>
<?php

?>
<?php
if(isset($_POST['subb']))
{

    
    $nota_id = $_POST['idnota'];
    $kamera_id = $_POST['idkamera'];
    $hargasewa = $_POST['hargasewas'];
    $durasi = $_POST['durasis'];
    $tanggalambil = $_POST['tanggalambils'];
    $tanggalkembali = $_POST['tanggalkembalis'];
    $denda = $_POST['dendas'];
  

    $w = mysqli_query($link, "UPDATE hub_notasewa_dan_kamera SET hargasewa = '" .$hargasewa. "', durasi = '" .$durasi. "', tgl_ambil = '" .$tanggalambil. "', tgl_kembali = '" .$tanggalkembali. "', denda = '" .$denda. "'  WHERE nota_id = '" .$nota_id. "' AND kamera_id = '".$kamera_id."'");
    $p = mysqli_query($link, "SELECT SUM(hargasewa+denda)as grandtotal from hub_notasewa_dan_kamera WHERE nota_id = '" .$nota_id. "'");
            $res_p = mysqli_fetch_array($p);
            $total = $res_p['grandtotal']; 

     $y = mysqli_query($link,'UPDATE notasewa SET grandtotal = '.$total. ', dendaterlambat = '.$denda.' WHERE id = "' .$nota_id. '"');

    echo '<script language="javascript">
    var id="' .$nota_id. '"; 
          alert("Data berhasil diubah.")
          document.location.href="detildenda.php?id="+id
          </script>';

 
}

?>
<?php
if(isset($_POST['kerusakan']))
{

    
    $nota_id = $_POST['idnota'];
    $kamera_id = $_POST['idkamera'];
    $hargasewa = $_POST['hargasewa'];
    $dendakerusakan = $_POST['biayakerusakan'];
    $rusak = $_POST['rusaks'];

     $w = mysqli_query($link, "UPDATE hub_notasewa_dan_kamera SET  hargasewa = '" .$hargasewa. "', rusak = '".$dendakerusakan."', kondisikamera = '".$rusak."' WHERE nota_id = '" .$nota_id. "' AND kamera_id = '".$kamera_id."'");
     $p = mysqli_query($link, "SELECT SUM(hargasewa+rusak+denda)as grandtotal from hub_notasewa_dan_kamera WHERE nota_id = '" .$nota_id. "'");
            $res_p = mysqli_fetch_array($p);
            $totalrusak = $res_p['grandtotal']; 

     $y = mysqli_query($link,"UPDATE notasewa SET grandtotal = '".$totalrusak."', dendakerusakan = '".$dendakerusakan."', kondisikamera = '".$rusak."'  WHERE id =  '".$nota_id. "'");
  
  echo '<script language="javascript"> 
  var id="' .$nota_id. '"; 
          alert("Data berhasil diubah.")
          document.location.href="detildenda.php?id="+id
          </script>';


}

?>  