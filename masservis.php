<?php
session_start();
require './db.php';

if(isset($_SESSION['admin']))
{
    $admin = $_SESSION['admin'];

    if(!isset($_SESSION['admin_loggedIn']))
    {
        echo '<script language="javascript">';
        echo 'document.location.href="login.php"';
        echo '</script>';
    }
    else
    {
        $pengguna = $_SESSION['admin_loggedIn'];
    }
}
else
{
    echo '<script language="javascript">';
    echo 'window.alert("Anda harus login terlebih dahulu!");';
    echo 'document.location.href="../login.php"';
    echo '</script>';
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Master Servis | ADMIN</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

       <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Anda Masuk Mode Admin</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $pengguna; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="profil_admin.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-coffee"></i> Master Barang<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="masbarang.php">Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masmesin.php">Mesin Kopi</a>
                                </li>
                                <li>
                                    <a href="masspare.php">Sparepart</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="maskategori.php"><i class="fa fa-fw fa-edit"></i> Master Kategori Barang</a>
                        </li>
                        <li>
                            <a href="maspelanggan.php"><i class="fa fa-fw fa-users"></i> Master Pelanggan</a>
                        </li>
                         <li>
                            <a href="maslelang.php"><i class="fa fa-university"></i> Master Lelang Resep</a>
                        </li>
                         <li >
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa fa-book"></i> Master Order Jual<i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="masorder.php">Order Biji Kopi</a>
                                </li>
                                <li>
                                    <a href="masorder_mesin.php">Order Mesin Kopi</a>
                                </li>
                            </ul>
                        </li>
                        <li >
                            <a href="maskaryawan.php"><i class="fa fa-male"></i>  Master Karyawan</a>
                        </li>
                        <li >
                            <a href="maskomplain.php"><i class="fa fa-question"></i>  Master Keluhan</a>
                        </li>
                        <li>
                            <a href="masservis.php"><i class="fa fa-wrench"></i>  Transaksi Servis</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Master Servis
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-wrench"></i> Master Servis
                                </li>
                            </ol>
                        </div>
                        <div class="col-sm-10">
                            <h2>Data Servis Saat Ini</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr >
                                            <th style="text-align: center;">ID SERVIS</th>
                                            <th style="text-align: center;">DETIL KOMPLAIN</th>
                                            <th style="text-align: center;">TANGGAL NOTA</th>
                                            <th style="text-align: center;">SPAREPART</th>
                                            <th style="text-align: center;">LAYANAN</th>
                                            <th style="text-align: center;">TOTAL BIAYA</th>
                                            <th style="text-align: center;">STATUS SERVIS</th>
                                            <th style="text-align: center;">KARYAWAN</th>
                                            <th style="text-align: center;">CETAK NOTA</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'tanggal_indo.php';

                                        // SELECT *, k.barang_id as id_bar FROM karyawan kar, pelanggan p, komplain k, nota_servis n LEFT JOIN hub_sparepart_notaservis h1 ON n.id_servis = h1.servis_id LEFT JOIN hub_layanan_notaservis h2 ON n.id_servis = h2.servis_id WHERE p.id_pel = k.pelanggan_id AND k.id_komplain = n.komplain_id AND n.karyawan_id = kar.id AND p.hapuskah = '0' AND k.hapuskah = '0' AND n.hapuskah = '0'

                                        

                                        $sql = "SELECT *, k.barang_id as id_bar FROM karyawan kar, pelanggan p, komplain k, nota_servis n WHERE p.id_pel = k.pelanggan_id AND k.id_komplain = n.komplain_id AND n.karyawan_id = kar.id AND p.hapuskah = '0' AND k.hapuskah = '0' AND n.hapuskah = '0'";
                                        $result = mysqli_query($link, $sql);
                                        if (!$result) {
                                            die("SQL Error:" . mysqli_error($link));
                                        }
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<tr class= "row1">';
                                            echo "<th style='text-align: center;'>" . $row['id_servis'] . "</th>";

                                            $p = mysqli_query($link, "SELECT nama_barang from barang where id_barang = '".$row['id_bar']. "'");
                                            $ress = mysqli_fetch_array($p);
                                            $nama = $ress['nama_barang'];
                                            echo "<td class='row1 col-sm-2' style='text-align: center;'>

                                             <button type='button' class='btn btn-info btn-sm' data-pela='" .$row['nama_pel']. "' data-bar='" .$nama. "' data-sam='" .$row['deskripsi']. "' data-toggle='modal' data-target='#komplain'>Detil Komplain</button>

                                            </td>";

                                            echo "<td class='row1 col-sm-3' style='text-align: center;'>" . TanggalIndo($row['tanggal']) ."</td>";

                                            echo "<td class='row1 col-sm-1' style='text-align: center;'>
                                            <a href='detil_servis_spare.php?nota=".$row['id_servis']. "'>
                                                <button type='submit' name='pass' class='btn btn-info btn-sm'>Sparepart</button></a>
                                                </td>";

                                            echo "<td class='row1 col-sm-1' style='text-align: center;'>
                                            <a href='detil_servis_layanan.php?nota=" .$row['id_servis']. "'>
                                                <button name='pass' class='btn btn-info btn-sm'>Layanan</button></a>
                                                </td>";

                                                echo "
                                            <td class='row1 col-sm-2' style='text-align: center;'>Rp." .number_format($row['grand_total'], 0, ',', '.') .",-" ."<br></br>
                                            </td>";

                                            echo "<td class='row1 col-sm-2' style='text-align: center;'>" 
                                                .$row['status']."<br></br>
                                                <button type='button' class='btn btn-primary btn-sm' data-nota='" .$row['id_servis']. "' data-stat='" .$row['status']. "' data-toggle='modal' data-target='#edit_stat'>Edit</button>
                                            </td>";

                                            echo "<td class='row1 col-sm-2'>" .$row['karyawan_id']. " - " .$row['nama']. "</td>";

                                            echo "<td class='row1 col-sm-2' style='text-align: center;'>
                                            <form method='POST' action='cetak_notaServis.php'>
                                                <input type='hidden' name='idnota' value='" .$row['id_servis']. "' />
                                                <button type='submit' class='btn btn-success btn-sm' name='cetak'><i class='fa fa-print'></i></button>
                                                </form></td>";
                                            
                                        echo "</tr>";
                                        }   ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="modal fade" id="komplain" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Detil Komplain</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Diajukan Oleh </label><br>
                                                    <span class="namaa"></span>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Mesin </label><br>
                                                    <span class="mesinn"></span>
                                                </div>
                                                <br>
                                                <div class="form-group row">
                                                    <label for="NamaPenerima">Keluhan </label><br>
                                                    <span class="keluh"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#komplain').on('show.bs.modal', function(e) {

                                var pela1 = $(e.relatedTarget).data('pela');
                                var bar1 = $(e.relatedTarget).data('bar');
                                var sam1 = $(e.relatedTarget).data('sam');
    
                                $(".namaa").html(pela1);  
                                $(".mesinn").html(bar1);
                                $(".keluh").html(sam1);                              
                            });
                            </script>

                            <div class="modal fade" id="edit_stat" role="dialog">
                                <div class="modal-dialog" role="document">
                                
                                  <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Edit Detail Jual</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin-left: 5%; margin-right: 5%;">
                                                <div class="form-group row">
                                                    <form method="POST" action="masservis.php">
                                                    <input type="hidden" name="notaa" />
                                                    <label for="NamaPenerima">Status </label><br>
                                                    <select id="sta" name="stat" class="form-control">
                                                        <option value="">--Pilih Status--</option>
                                                        <option value="Diterima">Diterima</option>
                                                        <option value="Diproses">Diproses</option>
                                                        <option value="Selesai">Selesai</option>
                                                        <option value="Diambil">Diambil</option>
                                                    </select>
                                                </div>
                                                <div class="form-group row">
                                                    <input type="submit" class="btn btn-info" name="edit_stat" value="Ubah" onclick="return confirm('Apakah anda yakin untuk mengubah data?');">
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript"> 
                            $('#edit_stat').on('show.bs.modal', function(e) {
                                
                                var not = $(e.relatedTarget).data('nota');
                                var sta = $(e.relatedTarget).data('stat');
                                 
                                $(e.currentTarget).find('input[name="notaa"]').val(not);
                                $("#sta").val(sta);                                 
                            });
                            </script>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
    </body>
</html>


<?php 

if (isset($_POST['edit_stat'])) 
{
    $notaa1 = $_POST['notaa'];
    $stat1 = $_POST['stat'];

    $q = mysqli_query($link, "UPDATE nota_servis SET status = '" .$stat1. "' WHERE id_servis = '" .$notaa1. "'");

    if(!$q)
    {
        echo mysqli_error($mycon);
    }
    else
    {
        echo '<script language="javascript"> 
          alert("Status berhasil diubah.")
          document.location.href="masservis.php"
          </script>';
    }
}

?>


